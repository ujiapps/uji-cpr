package es.uji.apps.cpr.ui;

import java.io.Serializable;

import es.uji.apps.cpr.model.Departments;

public class DepartmentsUI implements Serializable
{

    private UserUI user;
    private int location;
    private String department;
    private String name;
    private String surname1;
    private String surname2;
    private String type;

    public DepartmentsUI()
    {

    }

    public UserUI getUser()
    {
        return user;
    }

    public void setUser(UserUI user)
    {
        this.user = user;
    }

    public int getLocation()
    {
        return location;
    }

    public void setLocation(int location)
    {
        this.location = location;
    }

    public String getDepartment()
    {
        return department;
    }

    public void setDepartment(String department)
    {
        this.department = department;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSurname1()
    {
        return surname1;
    }

    public void setSurname1(String surname1)
    {
        this.surname1 = surname1;
    }

    public String getSurname2()
    {
        return surname2;
    }

    public void setSurname2(String surname2)
    {
        this.surname2 = surname2;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public static DepartmentsUI toUI(Departments departments, UserUI userUI)
    {

        DepartmentsUI departmentsUI = new DepartmentsUI();
        departmentsUI.setName(departments.getName());
        departmentsUI.setDepartment(departments.getDepartment());
        departmentsUI.setLocation(departments.getLocationId());
        departmentsUI.setSurname1(departments.getSurname1());
        departmentsUI.setSurname2(departments.getSurname2());
        departmentsUI.setType(departments.getType());

        if (userUI != null)
        {
            departmentsUI.setUser(userUI);
        }

        return departmentsUI;
    }

}
