package es.uji.apps.cpr.ui;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uji.apps.cpr.model.AreasResponsible;
import es.uji.apps.cpr.model.DepartmentsAreas;
import es.uji.apps.cpr.model.User;

public class AreasResponsibleUI implements Serializable {

    private Long id;
    private String department;
    private String area;
    private int areaId;
    private UserUI user;

    public AreasResponsibleUI() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public UserUI getUser() {
        return user;
    }

    public void setUser(UserUI user) {
        this.user = user;
    }

    public static AreasResponsibleUI toUI(AreasResponsible areasResponsible) {

        AreasResponsibleUI areasResponsibleUI = new AreasResponsibleUI();
        areasResponsibleUI.setId(areasResponsible.getId());
        areasResponsibleUI.setUser(UserUI.toUI(areasResponsible.getUser(), null));
        areasResponsibleUI.setArea(areasResponsible.getDepartmentsAreas().getArea());
        areasResponsibleUI.setAreaId(areasResponsible.getDepartmentsAreas().getAreaId());
        areasResponsibleUI.setDepartment(areasResponsible.getDepartmentsAreas().getDepartment());

        return areasResponsibleUI;
    }

    public static AreasResponsible toModel(AreasResponsibleUI areasResponsibleUI) {
        AreasResponsible areasResponsible = new AreasResponsible();

        if (areasResponsibleUI.getId() != null) {
            areasResponsible.setId(areasResponsibleUI.getId());
        }
        User user = new User();
        user.setUserId(areasResponsibleUI.getUser().getId());
        areasResponsible.setUser(user);
        DepartmentsAreas departmentsAreas = new DepartmentsAreas();
        departmentsAreas.setAreaId(areasResponsibleUI.getAreaId());
        areasResponsible.setDepartmentsAreas(departmentsAreas);

        return areasResponsible;
    }

    public static String toCSV(List<AreasResponsibleUI> areasResponsibles) {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = null;

        try {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<String[]>();

            List<String> record = null;

            record = Arrays.asList("DEPARTAMENT", "AREA", "USUARI");

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (AreasResponsibleUI areasResponsible : areasResponsibles) {
                record = Arrays.asList(String.valueOf(String.valueOf(areasResponsible.getDepartment())),
                        String.valueOf(areasResponsible.getArea()),
                        String.valueOf(areasResponsible.getUser().getName().toString().concat(" " + areasResponsible.getUser().getSurname())));

                recordArray = new String[record.size()];
                record.toArray(recordArray);
                records.add(recordArray);
            }

            csvWriter.writeAll(records);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

}
