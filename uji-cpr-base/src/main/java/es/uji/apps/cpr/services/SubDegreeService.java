package es.uji.apps.cpr.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.SubDegreeDAO;
import es.uji.apps.cpr.model.SubDegree;

@Service
public class SubDegreeService {

    @Autowired
    private SubDegreeDAO subDegreeDAO;

    public List<SubDegree> getSubDegreeByIdSubject(String subjectId) throws GeneralCPRException {
        return subDegreeDAO.getSubDegreeByIdSubject(subjectId);
    }

    public List<SubDegree> getSubDegreeByIdDegree(int degreeId) throws GeneralCPRException {
        return subDegreeDAO.getSubDegreeByIdDegree(degreeId);
    }

}

