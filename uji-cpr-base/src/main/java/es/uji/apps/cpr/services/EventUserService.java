package es.uji.apps.cpr.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.EventsUsersDAO;
import es.uji.apps.cpr.model.EventUser;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;

@Service
public class EventUserService
{

    static final Logger logger = LoggerFactory.getLogger(UserKeyService.class);

    @Autowired
    private EventsUsersDAO eventsUsersDAO;

    public List<EventUser> getEventUsersByEventId(String eventId, int start, int limit,
            List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        return eventsUsersDAO.getEventUsersByEventId(eventId, start, limit, filterList, sortList);
    }

    public List<EventUser> getEventUsersByEventIdAndType(String eventId, String type, int start,
            int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
            throws GeneralCPRException
    {
        return eventsUsersDAO.getEventUsersByEventIdAndType(eventId, type, start, limit,
                filterList, sortList);
    }

    public List<EventUser> getEventUsersByUserId(long userId, int start, int limit,
            List<ItemFilter> filterList, List<ItemSort> sortList)
    {

        List<EventUser> eventUserList = null;

        try
        {
            eventUserList = eventsUsersDAO.getEventUsersByUserId(userId, start, limit, filterList,
                    sortList);
        }
        catch (GeneralCPRException ex)
        {
            logger.info("No existe el evento asociado al usuario para el identificador propuesto.",
                    ex);
        }

        return eventUserList;

    }

}
