package es.uji.apps.cpr.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysema.query.Tuple;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.CheckDAO;
import es.uji.apps.cpr.dao.EventsDAO;
import es.uji.apps.cpr.dao.IncidenceDAO;
import es.uji.apps.cpr.model.Check;
import es.uji.apps.cpr.model.DepartmentsAreas;
import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.model.Incidence;
import es.uji.apps.cpr.ui.CheckUI;
import es.uji.apps.cpr.ui.EventUI;
import es.uji.apps.cpr.ui.UserUI;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;

@Service
public class EventService
{

    static final Logger logger = LoggerFactory.getLogger(EventService.class);

    @Autowired
    private EventsDAO eventsDAO;

    @Autowired
    private CheckDAO checksDAO;

    @Autowired
    private IncidenceDAO incidenceDAO;

    public Event getEventById(String id) throws GeneralCPRException
    {
        return eventsDAO.getEventById(id);
    }

    public List<EventUI> getEventsPendingByUserId(long userId, Date startDate, Date endDate, int start,
                                                  int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
            throws GeneralCPRException
    {
        List<Event> events = eventsDAO.getEventsPendingByUserId(userId, startDate, endDate, filterList, sortList);

        return parseEvents(events, userId, filterList, sortList);
    }

    public List<EventUI> getEventsByUserId(long userId, Date startDate, Date endDate, int start,
                                           int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
            throws GeneralCPRException
    {
        List<Event> events = eventsDAO.getEventsByUserId(userId, startDate, endDate, filterList, sortList);

        return parseEvents(events, userId, filterList, sortList);
    }

    public List<EventUI> getEventsByUserIdAndCode(long userId, String code, Date startDate, Date endDate, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        List<Event> events = eventsDAO.getEventsByUserIdAndCode(userId, code, startDate, endDate, filterList, sortList);

        return parseEvents(events, userId, filterList, sortList);
    }

    public Event getEventPrevUser(long userId, Calendar time, int start, int limit,
                                  List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {

        List<Event> events = eventsDAO.getEventsByUserIdAndDate(userId, time, start, limit,
                filterList, sortList);
        ListIterator<Event> listIterator = events.listIterator();

        while (listIterator.hasNext())
        {
            Event event = listIterator.next();

            Calendar endEvent = new GregorianCalendar();
            endEvent.setTime(event.getEndEvent());

            if (endEvent.get(Calendar.HOUR_OF_DAY) < time.get(Calendar.HOUR_OF_DAY))
            {
                return event;
            }
        }

        return null;
    }

    public Event getEventNextUser(long userId, Calendar time, int start, int limit,
                                  List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {

        List<Event> events = eventsDAO.getEventsByUserIdAndDate(userId, time, start, limit,
                filterList, sortList);

        ListIterator<Event> listIterator = events.listIterator();

        while (listIterator.hasNext())
        {
            Event event = listIterator.next();

            Calendar startEvent = new GregorianCalendar();
            startEvent.setTime(event.getStartEvent());

            if (startEvent.get(Calendar.HOUR_OF_DAY) > time.get(Calendar.HOUR_OF_DAY))
            {
                return event;
            }
        }

        return null;
    }

    public Event getEventPrevLocation(String location, Calendar time, int start, int limit,
                                      List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {

        List<Event> events = eventsDAO.getEventsByLocationAndDate(location, time, start, limit,
                filterList, sortList);
        ListIterator<es.uji.apps.cpr.model.Event> listIterator = events.listIterator();

        while (listIterator.hasNext())
        {
            es.uji.apps.cpr.model.Event event = listIterator.next();

            Calendar endEvent = new GregorianCalendar();
            endEvent.setTime(event.getEndEvent());

            if (endEvent.get(Calendar.HOUR_OF_DAY) < time.get(Calendar.HOUR_OF_DAY))
            {
                return event;
            }
        }

        return null;
    }

    public Event getEventNextLocation(String location, Calendar time, int start, int limit,
                                      List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        List<Event> events = eventsDAO.getEventsByLocationAndDate(location, time, start, limit,
                filterList, sortList);

        ListIterator<Event> listIterator = events.listIterator();

        while (listIterator.hasNext())
        {
            Event event = listIterator.next();

            Calendar startEvent = new GregorianCalendar();
            startEvent.setTime(event.getStartEvent());

            if (startEvent.get(Calendar.HOUR_OF_DAY) > time.get(Calendar.HOUR_OF_DAY))
            {
                return event;
            }
        }

        return null;
    }

    public List<Event> getEventsByCode(String code, Date startDate, Date endDate, List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        List<Event> events = eventsDAO.getEventsByCode(code, startDate, endDate, filterList, sortList);

        return events;
    }

    public List<Tuple> getEventsIncidencePendingDegree(Long userId, Date startDateFormatted, Date endDateFormatted) throws GeneralCPRException
    {
       return eventsDAO.getEventsIncidencePendingDegree(userId, startDateFormatted, endDateFormatted);
    }

    public List<Tuple> getEventsIncidencePendingArea(List<DepartmentsAreas> areasList, Date startDateFormatted, Date endDateFormatted)
    {
        return eventsDAO.getEventsIncidencePendingArea(areasList, startDateFormatted, endDateFormatted);
    }

    public List<Event> groupEvents(List<Event> events) throws GeneralCPRException
    {
        List<Event> eventList = new ArrayList<Event>();
        boolean exist = false;
        int group = -1;
        Date startEvent = null;

        for (Event event : events)
        {
            if (event.getGrupoComp() != null)
            {

                if (event.getGrupoComp().equals(group) && event.getStartEvent().equals(startEvent))
                {
                    exist = true;
                } else
                {
                    exist = false;
                    startEvent = event.getStartEvent();
                    group = event.getGrupoComp();
                    event.setCode(event.getGrupoNom());
                }
            } else
            {
                group = -1;
            }
            if (exist == false)
            {
                eventList.add(event);
            }
        }
        return eventList;
    }

    // TODO: Falta implenetar el offset y el limit para la lista de eventos.
    public List<EventUI> parseEvents(List<Event> events, long userId, List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {

        List<EventUI> eventUIList = new ArrayList<EventUI>();
        boolean exist = false;
        int group = -1;
        String location = "";
        Date startEvent = null;


        for (Event event : events)
        {
            if (event.getGrupoComp() != null)
            {

                if (event.getGrupoComp().equals(group) && event.getStartEvent().equals(startEvent) && event.getLocation().equals(location))
                {
                    exist = true;
                } else
                {
                    exist = false;
                    location = event.getLocation();
                    startEvent = event.getStartEvent();
                    group = event.getGrupoComp();
                    event.setCode(event.getGrupoNom());
                }

            } else
            {
                group = -1;
                location = "";
                exist = false;
            }

            if (exist == false)
            {
                List<CheckUI> checkListIn = new ArrayList<CheckUI>();
                List<CheckUI> checkListOut = new ArrayList<CheckUI>();
                Set<Incidence> incidences = event.getIncidences();

                if (incidences != null)
                {
                    for (Incidence incidence : incidences)
                    {
                        if (incidence.getCheckIn() == null && incidence.getStatus() != 2)
                        {
                            CheckUI checkUIIn = new CheckUI();
                            checkUIIn.setStatus(incidence.getStatus());
                            checkUIIn.setEvent(EventUI.toUI(event));
                            checkUIIn.setLocation(event.getLocation());
                            checkUIIn.setTime(event.getStartEvent());
                            checkUIIn.setUser(UserUI.toUI(incidence.getUserRequest(), null));
                            checkListIn.add(checkUIIn);
                        } else if (incidence.getCheckIn() != null && incidence.getStatus() != 2)
                        {
                            CheckUI checkUIIn = CheckUI.toUI(incidence.getCheckIn(), null, null, incidence.getStatus());
                            checkListIn.add(checkUIIn);
                        }
                    }
                }

                Set<Check> checkList = event.getChecks();

                for (Check check : checkList)
                {
                    if (check.getDirection() != null && check.getUser().getType().equals("PDI"))
                    {
                        if (check.getDirection() == 0)
                        {
                            checkListIn.add(CheckUI.toUI(check, null, null, -1));
                        }
                    }
                }

                EventUI eventUI = EventUI.toUIByUI(event, checkListIn, checkListOut);
                eventUI.setStartDate(new Date(event.getStartEvent().getTime()));
                eventUI.setEndDate(new Date(event.getEndEvent().getTime()));
                eventUIList.add(eventUI);
            }

        }
        return eventUIList;
    }

    public Boolean checkSamePOD(String code, Long userId){
        return eventsDAO.checkSamePOD(code, userId);
    }

    public List<Tuple> getEventsByArea(Integer areaId)
    {
        return eventsDAO.getEventsByArea(areaId);

    }

    public List<Tuple> getEventsByTit(Integer Tit)
    {
        return eventsDAO.getEventsByTit(Tit);
    }

    public List<Tuple> getEventsByTitAndAreaId(int tit, Integer areaId)
    {
        return  eventsDAO.getEventsByTitAndAreaId(tit, areaId);
    }
}
