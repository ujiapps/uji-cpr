package es.uji.apps.cpr.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.dao.ResumenEventosDAO;
import es.uji.apps.cpr.model.ResumenEventos;

@Service
public class ResumenEventosService
{
    private ResumenEventosDAO resumenEventosDAO;

    @Autowired
    public ResumenEventosService(ResumenEventosDAO resumenEventosDAO)
    {
        this.resumenEventosDAO = resumenEventosDAO;
    }

    public List<ResumenEventos> getResumenEventos(Long personaId, String codigo)
    {
        return resumenEventosDAO.getResumenEventos(personaId,codigo);
    }
}
