package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CPR_TERMINAL_CHECKS", schema = "UJI_CONTROLPRESENCIA")
public class Check implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private long id;

    @ManyToOne
    @JoinColumn(name = "IDUSER")
    private User user;

    @Column(name = "ITIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date itime;

    @Column(name = "KEY")
    private String key;

    @Column(name = "SERVICE")
    private String service;

    @Column(name = "TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @Column(name = "TYPE")
    private int type;

    @ManyToOne
    @JoinColumn(name = "IDEVENT")
    private Event event;

    @ManyToOne
    @JoinColumn(name = "IDTERMINAL")
    private Terminal terminal;

    @Column(name = "DIRECTION")
    private Integer direction;

    public Check() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getItime() {
        return this.itime;
    }

    public void setItime(Date itime) {
        this.itime = itime;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getService() {
        return this.service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Date getTime() {
        return this.time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Terminal getTerminal() {
        return this.terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }
}
