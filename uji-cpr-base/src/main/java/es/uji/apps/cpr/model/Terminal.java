package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CPR_TERMINAL", schema = "UJI_CONTROLPRESENCIA")
public class Terminal implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private long id;

    @Column(name = "CACHE_LAST")
    private long cacheLast;

    @Column(name = "CACHE_MODE")
    private long cacheMode;

    @Column(name = "CACHE_TIME_DAYS")
    private long cacheTimeDays;

    @Column(name = "CACHE_TIME_START")
    private long cacheTimeStart;

    private String description;

    @Column(name = "GEN_EVENTS")
    private long genEvents;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "LOCATION_PHISICAL")
    private String locationPhisical;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NET_ETHER_IP")
    private String netEtherIp;

    @Column(name = "NET_ETHER_MASK")
    private String netEtherMask;

    @Column(name = "NET_ETHER_MODE")
    private long netEtherMode;

    @Column(name = "NET_GW")
    private String netGw;

    @Column(name = "NET_WIFI_IP")
    private String netWifiIp;

    @Column(name = "NET_WIFI_KEY")
    private String netWifiKey;

    @Column(name = "NET_WIFI_MASK")
    private String netWifiMask;

    @Column(name = "NET_WIFI_MODE")
    private long netWifiMode;

    @Column(name = "NET_WIFI_SSID")
    private String netWifiSsid;

    @Column(name = "ONLINE_MODE")
    private long onlineMode;

    @Column(name = "READ_MODE")
    private long readMode;

    @Column(name = "READ_ONLY_IN")
    private long readOnlyIn;

    @Column(name = "READ_TIME_START")
    private long readTimeStart;

    @Column(name = "READ_TIME_STOP")
    private long readTimeStop;

    @Column(name = "SEND_BUFFER")
    private long sendBuffer;

    @Column(name = "SEND_MODE")
    private long sendMode;

    @Column(name = "SEND_TIME_INTERVAL")
    private long sendTimeInterval;

    @Column(name = "STATUS")
    private long status;
    @Column(name = "TYPE")
    private long type;


    // bi-directional many-to-one association to Check
    @OneToMany(mappedBy = "terminal")
    private Set<Check> checks;

    public Terminal() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCacheLast() {
        return this.cacheLast;
    }

    public void setCacheLast(long cacheLast) {
        this.cacheLast = cacheLast;
    }

    public long getCacheMode() {
        return this.cacheMode;
    }

    public void setCacheMode(long cacheMode) {
        this.cacheMode = cacheMode;
    }

    public long getCacheTimeDays() {
        return this.cacheTimeDays;
    }

    public void setCacheTimeDays(long cacheTimeDays) {
        this.cacheTimeDays = cacheTimeDays;
    }

    public long getCacheTimeStart() {
        return this.cacheTimeStart;
    }

    public void setCacheTimeStart(long cacheTimeStart) {
        this.cacheTimeStart = cacheTimeStart;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getGenEvents() {
        return this.genEvents;
    }

    public void setGenEvents(long genEvents) {
        this.genEvents = genEvents;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationPhisical() {
        return this.locationPhisical;
    }

    public void setLocationPhisical(String locationPhisical) {
        this.locationPhisical = locationPhisical;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNetEtherIp() {
        return this.netEtherIp;
    }

    public void setNetEtherIp(String netEtherIp) {
        this.netEtherIp = netEtherIp;
    }

    public String getNetEtherMask() {
        return this.netEtherMask;
    }

    public void setNetEtherMask(String netEtherMask) {
        this.netEtherMask = netEtherMask;
    }

    public long getNetEtherMode() {
        return this.netEtherMode;
    }

    public void setNetEtherMode(long netEtherMode) {
        this.netEtherMode = netEtherMode;
    }

    public String getNetGw() {
        return this.netGw;
    }

    public void setNetGw(String netGw) {
        this.netGw = netGw;
    }

    public String getNetWifiIp() {
        return this.netWifiIp;
    }

    public void setNetWifiIp(String netWifiIp) {
        this.netWifiIp = netWifiIp;
    }

    public String getNetWifiKey() {
        return this.netWifiKey;
    }

    public void setNetWifiKey(String netWifiKey) {
        this.netWifiKey = netWifiKey;
    }

    public String getNetWifiMask() {
        return this.netWifiMask;
    }

    public void setNetWifiMask(String netWifiMask) {
        this.netWifiMask = netWifiMask;
    }

    public long getNetWifiMode() {
        return this.netWifiMode;
    }

    public void setNetWifiMode(long netWifiMode) {
        this.netWifiMode = netWifiMode;
    }

    public String getNetWifiSsid() {
        return this.netWifiSsid;
    }

    public void setNetWifiSsid(String netWifiSsid) {
        this.netWifiSsid = netWifiSsid;
    }

    public long getOnlineMode() {
        return this.onlineMode;
    }

    public void setOnlineMode(long onlineMode) {
        this.onlineMode = onlineMode;
    }

    public long getReadMode() {
        return this.readMode;
    }

    public void setReadMode(long readMode) {
        this.readMode = readMode;
    }

    public long getReadOnlyIn() {
        return this.readOnlyIn;
    }

    public void setReadOnlyIn(long readOnlyIn) {
        this.readOnlyIn = readOnlyIn;
    }

    public long getReadTimeStart() {
        return this.readTimeStart;
    }

    public void setReadTimeStart(long readTimeStart) {
        this.readTimeStart = readTimeStart;
    }

    public long getReadTimeStop() {
        return this.readTimeStop;
    }

    public void setReadTimeStop(long readTimeStop) {
        this.readTimeStop = readTimeStop;
    }

    public long getSendBuffer() {
        return this.sendBuffer;
    }

    public void setSendBuffer(long sendBuffer) {
        this.sendBuffer = sendBuffer;
    }

    public long getSendMode() {
        return this.sendMode;
    }

    public void setSendMode(long sendMode) {
        this.sendMode = sendMode;
    }

    public long getSendTimeInterval() {
        return this.sendTimeInterval;
    }

    public void setSendTimeInterval(long sendTimeInterval) {
        this.sendTimeInterval = sendTimeInterval;
    }

    public Set<Check> getChecks() {
        return this.checks;
    }

    public void setChecks(Set<Check> checks) {
        this.checks = checks;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public Check addCprTerminalCheck(Check check) {
        getChecks().add(check);
        check.setTerminal(this);

        return check;
    }

    public Check removeCprTerminalCheck(Check check) {
        getChecks().remove(check);
        check.setTerminal(null);

        return check;
    }

}
