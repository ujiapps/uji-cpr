package es.uji.apps.cpr.services.rest;

import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.text.SimpleDateFormat;

@Path("config")
public class ConfigResource  extends CoreBaseService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getConfig(){

        UIEntity uiEntity = new UIEntity();
        uiEntity.put("periodoDocente", GeneralUtils.getPeriodoDocente());
        uiEntity.put("initialDate",  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(GeneralUtils.getInitialDate()));
        return uiEntity;
    }
}
