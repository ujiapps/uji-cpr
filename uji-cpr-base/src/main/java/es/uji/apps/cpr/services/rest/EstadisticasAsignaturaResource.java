package es.uji.apps.cpr.services.rest;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.DepartmentsAreas;
import es.uji.apps.cpr.model.EstadisticasAsignatura;
import es.uji.apps.cpr.model.EstadisticasProfesor;
import es.uji.apps.cpr.model.Paginacion;
import es.uji.apps.cpr.services.DepartmentsAreasService;
import es.uji.apps.cpr.services.DepartmentsService;
import es.uji.apps.cpr.services.EstadisticasAsignaturaService;
import es.uji.apps.cpr.services.UserService;
import es.uji.apps.cpr.services.VerifyUsersService;
import es.uji.apps.cpr.ui.EstadisticasAreaUI;
import es.uji.apps.cpr.ui.EstadisticasProfesorUI;
import es.uji.apps.cpr.ui.EstadisticasUI;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

@Path("estadisticas")
public class EstadisticasAsignaturaResource extends CoreBaseService
{

    @InjectParam
    private EstadisticasAsignaturaService estadisticasAsignaturaService;
    @InjectParam
    private VerifyUsersService verifyUsersService;
    @InjectParam
    private DepartmentsService departmentsService;
    @InjectParam
    private UserService userService;

    @InjectParam
    private DepartmentsAreasService departmentsAreasService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEstadisicas(@QueryParam("areaId") @DefaultValue("-1") Integer areaId,
                                          @QueryParam("personaId") @DefaultValue("-1") Long personaId,
                                          @QueryParam("code") @DefaultValue("-1") String code, @QueryParam("start") @DefaultValue("0") Long start,
                                          @QueryParam("limit") @DefaultValue("25") Long limit) throws GeneralCPRException
    {
        ParamUtils.checkNotNull(areaId, personaId, code);
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();
        Boolean isAdmin = false;
        Boolean areaIdValid = false;
        Long direcTitu = null;
        Paginacion paginacion = new Paginacion(start, limit);

        isAdmin = verifyUsersService.isAdmin(connectedUserId);
        if (isAdmin)
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreas();
        } else if (verifyUsersService.isTitDir(connectedUserId))
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByTit(connectedUserId);
            direcTitu = connectedUserId;

        } else
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByUserId(connectedUserId);
        }
        if (areaId != -1)
        {
            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                if (departmentsArea.getAreaId() == areaId)
                {
                    areaIdValid = true;
                }
            }
            if (!areaIdValid)
            {
                throw new GeneralCPRException("Parametres no valids.");
            } else
            {
                departmentsAreas.clear();
                departmentsAreas.add(departmentsAreasService.getDepartamentArea(areaId));
            }
        }
        List<EstadisticasAsignatura> list = estadisticasAsignaturaService.searchEstadisticas(departmentsAreas, personaId, code, paginacion, direcTitu, isAdmin);
        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(paginacion.getTotalCount().intValue());
        responseMessage.setData(UIEntity.toUI(list));
        return responseMessage;
    }

    @GET
    @Path("profesor")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getEstadisicasProfesor(@QueryParam("areaId") @DefaultValue("-1") Integer areaId,
                                                  @QueryParam("personaId") @DefaultValue("-1") Long personaId,
                                                  @QueryParam("code") @DefaultValue("-1") String code, @QueryParam("start") @DefaultValue("0") Long start,
                                                  @QueryParam("limit") @DefaultValue("25") Long limit) throws GeneralCPRException
    {
        ParamUtils.checkNotNull(areaId, personaId, code);
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();
        Boolean isAdmin = false;
        Boolean areaIdValid = false;
        Long direcTitu = null;
        Paginacion paginacion = new Paginacion(start, limit);

        isAdmin = verifyUsersService.isAdmin(connectedUserId);
        if (isAdmin)
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreas();
        } else if (verifyUsersService.isTitDir(connectedUserId))
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByTit(connectedUserId);
            direcTitu = connectedUserId;

        } else
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByUserId(connectedUserId);
        }
        if (areaId != -1)
        {
            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                if (departmentsArea.getAreaId() == areaId)
                {
                    areaIdValid = true;
                }
            }
            if (!areaIdValid)
            {
                throw new GeneralCPRException("Parametres no valids.");
            } else
            {
                departmentsAreas.clear();
                departmentsAreas.add(departmentsAreasService.getDepartamentArea(areaId));
            }
        }
        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(estadisticasAsignaturaService.searchEstadisticasProfesor(departmentsAreas, personaId, direcTitu));
        return responseMessage;
    }


    @GET
    @Path("file/csv")
    @Produces("text/csv")
    public Response generarListadoAreasCSV(@QueryParam("areaId") @DefaultValue("-1") Integer areaId,
                                        @QueryParam("personaId") @DefaultValue("-1") Long personaId,
                                        @QueryParam("code") String code,
                                        @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException, GeneralCPRException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();
        Template template = new PDFTemplate("cpr/pdfArea", new Locale("ES"), "cpr");
        Boolean isAdmin = false;
        Boolean areaIdValid = false;
        Long direcTitu = null;
        template.put("titulo", "Estadístiques de Seguiment docent");
        template.put("cursoActual", GeneralUtils.getCursoActual());

        isAdmin = verifyUsersService.isAdmin(connectedUserId);
        if (isAdmin)
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreas();
        } else if (verifyUsersService.isTitDir(connectedUserId))
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByTit(connectedUserId);
            direcTitu = connectedUserId;
        } else
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByUserId(connectedUserId);
        }
        if (areaId != -1)
        {
            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                if (departmentsArea.getAreaId() == areaId)
                {
                    areaIdValid = true;
                }
            }
            if (!areaIdValid)
            {
                throw new GeneralCPRException("Parametres no valids.");
            } else
            {
                DepartmentsAreas area = departmentsAreasService.getDepartamentArea(areaId);
                template.put("titulo", "Estadístiques de Seguiment docent per assignatura");
                template.put("areaNombre", area.getArea());
                departmentsAreas.clear();
                departmentsAreas.add(area);
            }
        }
        List<EstadisticasAreaUI> estadisticasPorArea = new ArrayList<>();
        for (DepartmentsAreas da : departmentsAreas)
        {
            List<EstadisticasAsignatura> list = estadisticasAsignaturaService.searchEstadisticasArea(da, personaId, code, direcTitu);

            HashMap<String, List<EstadisticasAsignatura>> listadoAgrupado = new HashMap<String, List<EstadisticasAsignatura>>();
            if (!list.isEmpty())
            {
                for (EstadisticasAsignatura estadisticasAsignatura : list)
                {
                    if (!listadoAgrupado.containsKey(estadisticasAsignatura.getCode()))
                    {
                        List<EstadisticasAsignatura> lista = new ArrayList<EstadisticasAsignatura>();
                        lista.add(estadisticasAsignatura);
                        listadoAgrupado.put(estadisticasAsignatura.getCode(), lista);
                    } else
                        listadoAgrupado.get(estadisticasAsignatura.getCode()).add(estadisticasAsignatura);
                }

                List<EstadisticasUI> estadisticasUIList = new ArrayList<>();
                Iterator it = listadoAgrupado.entrySet().iterator();
                EstadisticasAreaUI estadisticasAreaUI = new EstadisticasAreaUI();
                EstadisticasUI estadisticasUI = null;
                while (it.hasNext())
                {
                    Map.Entry pairs = (Map.Entry) it.next();
                    estadisticasUI = new EstadisticasUI((List<EstadisticasAsignatura>) pairs.getValue());
                    estadisticasUIList.add(estadisticasUI);
                    it.remove();
                }
                Collections.sort(estadisticasUIList, new Comparator<EstadisticasUI>()
                {
                    @Override
                    public int compare(EstadisticasUI i1, EstadisticasUI i2)
                    {
                        String ip = i1.getGrupoNombre();
                        return ip.compareTo(i2.getGrupoNombre());
                    }
                });
                estadisticasAreaUI.setArea(da);
                estadisticasAreaUI.setEstadisticasUI(estadisticasUIList);
                estadisticasAreaUI.calculaSumatorios();
                estadisticasPorArea.add(estadisticasAreaUI);
            }

        }
//        template.put("listado", estadisticasPorArea);
//        template.put("fecha", new SimpleDateFormat("dd/MM/yyyy hh:mm").format(Calendar.getInstance().getTime()));
//        calculaSumatorios(estadisticasUIList, template);
//        return template;
//        return Response.ok(template).type("application/pdf").header("Content-Disposition", "attachment; filename = estadisticas.pdf").build();
        String data =
                estadisticasAreatoCSV(estadisticasPorArea);
        return Response.ok(data)
                .header("Content-Disposition", "attachment; filename = estadisticas.csv")
                .header("Content-Type", "text/csv").build();

    }

    @GET
    @Path("file/pdf")
    @Produces("application/pdf")
    public Response generarListadoAreasPDF(@QueryParam("areaId") @DefaultValue("-1") Integer areaId,
                                           @QueryParam("personaId") @DefaultValue("-1") Long personaId,
                                           @QueryParam("code") String code,
                                           @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException, GeneralCPRException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();
        Template template = new PDFTemplate("cpr/pdfArea", new Locale("ES"), "cpr");
        Boolean isAdmin = false;
        Boolean areaIdValid = false;
        Long direcTitu = null;
        template.put("titulo", "Estadístiques de Seguiment docent");
        template.put("cursoActual", GeneralUtils.getCursoActual());

        isAdmin = verifyUsersService.isAdmin(connectedUserId);
        if (isAdmin)
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreas();
        } else if (verifyUsersService.isTitDir(connectedUserId))
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByTit(connectedUserId);
            direcTitu = connectedUserId;
        } else
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByUserId(connectedUserId);
        }
        if (areaId != -1)
        {
            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                if (departmentsArea.getAreaId() == areaId)
                {
                    areaIdValid = true;
                }
            }
            if (!areaIdValid)
            {
                throw new GeneralCPRException("Parametres no valids.");
            } else
            {
                DepartmentsAreas area = departmentsAreasService.getDepartamentArea(areaId);
                template.put("titulo", "Estadístiques de Seguiment docent per assignatura");
                template.put("areaNombre", area.getArea());
                departmentsAreas.clear();
                departmentsAreas.add(area);
            }
        }
        List<EstadisticasAreaUI> estadisticasPorArea = new ArrayList<>();
        for (DepartmentsAreas da : departmentsAreas)
        {
            List<EstadisticasAsignatura> list = estadisticasAsignaturaService.searchEstadisticasArea(da, personaId, code, direcTitu);

            HashMap<String, List<EstadisticasAsignatura>> listadoAgrupado = new HashMap<String, List<EstadisticasAsignatura>>();
            if (!list.isEmpty())
            {
                for (EstadisticasAsignatura estadisticasAsignatura : list)
                {
                    if (!listadoAgrupado.containsKey(estadisticasAsignatura.getCode()))
                    {
                        List<EstadisticasAsignatura> lista = new ArrayList<EstadisticasAsignatura>();
                        lista.add(estadisticasAsignatura);
                        listadoAgrupado.put(estadisticasAsignatura.getCode(), lista);
                    } else
                        listadoAgrupado.get(estadisticasAsignatura.getCode()).add(estadisticasAsignatura);
                }

                List<EstadisticasUI> estadisticasUIList = new ArrayList<>();
                Iterator it = listadoAgrupado.entrySet().iterator();
                EstadisticasAreaUI estadisticasAreaUI = new EstadisticasAreaUI();
                EstadisticasUI estadisticasUI = null;
                while (it.hasNext())
                {
                    Map.Entry pairs = (Map.Entry) it.next();
                    estadisticasUI = new EstadisticasUI((List<EstadisticasAsignatura>) pairs.getValue());
                    estadisticasUIList.add(estadisticasUI);
                    it.remove();
                }
                Collections.sort(estadisticasUIList, new Comparator<EstadisticasUI>()
                {
                    @Override
                    public int compare(EstadisticasUI i1, EstadisticasUI i2)
                    {
                        String ip = i1.getGrupoNombre();
                        return ip.compareTo(i2.getGrupoNombre());
                    }
                });
                estadisticasAreaUI.setArea(da);
                estadisticasAreaUI.setEstadisticasUI(estadisticasUIList);
                estadisticasAreaUI.calculaSumatorios();
                estadisticasPorArea.add(estadisticasAreaUI);
            }

        }
        template.put("listado", estadisticasPorArea);
        template.put("fecha", new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime()));
//        calculaSumatorios(estadisticasUIList, template);
//        return template;
        return Response.ok(template).type("application/pdf").header("Content-Disposition", "attachment; filename = estadisticas.pdf").build();
//        String data =
//                estadisticasAreatoCSV(estadisticasPorArea);
//        return Response.ok(data)
//                .header("Content-Disposition", "attachment; filename = estadisticaArea.csv")
//                .header("Content-Length", data.length()).header("Content-Type", "text/csv").build();

    }


    @GET
    @Path("profesores/file/pdf")
    @Produces("application/pdf")
    public Response generarListadoProfesores(@QueryParam("areaId") @DefaultValue("-1") Integer areaId,
                                             @QueryParam("personaId") @DefaultValue("-1") Long personaId,
                                             @QueryParam("code") String code,
                                             @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException, GeneralCPRException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();
        Template template = new PDFTemplate("cpr/pdfProfesor", new Locale("ES"), "cpr");
        Boolean isAdmin = false;
        Boolean areaIdValid = false;
        Long direcTitu = null;
        isAdmin = verifyUsersService.isAdmin(connectedUserId);
        if (isAdmin)
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreas();
        } else if (verifyUsersService.isTitDir(connectedUserId))
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByTit(connectedUserId);
            direcTitu = connectedUserId;
        } else
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByUserId(connectedUserId);
        }
        if (areaId != -1)
        {
            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                if (departmentsArea.getAreaId() == areaId)
                {
                    areaIdValid = true;
                }
            }
            if (!areaIdValid)
            {
                throw new GeneralCPRException("Parametres no valids.");
            } else
            {
                DepartmentsAreas area = departmentsAreasService.getDepartamentArea(areaId);
                template.put("titulo", "Estadístiques de Seguiment docent per profesor");
                template.put("cursoActual", GeneralUtils.getCursoActual());
                template.put("areaNombre", area.getArea());
                departmentsAreas.clear();
                departmentsAreas.add(area);
            }
        }
        List<EstadisticasProfesorUI> estadisticasProfesorList = new ArrayList<>();
        for (DepartmentsAreas da : departmentsAreas)
        {

            List<EstadisticasProfesor> list = estadisticasAsignaturaService.searchEstadisticasProfesorArea(da, direcTitu);
            if (!list.isEmpty())
            {
                EstadisticasProfesorUI estadisticasProfesorUI = new EstadisticasProfesorUI();

                estadisticasProfesorUI.setArea(da);
                estadisticasProfesorUI.setEstadisticasProfesor(list);
                estadisticasProfesorUI.calculaSumatorios();
                estadisticasProfesorList.add(estadisticasProfesorUI);
            }
        }

        template.put("listado", estadisticasProfesorList);
        template.put("fecha", new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime()));
//        calculaSumatorios(estadisticasUIList, template);
//        return template;
        return Response.ok(template).type("application/pdf").header("Content-Disposition", "attachment; filename = Estadisticas-Profesor.pdf").build();
//        return Response.ok(template).type("application/pdf").header("Content-Disposition", "attachment; filename = estadisticas.pdf").build();
    }

    @GET
    @Path("profesores/file/csv")
    @Produces("text/csv")
    public Response generarCSVProfesores(@QueryParam("areaId") @DefaultValue("-1") Integer areaId,
                                         @QueryParam("personaId") @DefaultValue("-1") Long personaId,
                                         @QueryParam("code") String code,
                                         @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException, GeneralCPRException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();
        Boolean isAdmin = false;
        Boolean areaIdValid = false;
        Long direcTitu = null;

        isAdmin = verifyUsersService.isAdmin(connectedUserId);
        if (isAdmin)
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreas();
        } else if (verifyUsersService.isTitDir(connectedUserId))
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByTit(connectedUserId);
            direcTitu = connectedUserId;
        } else
        {
            departmentsAreas = departmentsAreasService.getDepartmentsAreasByUserId(connectedUserId);
        }
        if (areaId != -1)
        {
            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                if (departmentsArea.getAreaId() == areaId)
                {
                    areaIdValid = true;
                }
            }
            if (!areaIdValid)
            {
                throw new GeneralCPRException("Parametres no valids.");
            } else
            {
                DepartmentsAreas area = departmentsAreasService.getDepartamentArea(areaId);
                departmentsAreas.clear();
                departmentsAreas.add(area);
            }
        }
        List<EstadisticasProfesorUI> estadisticasProfesorList = new ArrayList<>();
        for (DepartmentsAreas da : departmentsAreas)
        {

            List<EstadisticasProfesor> list = estadisticasAsignaturaService.searchEstadisticasProfesorArea(da, direcTitu);
            if (!list.isEmpty())
            {
                EstadisticasProfesorUI estadisticasProfesorUI = new EstadisticasProfesorUI();

                estadisticasProfesorUI.setArea(da);
                estadisticasProfesorUI.setEstadisticasProfesor(list);
                estadisticasProfesorList.add(estadisticasProfesorUI);
            }
        }
        String data =
                toCSV(estadisticasProfesorList);
        Response.ResponseBuilder res = null;
        return Response.ok(data)
                .header("Content-Disposition", "attachment; filename = estadisticaProfesores.csv")
                .header("Content-Type", "text/csv").build();
    }

    public static String estadisticasAreatoCSV(List<EstadisticasAreaUI> estadisticasAreaUIList)
    {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = null;

        try
        {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<String[]>();

            List<String> record = null;

            record = Arrays.asList("AREA", "NOM", "GRUPS", "SESSIONS", "INCIDENTADES", "PENDENTS", "OBERTES", "RESOLTES", "REVOCADES", "AUTO-RESOLTES");

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (EstadisticasAreaUI estadisticas : estadisticasAreaUIList)
            {
                for (EstadisticasUI estadisticasUI : estadisticas.getEstadisticasUI())
                {
                    record = Arrays.asList(estadisticas.getArea().getArea(),
                            estadisticasUI.getAsigNombre(),
                            estadisticasUI.getTotalGrupos().toString(),
                            estadisticasUI.getTotalSesionesGrupos().toString(),
                            estadisticasUI.getTotalSinIncidencias().toString(),
                            estadisticasUI.getTotalPendientes().toString(),
                            estadisticasUI.getTotalAbiertas().toString(),
                            estadisticasUI.getTotalResueltas().toString(),
                            estadisticasUI.getTotalRevocadas().toString(),
                            estadisticasUI.getTotalAutoResueltas().toString());

                    recordArray = new String[record.size()];
                    record.toArray(recordArray);
                    records.add(recordArray);
                }
            }

            csvWriter.writeAll(records);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return writer.toString();
    }

    public static String toCSV(List<EstadisticasProfesorUI> estadisticasProfesorUIs)
    {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = null;

        try
        {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<String[]>();

            List<String> record = null;

            record = Arrays.asList("AREA", "NOM", "SESSIONS", "INCIDENTADES", "PENDENTS", "OBERTES", "RESOLTES", "REVOCADES", "AUTO-RESOLTES");

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (EstadisticasProfesorUI estadisticas : estadisticasProfesorUIs)
            {
                for (EstadisticasProfesor profesor : estadisticas.getEstadisticasProfesor())
                {
                    record = Arrays.asList(estadisticas.getArea().getArea(),
                            profesor.getNombre(), profesor.getTotalSesiones().toString(),
                            profesor.getSinIncidencia().toString(),
                            profesor.getPendientes().toString(),
                            profesor.getIncidenciasAbiertas().toString(),
                            profesor.getIncidenciasResueltas().toString(),
                            profesor.getIncidenciasRevocadas().toString(),
                            profesor.getIncidenciasAuto().toString());

                    recordArray = new String[record.size()];
                    record.toArray(recordArray);
                    records.add(recordArray);
                }
            }

            csvWriter.writeAll(records);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return writer.toString();
    }

    private void calculaSumatorios(List<EstadisticasUI> estadisticasUIList, Template template)
    {
        Integer sumGrupo = 0;
        Long sumSesiones = 0L;
        Long sumSinIn = 0L;
        Long sumPen = 0L;
        Long sumAbier = 0L;
        Long sumResueltas = 0L;
        Long sumRevocadas = 0L;
        Long sumAuto = 0L;
        for (EstadisticasUI est : estadisticasUIList)
        {
            sumGrupo = sumGrupo + est.getTotalGrupos();
            sumSesiones = sumSesiones + est.getTotalSesionesGrupos();
            sumSinIn = sumSinIn + est.getTotalSinIncidencias();
            sumPen = sumPen + est.getTotalPendientes();
            sumAbier = sumAbier + est.getTotalAbiertas();
            sumResueltas = sumResueltas + est.getTotalResueltas();
            sumRevocadas = sumRevocadas + est.getTotalRevocadas();
            sumAuto = sumAuto + est.getTotalAutoResueltas();
        }
        template.put("sumGrupo", sumGrupo);
        template.put("sumSesiones", sumSesiones);
        template.put("sumSinIn", sumSinIn);
        template.put("sumPen", sumPen);
        template.put("sumAbier", sumAbier);
        template.put("sumResueltas", sumResueltas);
        template.put("sumRevocadas", sumRevocadas);
        template.put("sumAuto", sumAuto);

        template.put("sumSinInPor", sinIncidenciaPor(sumSinIn, sumSesiones));
        template.put("sumPenPor", pendientesPor(sumSinIn, sumSesiones, sumPen));
        template.put("sumAbierPor", incidenciaAbriertasPor(sumSinIn, sumSesiones, sumAbier));
        template.put("sumResueltasPor", incidenciaResueltasPor(sumSinIn, sumSesiones, sumResueltas));
        template.put("sumRevocadasPor", incidenciaRevocadaPor(sumSinIn, sumSesiones, sumRevocadas));
        template.put("sumAutoPor", incidenciaAutoResPor(sumSinIn, sumSesiones, sumAuto));
    }

    public static double round(double value, int places)
    {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private Double sinIncidenciaPor(Long sinIncidencia, Long totalSesiones)
    {
        try
        {
            Long incidentadas = totalSesiones - sinIncidencia;
            return round(((incidentadas * 100.0) / totalSesiones), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }

    }

    private Double pendientesPor(Long sinIncidencia, Long totalSesiones, Long pendientes)
    {
        try
        {
            return round(((pendientes * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    private Double incidenciaAbriertasPor(Long sinIncidencia, Long totalSesiones, Long incidenciasAbiertas)
    {
        try
        {
            return round(((incidenciasAbiertas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }

    }

    private Double incidenciaResueltasPor(Long sinIncidencia, Long totalSesiones, Long incidenciasResueltas)
    {
        try
        {
            return round(((incidenciasResueltas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    private Double incidenciaRevocadaPor(Long sinIncidencia, Long totalSesiones, Long incidenciasRevocadas)
    {
        try
        {
            return round(((incidenciasRevocadas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }


    }

    private Double incidenciaAutoResPor(Long sinIncidencia, Long totalSesiones, Long incidenciasAuto)
    {
        try
        {
            return round(((incidenciasAuto * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

}
