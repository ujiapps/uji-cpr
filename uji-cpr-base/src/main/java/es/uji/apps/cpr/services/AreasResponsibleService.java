package es.uji.apps.cpr.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.AreasResponsibleDAO;
import es.uji.apps.cpr.model.AreasResponsible;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;

@Service
public class AreasResponsibleService {

    static final Logger logger = LoggerFactory.getLogger(AreasResponsibleService.class);

    @Autowired
    private AreasResponsibleDAO areasResponsibleDAO;

    @Transactional
    public AreasResponsible insert(AreasResponsible areasResponsible, Long connectedUserId) {
        return areasResponsibleDAO.insert(areasResponsible);
    }

    @Transactional
    public AreasResponsible update(AreasResponsible areasResponsible, Long connectedUserId) {
        return areasResponsibleDAO.update(areasResponsible);
    }

    @Transactional
    public void delete(long id) {
        areasResponsibleDAO.delete(AreasResponsible.class, id);
    }

    public AreasResponsible getAreasResponsibleById(long id) throws GeneralCPRException {
        return areasResponsibleDAO.getAreasResponsibleById(id);
    }

    public List<AreasResponsible> getAreasResponsiblesByUserId(Long connectedUserId, int start, int limit,
                                                               List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException {
        return areasResponsibleDAO.getAreasResponsiblesByUserId(connectedUserId, start, limit, filterList,
                sortList);
    }

    public List<AreasResponsible> getAreasResponsiblesByAreaId(int areaId, Long connectedUserId, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList) {
        return areasResponsibleDAO.getAreasResponsiblesByAreaId(areaId, start, limit, filterList, sortList);
    }

    public List<AreasResponsible> getAreasResponsiblesByAreaIds(List<Integer> areaIds, Long connectedUserId, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList) {
        return areasResponsibleDAO.getAreasResponsiblesByAreaIds(areaIds, start, limit, filterList, sortList);
    }

    public long getAreasResponsiblesByAreaIdsCount(List<Integer> areaIds, List<ItemFilter> filterList) {
        return areasResponsibleDAO.getAreasResponsiblesByAreaIdsCount(areaIds, filterList);
    }

}
