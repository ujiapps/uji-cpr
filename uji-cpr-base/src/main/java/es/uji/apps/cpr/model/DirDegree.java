package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cpr_vmc_dir_titulacion", schema = "UJI_CONTROLPRESENCIA")
public class DirDegree implements Serializable {

    @Id
    @Column(name = "ID_TITULACION")
    private int degreeId;

    @Column(name = "ID_PERSONA")
    private long userId;

    @ManyToOne
    @JoinColumn(name = "ID_PERSONA" , insertable = false, updatable = false)
    private User user;

    @OneToMany(mappedBy = "dirDegree")
    private Set<SubDegree> subDegreeSet;

    public int getDegreeId() {
        return degreeId;
    }

    public void setDegreeId(int idTitulacion) {
        this.degreeId = idTitulacion;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long idPersona) {
        this.userId = idPersona;
    }

    public Set<SubDegree> getSubDegreeSet() {
        return subDegreeSet;
    }

    public void setSubDegreeSet(Set<SubDegree> subDegreeSet) {
        this.subDegreeSet = subDegreeSet;
    }
}
