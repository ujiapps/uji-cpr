package es.uji.apps.cpr.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.DepartmentsAreasDAO;
import es.uji.apps.cpr.dao.SubDegreeDAO;
import es.uji.apps.cpr.model.DepartmentsAreas;

@Service
public class DepartmentsAreasService
{

    @Autowired
    private DepartmentsAreasDAO departmentsAreasDAO;
    @Autowired
    private SubDegreeDAO subDegreeDAO;

    //@Role({ "ADMIN", "DIRECTOR_DE_DEPARTAMENTO" })
    public List<DepartmentsAreas> getDepartmentsAreasByDepartments(Integer departmentId)
    {
        return departmentsAreasDAO.getDepartmentsAreasByDepartment(departmentId);
    }

    public List<DepartmentsAreas> getDepartmentsAreas()
    {
        return departmentsAreasDAO.getDepartmentsAreas();
    }

    public List<DepartmentsAreas> getDepartmentsAreasByUserId(Long connectedUserId)
    {
        return departmentsAreasDAO.getDepartmentsAreasByUserId(connectedUserId);
    }

    public DepartmentsAreas getDepartamentArea(Integer areaId){
        return departmentsAreasDAO.getDepartamentArea(areaId);
    }

    public List<DepartmentsAreas> getDepartmentsAreasByTit(Long connectedUserId) throws GeneralCPRException
    {
//        List<SubDegree> subDegrees = subDegreeDAO.getSubDegreeByDirId(connectedUserId);
//        List<String> codes = new ArrayList<>();
//
//        for (SubDegree subDegree : subDegrees){
//            codes.add(subDegree.getSubjectId());
//        }
        return departmentsAreasDAO.getDepartmentsAreasByDirTit(connectedUserId);
//        return departmentsAreasDAO.getDepartmentsAreasByAsig(codes);
    }
}
