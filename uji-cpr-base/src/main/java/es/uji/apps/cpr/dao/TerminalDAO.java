package es.uji.apps.cpr.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.QTerminal;
import es.uji.apps.cpr.model.Terminal;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TerminalDAO extends BaseDAODatabaseImpl {

    public Terminal getTerminalById(long terminalId) throws GeneralCPRException {

        JPAQuery query = new JPAQuery(entityManager);

        QTerminal qTerminal = QTerminal.terminal;

        Terminal terminal = new Terminal();

        if (terminalId != -1) {
            terminal = query.from(qTerminal).where(qTerminal.id.eq(terminalId))
                    .uniqueResult(qTerminal);
        }

        if (terminal == null) {
            throw new GeneralCPRException(
                    "No se ha encontrado la entidad Check solicitada a traves del ID");
        }

        return terminal;
    }

}
