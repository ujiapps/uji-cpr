package es.uji.apps.cpr.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.DirDegreeDAO;
import es.uji.apps.cpr.model.DirDegree;

@Service
public class DirDegreeService {

    @Autowired
    private DirDegreeDAO dirDegreeDAO;

    public DirDegree getDirDegreeByIdDegree(int degreeId) throws GeneralCPRException {
        return dirDegreeDAO.getDirDegreeByIdDegree(degreeId);
    }

    public DirDegree getDirDegreeByUserId(long userId) throws GeneralCPRException {
        return dirDegreeDAO.getDirDegreeByUserId(userId);
    }

    public List<DirDegree> getDirDegreeByUsersId(long userId) throws GeneralCPRException {
        return dirDegreeDAO.getDirDegreeByUsersId(userId);
    }

}
