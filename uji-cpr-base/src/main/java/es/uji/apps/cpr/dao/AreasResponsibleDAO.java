package es.uji.apps.cpr.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.AreasResponsible;
import es.uji.apps.cpr.model.QAreasResponsible;
import es.uji.apps.cpr.util.DynamicFilter;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AreasResponsibleDAO extends BaseDAODatabaseImpl {

    public AreasResponsible getAreasResponsibleById(Long id) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);
        QAreasResponsible qAreasResponsible = QAreasResponsible.areasResponsible;
        return query.from(qAreasResponsible).where(qAreasResponsible.id.eq(id))
                .uniqueResult(qAreasResponsible);
    }

    public List<AreasResponsible> getAreasResponsiblesByUserId(long userId, int start, int limit,
                                                               List<ItemFilter> filterList, List<ItemSort> sortList) {
        JPAQuery query = new JPAQuery(entityManager);
        QAreasResponsible qAreasResponsible = QAreasResponsible.areasResponsible;

        query.from(qAreasResponsible).where(qAreasResponsible.user.userId.eq(userId));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qAreasResponsible.id);
        validFilters.put("user", qAreasResponsible.user);
        validFilters.put("areaId", qAreasResponsible.departmentsAreas.areaId);
        validFilters.put("departmentsAreas", qAreasResponsible.departmentsAreas);

        if (filterList != null) {
            query.where(DynamicFilter.applyFilter(filterList, validFilters));
        }

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qAreasResponsible.id);
        validSorts.put("user", qAreasResponsible.user);
        validSorts.put("areaId", qAreasResponsible.departmentsAreas.areaId);
        validSorts.put("departmentsAreas", qAreasResponsible.departmentsAreas);

        if (sortList != null) {
            for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts)) {
                query.orderBy(o);
            }
        }

        return query.list(qAreasResponsible);
    }

    public List<AreasResponsible> getAreasResponsiblesByAreaId(int areaId, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList) {
        JPAQuery query = new JPAQuery(entityManager);
        QAreasResponsible qAreasResponsible = QAreasResponsible.areasResponsible;

        query.from(qAreasResponsible).where(qAreasResponsible.departmentsAreas.areaId.eq(areaId));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qAreasResponsible.id);
        validFilters.put("user", qAreasResponsible.user);
        validFilters.put("areaId", qAreasResponsible.departmentsAreas.areaId);
        validFilters.put("departmentsAreas", qAreasResponsible.departmentsAreas);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qAreasResponsible.id);
        validSorts.put("user", qAreasResponsible.user);
        validSorts.put("areaId", qAreasResponsible.departmentsAreas.areaId);
        validSorts.put("departmentsAreas", qAreasResponsible.departmentsAreas);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts)) {
            query.orderBy(o);
        }
        if (start != -1) {
            query.offset(start).limit(limit);
        }
        return query.list(qAreasResponsible);
    }

    public List<AreasResponsible> getAreasResponsiblesByAreaIds(List<Integer> areaIds, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList) {
        JPAQuery query = new JPAQuery(entityManager);
        QAreasResponsible qAreasResponsible = QAreasResponsible.areasResponsible;

        query.from(qAreasResponsible).leftJoin(qAreasResponsible.departmentsAreas).fetch().where(qAreasResponsible.departmentsAreas.areaId.in(areaIds));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qAreasResponsible.id);
        validFilters.put("user", qAreasResponsible.user);
        validFilters.put("areaId", qAreasResponsible.departmentsAreas.areaId);
        validFilters.put("departmentsAreas", qAreasResponsible.departmentsAreas);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qAreasResponsible.id);
        validSorts.put("user", qAreasResponsible.user);
        validSorts.put("areaId", qAreasResponsible.departmentsAreas.areaId);
        validSorts.put("departmentsAreas", qAreasResponsible.departmentsAreas);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts)) {
            query.orderBy(o);
        }
        if (start != -1) {
            query.offset(start).limit(limit);
        }
        return query.list(qAreasResponsible);
    }

    public long getAreasResponsiblesByAreaIdsCount(List<Integer> areaIds, List<ItemFilter> filterList) {
        JPAQuery query = new JPAQuery(entityManager);
        QAreasResponsible qAreasResponsible = QAreasResponsible.areasResponsible;

        query.from(qAreasResponsible).where(qAreasResponsible.departmentsAreas.areaId.in(areaIds));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qAreasResponsible.id);
        validFilters.put("user", qAreasResponsible.user);
        validFilters.put("areaId", qAreasResponsible.departmentsAreas.areaId);
        validFilters.put("departmentsAreas", qAreasResponsible.departmentsAreas);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        return query.count();
    }

}
