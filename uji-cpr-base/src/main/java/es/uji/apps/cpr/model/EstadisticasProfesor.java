package es.uji.apps.cpr.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CPR_VMC_ESTADISTICAS_PROFESOR", schema = "UJI_CONTROLPRESENCIA", catalog = "")
public class EstadisticasProfesor
{
    @Id
    @Column(name = "PER_ID")
    private Long personaId;

    private String nombre;
    @Column(name = "AREA_ID")
    private Long areaId;

    @Column(name = "AREA")
    private String area;

    @Column(name = "TOTAL_SESIONES")
    private Long totalSesiones;
    @Column(name = "TOTAL_INCIDENCIAS_CREADAS")
    private Long totalIncidenciasCreadas;
    @Column(name = "SIN_INCIDENCIA")
    private Long sinIncidencia;
    @Column(name = "PENDIENTES")
    private Long pendientes;
    @Column(name = "INCIDENCIAS_ABIERTAS")
    private Long incidenciasAbiertas;
    @Column(name = "INCIDENCIAS_AUTO")
    private Long incidenciasAuto;
    @Column(name = "INCIDENCIAS_REVOCADAS")
    private Long incidenciasRevocadas;
    @Column(name = "INCIDENCIAS_RESUELTAS")
    private Long incidenciasResueltas;

    @Transient
    private Double sumSinInPor;
    @Transient
    private Double sumPenPor;
    @Transient
    private Double sumAbierPor;
    @Transient
    private Double sumResueltasPor;
    @Transient
    private Double sumRevocadasPor;
    @Transient
    private Double sumAutoPor;

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getAreaId()
    {
        return areaId;
    }

    public void setAreaId(Long areaId)
    {
        this.areaId = areaId;
    }

    public String getArea()
    {
        return area;
    }

    public void setArea(String area)
    {
        this.area = area;
    }

    public Long getTotalSesiones()
    {
        return totalSesiones;
    }

    public void setTotalSesiones(Long totalSesiones)
    {
        this.totalSesiones = totalSesiones;
    }

    public Long getTotalIncidenciasCreadas()
    {
        return totalIncidenciasCreadas;
    }

    public void setTotalIncidenciasCreadas(Long totalIncidenciasCreadas)
    {
        this.totalIncidenciasCreadas = totalIncidenciasCreadas;
    }

    public Long getSinIncidencia()
    {
        return sinIncidencia;
    }

    public void setSinIncidencia(Long sinIncidencia)
    {
        this.sinIncidencia = sinIncidencia;
    }

    public Long getPendientes()
    {
        return pendientes;
    }

    public void setPendientes(Long pendientes)
    {
        this.pendientes = pendientes;
    }

    public Long getIncidenciasAbiertas()
    {
        return incidenciasAbiertas;
    }

    public void setIncidenciasAbiertas(Long incidenciasAbiertas)
    {
        this.incidenciasAbiertas = incidenciasAbiertas;
    }

    public Long getIncidenciasAuto()
    {
        return incidenciasAuto;
    }

    public void setIncidenciasAuto(Long incidenciasAuto)
    {
        this.incidenciasAuto = incidenciasAuto;
    }

    public Long getIncidenciasRevocadas()
    {
        return incidenciasRevocadas;
    }

    public void setIncidenciasRevocadas(Long incidenciasRevocadas)
    {
        this.incidenciasRevocadas = incidenciasRevocadas;
    }

    public Long getIncidenciasResueltas()
    {
        return incidenciasResueltas;
    }

    public void setIncidenciasResueltas(Long incidenciasResueltas)
    {
        this.incidenciasResueltas = incidenciasResueltas;
    }

    public Double getSumSinInPor()
    {
        return sumSinInPor;
    }

    public void setSumSinInPor(Double sumSinInPor)
    {
        this.sumSinInPor = sumSinInPor;
    }

    public Double getSumPenPor()
    {
        return sumPenPor;
    }

    public void setSumPenPor(Double sumPenPor)
    {
        this.sumPenPor = sumPenPor;
    }

    public Double getSumAbierPor()
    {
        return sumAbierPor;
    }

    public void setSumAbierPor(Double sumAbierPor)
    {
        this.sumAbierPor = sumAbierPor;
    }

    public Double getSumResueltasPor()
    {
        return sumResueltasPor;
    }

    public void setSumResueltasPor(Double sumResueltasPor)
    {
        this.sumResueltasPor = sumResueltasPor;
    }

    public Double getSumRevocadasPor()
    {
        return sumRevocadasPor;
    }

    public void setSumRevocadasPor(Double sumRevocadasPor)
    {
        this.sumRevocadasPor = sumRevocadasPor;
    }

    public Double getSumAutoPor()
    {
        return sumAutoPor;
    }

    public void setSumAutoPor(Double sumAutoPor)
    {
        this.sumAutoPor = sumAutoPor;
    }

    public void calculaSumatorios()
    {
        this.sumSinInPor = sinIncidenciaPor(this.getSinIncidencia(), this.getTotalSesiones());
        this.sumPenPor = pendientesPor(this.getSinIncidencia(), this.getTotalSesiones(), this.getPendientes());
        this.sumAbierPor = incidenciaAbriertasPor(this.getSinIncidencia(), this.getTotalSesiones(), this.getIncidenciasAbiertas());
        this.sumResueltasPor = incidenciaResueltasPor(this.getSinIncidencia(), this.getTotalSesiones(), this.getIncidenciasResueltas());
        this.sumRevocadasPor = incidenciaRevocadaPor(this.getSinIncidencia(), this.getTotalSesiones(), this.getIncidenciasRevocadas());
        this.sumAutoPor = incidenciaAutoResPor(this.getSinIncidencia(), this.getTotalSesiones(), this.getIncidenciasAuto());
    }

    private Double sinIncidenciaPor(Long sinIncidencia, Long totalSesiones)
    {
        try
        {
            Long incidentadas = totalSesiones - sinIncidencia;

            return Math.round(((incidentadas * 100D) / totalSesiones) * 10D) / 10D;
        } catch (ArithmeticException e)
        {
            return 0D;
        }

    }

    private Double pendientesPor(Long sinIncidencia, Long totalSesiones, Long pendientes)
    {
        try
        {
            return Math.round(((pendientes * 100D) / (totalSesiones)) * 10) / 10D;
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    private Double incidenciaAbriertasPor(Long sinIncidencia, Long totalSesiones, Long incidenciasAbiertas)
    {
        try
        {
            return Math.round(((incidenciasAbiertas * 100D) / (totalSesiones)) * 10) / 10D;
        } catch (ArithmeticException e)
        {
            return 0D;
        }

    }

    private Double incidenciaResueltasPor(Long sinIncidencia, Long totalSesiones, Long incidenciasResueltas)
    {
        try
        {
            return Math.round(((incidenciasResueltas * 100D) / (totalSesiones)) * 10) / 10D;
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    private Double incidenciaRevocadaPor(Long sinIncidencia, Long totalSesiones, Long incidenciasRevocadas)
    {
        try
        {
            return Math.round(((incidenciasRevocadas * 100D) / (totalSesiones)) * 10) / 10D;
        } catch (ArithmeticException e)
        {
            return 0D;
        }


    }

    private Double incidenciaAutoResPor(Long sinIncidencia, Long totalSesiones, Long incidenciasAuto)
    {
        try
        {
            return Math.round(((incidenciasAuto * 100D) / (totalSesiones)) * 10) / 10D;
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }
}
