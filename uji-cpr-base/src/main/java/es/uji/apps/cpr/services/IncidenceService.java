package es.uji.apps.cpr.services;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.IncidenceDAO;
import es.uji.apps.cpr.model.Incidence;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class IncidenceService
{

    static final Logger logger = LoggerFactory.getLogger(IncidenceService.class);

    @Autowired
    private IncidenceDAO incidenceDAO;

    @Transactional
    public Incidence insert(Incidence incidence)
    {
        return incidenceDAO.insert(incidence);
    }

    @Transactional
    public Incidence update(Incidence incidence)
    {
        return incidenceDAO.update(incidence);
    }

    /*
     * public void delete(Incidence incidence) { incidenceDAO.delete(incidence); }
     */

    public Incidence getIncidenceById(long id) throws GeneralCPRException
    {
        return incidenceDAO.getIncidenceById(id);
    }

    public List<Incidence> getIncidencesByUserId(Long connectedUserId, int start, int limit,
                                                 List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        return incidenceDAO.getIncidencesByUserId(connectedUserId, start, limit, filterList, sortList);
    }

    public List<Incidence> getIncidenceByAreaId(int areaId, Long connectedUserId, int start, int limit,
                                                List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        return incidenceDAO.getIncidenceByAreaId(areaId, start, limit, filterList, sortList);
    }

    public List<Incidence> getIncidenceByAreaIds(List<Integer> areaId, Long connectedUserId, int start, int limit,
                                                 List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        return incidenceDAO.getIncidenceByAreaIds(areaId, start, limit, filterList, sortList);
    }

    public long getIncidencesByAreaIdsCount(List<Integer> areaId, List<ItemFilter> filterList) throws GeneralCPRException
    {
        return incidenceDAO.getIncidencesByAreaIdsCount(areaId, filterList);
    }

    public long getIncidencesByAreaIdCount(int areaId, List<ItemFilter> filterList) throws GeneralCPRException
    {
        return incidenceDAO.getIncidencesByAreaIdCount(areaId, filterList);
    }

    public List<Incidence> getIncidencesByCode(String code, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        return incidenceDAO.getIncidencesByCode(code, start, limit, filterList, sortList);
    }

    public long getIncidencesByCodeCount(String code, List<ItemFilter> filterList) throws GeneralCPRException
    {
        return incidenceDAO.getIncidencesByCodeCount(code, filterList);
    }

    public List<Incidence> getIncidencesByCodes(List<String> codes, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        return incidenceDAO.getIncidencesByCodes(codes, start, limit, filterList, sortList);
    }

    public long getIncidencesByCodesCount(List<String> codes, List<ItemFilter> filterList) throws GeneralCPRException
    {
        return incidenceDAO.getIncidencesByCodesCount(codes, filterList);
    }
}
