package es.uji.apps.cpr.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.EstadisticasAsignaturaDAO;
import es.uji.apps.cpr.dao.SubDegreeDAO;
import es.uji.apps.cpr.model.DepartmentsAreas;
import es.uji.apps.cpr.model.EstadisticasAsignatura;
import es.uji.apps.cpr.model.EstadisticasProfesor;
import es.uji.apps.cpr.model.EstadisticasProfesorTit;
import es.uji.apps.cpr.model.Paginacion;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Service
public class EstadisticasAsignaturaService
{

    static final Logger logger = LoggerFactory.getLogger(EstadisticasAsignaturaService.class);

    @Autowired
    private EstadisticasAsignaturaDAO estadisticasAsignaturaDAO;

    @Autowired
    private SubDegreeDAO subDegreeDAO;

    public List<EstadisticasAsignatura> getAllEstadisticas()
    {
        return estadisticasAsignaturaDAO.getAllEstadisticas();
    }

    public List<EstadisticasAsignatura> searchEstadisticas(List<DepartmentsAreas> areas, Long personaId, String code, Paginacion paginacion, Long direcTitu, Boolean isAdmin) throws GeneralCPRException
    {
        return estadisticasAsignaturaDAO.searchEstadisticas(areas, personaId, code, paginacion, direcTitu, isAdmin);
    }

    public List<EstadisticasAsignatura> searchEstadisticasArea(DepartmentsAreas area, Long personaId, String code, Long direcTitu)
    {
        return estadisticasAsignaturaDAO.searchEstadisticasArea(area, personaId, code, direcTitu);
    }

    public List<EstadisticasProfesor> searchEstadisticasProfesorArea(DepartmentsAreas da, Long directorTitu)
    {
        if (ParamUtils.isNotNull(directorTitu))
        {
            List<EstadisticasProfesor> estadisticasProfesors = new ArrayList<>();
            return to(estadisticasAsignaturaDAO.searchEstadisticasProfesor(da, directorTitu));
        } else
        {
            return estadisticasAsignaturaDAO.searchEstadisticasProfesor(da);
        }
    }

    public List<EstadisticasProfesor> to(List<EstadisticasProfesorTit> estadisticasProfesorTits)
    {
        List<EstadisticasProfesor> estadisticasProfesors = new ArrayList<>();
        for (EstadisticasProfesorTit estadisticasProfesorTit : estadisticasProfesorTits)
        {
            EstadisticasProfesor estadisticasProfesor = new EstadisticasProfesor();
            estadisticasProfesor.setPersonaId(estadisticasProfesorTit.getPersonaId());
            estadisticasProfesor.setNombre(estadisticasProfesorTit.getNombre());
            estadisticasProfesor.setAreaId(estadisticasProfesorTit.getAreaId());
            estadisticasProfesor.setArea(estadisticasProfesorTit.getArea());
            estadisticasProfesor.setTotalSesiones(estadisticasProfesorTit.getTotalSesiones());
            estadisticasProfesor.setTotalIncidenciasCreadas(estadisticasProfesorTit.getTotalIncidenciasCreadas());
            estadisticasProfesor.setSinIncidencia(estadisticasProfesorTit.getSinIncidencia());
            estadisticasProfesor.setPendientes(estadisticasProfesorTit.getPendientes());
            estadisticasProfesor.setIncidenciasAbiertas(estadisticasProfesorTit.getIncidenciasAbiertas());
            estadisticasProfesor.setIncidenciasAuto(estadisticasProfesorTit.getIncidenciasAuto());
            estadisticasProfesor.setIncidenciasRevocadas(estadisticasProfesorTit.getIncidenciasRevocadas());
            estadisticasProfesor.setIncidenciasResueltas(estadisticasProfesorTit.getIncidenciasResueltas());
            estadisticasProfesors.add(estadisticasProfesor);

        }
        return estadisticasProfesors;
    }

    public List<UIEntity> searchEstadisticasProfesor(List<DepartmentsAreas> da, Long personaId, Long directorTitu)
    {
        if (ParamUtils.isNotNull(directorTitu))
        {
            return UIEntity.toUI(estadisticasAsignaturaDAO.searchEstadisticasProfesor(da, personaId, directorTitu));
        } else
        {
            return UIEntity.toUI(estadisticasAsignaturaDAO.searchEstadisticasProfesor(da, personaId));
        }
    }
}
