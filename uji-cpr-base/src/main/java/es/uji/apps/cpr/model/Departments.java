package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CPR_VW_DEPARTAMENTOS", schema = "UJI_CONTROLPRESENCIA")
public class Departments implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private User user;

    @Column(name = "UBICACION_ID")
    private Integer locationId;

    @Column(name = "DEPARTAMENTO")
    private String department;

    @Column(name = "NOMBRE")
    private String name;

    @Column(name = "APELLIDO1")
    private String surname1;

    @Column(name = "APELLIDO2")
    private String surname2;

    @Column(name = "TYPE")
    private String type;

    @ManyToMany(mappedBy = "departments")
    private Set<DepartmentsAreas> departmentsAreas;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<DepartmentsAreas> getDepartmentsAreas() {
        return departmentsAreas;
    }

    public void setDepartmentsAreas(Set<DepartmentsAreas> departmentsAreas) {
        this.departmentsAreas = departmentsAreas;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer ubicacionId) {
        this.locationId = ubicacionId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String departamento) {
        this.department = departamento;
    }

    public String getName() {
        return name;
    }

    public void setName(String nombre) {
        this.name = nombre;
    }

    public String getSurname1() {
        return surname1;
    }

    public void setSurname1(String apellido1) {
        this.surname1 = apellido1;
    }

    public String getSurname2() {
        return surname2;
    }

    public void setSurname2(String apellido2) {
        this.surname2 = apellido2;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
