package es.uji.apps.cpr.services.rest;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cpr.model.Check;
import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.services.CheckService;
import es.uji.apps.cpr.services.EventService;
import es.uji.apps.cpr.ui.CheckUI;
import es.uji.apps.cpr.ui.EventUI;
import es.uji.apps.cpr.ui.NearEventsUI;
import es.uji.apps.cpr.ui.ResponseItemUI;
import es.uji.apps.cpr.ui.ResponseListUI;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

@Path("checks")
public class CheckResources extends CoreBaseService {

    @InjectParam
    private CheckService checkService;

    @InjectParam
    private EventService eventService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<CheckUI> getChecks(
            @QueryParam("start") @DefaultValue("-1") Integer start,
            @QueryParam("limit") @DefaultValue("25") Integer limit,
            @QueryParam("filter") @DefaultValue("") String filter,
            @QueryParam("sort") @DefaultValue("") String sort,
            @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {

        ResponseListUI<CheckUI> response = new ResponseListUI<CheckUI>();
        Boolean success = true;
        try {
            Long userId = AccessManager.getConnectedUserId(request);

            List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
            List<ItemSort> sortList = ItemSort.jsonDecode(sort);

            List<Check> checkListAll = checkService.getChecksByUserId(userId, -1, limit, filterList, sortList);
            List<Check> checkList = checkListAll;
            if (start != -1) {
                checkList = checkService.getChecksByUserId(userId, start, limit, filterList, sortList);
            }

            List<CheckUI> checkUIList = new ArrayList<CheckUI>();
            for (Check check : checkList) {
                Event event = new Event();
                event = check.getEvent();

                checkUIList.add(CheckUI.toUI(check, null, event, -1));
            }

            response.setData(checkUIList);
            response.setCount(checkListAll.size());
        } catch (Exception e) {
            success = false;
            response.setMessage(e.getLocalizedMessage());
        }
        response.setSuccess(success);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/nearEvents")
    public ResponseItemUI<NearEventsUI> getEventsById(@PathParam("id") long checkId,
                                                      @QueryParam("start") @DefaultValue("-1") Integer start,
                                                      @QueryParam("limit") @DefaultValue("25") Integer limit,
                                                      @QueryParam("filter") @DefaultValue("") String filter,
                                                      @QueryParam("sort") @DefaultValue("") String sort,
                                                      @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        ResponseItemUI<NearEventsUI> response = new ResponseItemUI<NearEventsUI>();
        Boolean success = true;
        String msg = "OK";
        try {
            Long userId = AccessManager.getConnectedUserId(request);

            List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
            List<ItemSort> sortList = ItemSort.jsonDecode(sort);

            Check check = checkService.getCheckById(checkId);

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(check.getTime().getTime());

            EventUI nextUser = EventUI.toUI(eventService.getEventNextUser(userId, calendar, start, limit, filterList, sortList));
            EventUI prevUser = EventUI.toUI(eventService.getEventPrevUser(userId, calendar, start, limit, filterList, sortList));
            EventUI nextLocation = EventUI.toUI(eventService.getEventNextLocation(check.getTerminal().getLocation(), calendar, start, limit, filterList, sortList));
            EventUI prevLocation = EventUI.toUI(eventService.getEventPrevLocation(check.getTerminal().getLocation(), calendar, start, limit, filterList, sortList));

            NearEventsUI nearEventsUI = new NearEventsUI();
            nearEventsUI.setNextByUser(nextUser);
            nearEventsUI.setPrevByUser(prevUser);
            nearEventsUI.setNextByLocation(nextLocation);
            nearEventsUI.setPrevByLocation(prevLocation);
            response.setData(nearEventsUI);

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setMessage(msg);
        response.setSuccess(success);
        return response;
    }

    @GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<CheckUI> search(
            @QueryParam("location") @DefaultValue("") String location,
            @QueryParam("fecha") @DefaultValue("") String fecha,
            @QueryParam("userLogin") @DefaultValue("") String userLogin)
    {


        ResponseListUI<CheckUI> response = new ResponseListUI<CheckUI>();
        Boolean success = true;
        try
        {
            List<Check> checkListAll = checkService.searchChecks(location, fecha, userLogin);
            List<CheckUI> checkUIList = new ArrayList<CheckUI>();
            for (Check check : checkListAll)
            {
                Event event = new Event();
                event = check.getEvent();

                checkUIList.add(CheckUI.toUI(check, check.getUser(), event, -1));
            }

            response.setData(checkUIList);
            response.setCount(checkListAll.size());
        } catch (Exception e)
        {
            success = false;
            response.setMessage(e.getLocalizedMessage());
        }
        response.setSuccess(success);
        return response;
    }

}

