package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CPR_VW_USERS", schema = "UJI_CONTROLPRESENCIA")
public class User implements Serializable {

    @Id
    @Column(name = "PERSONA_ID")
    private long userId;

    @Column(name = "NOMBRE")
    private String name;

    @Column(name = "APELLIDO1")
    private String surname1;

    @Column(name = "APELLIDO2")
    private String surname2;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "AREA_ID")
    private Integer areaId;

    @Column(name = "NOMBRE_BUSQUEDA")
    private String nombreBusqueda;

    @OneToMany(mappedBy = "user")
    private Set<AreasResponsible> areasResponsible;

    @OneToMany(mappedBy = "user")
    private Set<DirDegree> dirDegrees;

    @OneToMany(mappedBy = "user")
    private Set<Check> checks;

    @OneToMany(mappedBy = "user")
    private Set<EventUser> eventUsers;

    @OneToMany(mappedBy = "user")
    private Set<UserKey> userKeys;

    @OneToMany(mappedBy = "userRequest")
    private Set<Incidence> userRequestIncidence;

    @OneToMany(mappedBy = "userResolution")
    private Set<Incidence> userResolutionIncidence;

    @OneToMany(mappedBy = "userIDAssociated")
    private Set<Incidence> userUIAssociated;

    @OneToMany(mappedBy = "user")
    private Set<Departments> departments;

    @OneToMany(mappedBy = "user")
    private Set<EventPendent> eventPendents;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long personaId) {
        this.userId = personaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String nombre) {
        this.name = nombre;
    }

    public String getSurname1() {
        return surname1;
    }

    public void setSurname1(String apellido1) {
        this.surname1 = apellido1;
    }

    public String getSurname2() {
        return surname2;
    }

    public void setSurname2(String apellido2) {
        this.surname2 = apellido2;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getNombreBusqueda()
    {
        return nombreBusqueda;
    }

    public void setNombreBusqueda(String nombreBusqueda)
    {
        this.nombreBusqueda = nombreBusqueda;
    }

    public Set<DirDegree> getDirDegrees()
    {
        return dirDegrees;
    }

    public void setDirDegrees(Set<DirDegree> dirDegrees)
    {
        this.dirDegrees = dirDegrees;
    }

    public Set<AreasResponsible> getAreasResponsible() {
        return areasResponsible;
    }

    public void setAreasResponsible(Set<AreasResponsible> areasResponsible) {
        this.areasResponsible = areasResponsible;
    }

    public Set<Check> getChecks() {
        return checks;
    }

    public void setChecks(Set<Check> checks) {
        this.checks = checks;
    }

    public Set<EventUser> getEventUsers() {
        return eventUsers;
    }

    public void setEventUsers(Set<EventUser> eventUsers) {
        this.eventUsers = eventUsers;
    }

    public Set<UserKey> getUserKeys() {
        return userKeys;
    }

    public void setUserKeys(Set<UserKey> userKeys) {
        this.userKeys = userKeys;
    }

    public Set<Incidence> getUserRequestIncidence() {
        return userRequestIncidence;
    }

    public void setUserRequestIncidence(Set<Incidence> userRequestIncidence) {
        this.userRequestIncidence = userRequestIncidence;
    }

    public Set<Incidence> getUserResolutionIncidence() {
        return userResolutionIncidence;
    }

    public void setUserResolutionIncidence(Set<Incidence> userResolutionIncidence) {
        this.userResolutionIncidence = userResolutionIncidence;
    }

    public Set<Departments> getDepartments() {
        return departments;
    }

    public Set<Incidence> getUserUIAssociated() {
        return userUIAssociated;
    }

    public void setUserUIAssociated(Set<Incidence> userUIAssociated) {
        this.userUIAssociated = userUIAssociated;
    }

    public void setDepartments(Set<Departments> departments) {
        this.departments = departments;
    }

    public Set<EventPendent> getEventPendents()
    {
        return eventPendents;
    }

    public void setEventPendents(Set<EventPendent> eventPendents)
    {
        this.eventPendents = eventPendents;
    }
}
