package es.uji.apps.cpr.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CPR_VMC_ESTADISTICAS_ASIG", schema = "UJI_CONTROLPRESENCIA", catalog = "")
public class EstadisticasAsignatura
{
    @Id
    private String id;

    private String code;
    private String grupo;
    private String tipo;
    @Column(name = "NOMBRE_ASIGNATURA")
    private String nombreAsig;
    @Column(name = "GRUPO_NOM")
    private String grupoNombre;
    @Column(name = "TOTAL_SESIONES")
    private Long totalSesiones;
    @Column(name = "TOTAL_INCIDENCIAS_CREADAS")
    private Long totalIncidenciasCreadas;
    @Column(name = "SIN_INCIDENCIA")
    private Long sinIncidencia;
    @Column(name = "PENDIENTES")
    private Long pendientes;
    @Column(name = "INCIDENCIAS_ABIERTAS")
    private Long incidenciasAbiertas;
    @Column(name = "INCIDENCIAS_AUTO")
    private Long incidenciasAuto;
    @Column(name = "INCIDENCIAS_REVOCADAS")
    private Long incidenciasRevocadas;
    @Column(name = "INCIDENCIAS_RESUELTAS")
    private Long incidenciasResueltas;
    @Column(name = "AREA_ID")
    private Integer areaId;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getNombreAsig()
    {
        return nombreAsig;
    }

    public void setNombreAsig(String nombreAsig)
    {
        this.nombreAsig = nombreAsig;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public String getGrupoNombre()
    {
        return grupoNombre;
    }

    public void setGrupoNombre(String grupoNombre)
    {
        this.grupoNombre = grupoNombre;
    }

    public Long getTotalSesiones()
    {
        return totalSesiones;
    }

    public void setTotalSesiones(Long totalSesiones)
    {
        this.totalSesiones = totalSesiones;
    }

    public Long getTotalIncidenciasCreadas()
    {
        return totalIncidenciasCreadas;
    }

    public void setTotalIncidenciasCreadas(Long totalIncidenciasCreadas)
    {
        this.totalIncidenciasCreadas = totalIncidenciasCreadas;
    }

    public Long getSinIncidencia()
    {
        return sinIncidencia;
    }

    public void setSinIncidencia(Long sinIncidencia)
    {
        this.sinIncidencia = sinIncidencia;
    }

    public Long getPendientes()
    {
        return pendientes;
    }

    public void setPendientes(Long pendientes)
    {
        this.pendientes = pendientes;
    }

    public Long getIncidenciasAbiertas()
    {
        return incidenciasAbiertas;
    }

    public void setIncidenciasAbiertas(Long incidenciasAbiertas)
    {
        this.incidenciasAbiertas = incidenciasAbiertas;
    }

    public Long getIncidenciasAuto()
    {
        return incidenciasAuto;
    }

    public void setIncidenciasAuto(Long incidenciasAuto)
    {
        this.incidenciasAuto = incidenciasAuto;
    }

    public Long getIncidenciasRevocadas()
    {
        return incidenciasRevocadas;
    }

    public void setIncidenciasRevocadas(Long incidenciasRevocadas)
    {
        this.incidenciasRevocadas = incidenciasRevocadas;
    }

    public Long getIncidenciasResueltas()
    {
        return incidenciasResueltas;
    }

    public void setIncidenciasResueltas(Long incidenciasResueltas)
    {
        this.incidenciasResueltas = incidenciasResueltas;
    }

    public Integer getAreaId()
    {
        return areaId;
    }

    public void setAreaId(Integer areaId)
    {
        this.areaId = areaId;
    }
}
