package es.uji.apps.cpr.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.cpr.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DepartmentsAreasDAO extends BaseDAODatabaseImpl
{

    public List<DepartmentsAreas> getDepartmentsAreasByAreaId(int areaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;

        return query.from(qDepartmentsAreas).where(qDepartmentsAreas.areaId.eq(areaId))
                .list(qDepartmentsAreas);
    }

    public List<DepartmentsAreas> getDepartmentsAreasByDepartment(Integer departmentId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;

        return query.from(qDepartmentsAreas).leftJoin(qDepartmentsAreas.areasResponsible).fetch().where(qDepartmentsAreas.depId.eq(departmentId))
                .orderBy(qDepartmentsAreas.area.asc()).distinct().list(qDepartmentsAreas);
    }

    public List<DepartmentsAreas> getDepartmentsAreas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;

        return query.from(qDepartmentsAreas).leftJoin(qDepartmentsAreas.areasResponsible).fetch()
                .orderBy(qDepartmentsAreas.area.asc()).distinct().list(qDepartmentsAreas);
    }

    public List<DepartmentsAreas> getDepartmentsAreasByUserId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPASubQuery subquery = new JPASubQuery();

        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;
        QUser qUser = QUser.user;
        QDepartments qDepartments = QDepartments.departments;


        subquery.from(qDepartments).where(qDepartments.user.userId.eq(connectedUserId));
        query.from(qDepartmentsAreas).leftJoin(qDepartmentsAreas.areasResponsible).fetch().where(
                qDepartmentsAreas.depId.in(subquery.distinct().list(qDepartments.locationId)));
        query.orderBy(qDepartmentsAreas.area.asc());
        return query.distinct().list(qDepartmentsAreas);
    }

    public DepartmentsAreas getDepartamentArea(Integer areaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;
        return query.from(qDepartmentsAreas).where(qDepartmentsAreas.areaId.eq(areaId)).uniqueResult(qDepartmentsAreas);
    }


    public List<DepartmentsAreas> getDepartmentsAreasByAsig(List<String> codes)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;
        QEvent qEvent = QEvent.event;
        QUser qUser = QUser.user;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;

        JPASubQuery subQuery = new JPASubQuery();
        subQuery.from(qUser).join(qUser.eventUsers, qEventUser).join(qEventUser.event, qEvent)
                .where(qEvent.code.in(codes).and(qEvent.tipoId.in("TE", "PR", "LA","SE")).and(qUser.type.eq("PDI")));


        query.from(qDepartmentsAreas).leftJoin(qDepartmentsAreas.areasResponsible).fetch().where(qDepartmentsAreas.areaId.in(subQuery.list(qUser.areaId)));
        return query.list(qDepartmentsAreas);
    }

    public List<DepartmentsAreas> getDepartmentsAreasByDirTit(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;
        QEvent qEvent = QEvent.event;
        QUser qUser = QUser.user;
        QDirDegree qDirDegree = QDirDegree.dirDegree;
        QSubDegree qSubDegree = QSubDegree.subDegree;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;

        JPASubQuery subQuery = new JPASubQuery();
        subQuery.from(qUser).join(qUser.eventUsers, qEventUser)
                .join(qEventUser.event, qEvent)
                .where(qEvent.code.in(new JPASubQuery().from(qSubDegree).join(qSubDegree.dirDegree, qDirDegree)
                        .where(qDirDegree.user.userId.eq(connectedUserId)).distinct().list(qSubDegree.subjectId)));

        query.from(qDepartmentsAreas).where(qDepartmentsAreas.areaId.in(subQuery.distinct().list(qUser.areaId))).orderBy(qDepartmentsAreas.area.asc());
        return query.distinct().list(qDepartmentsAreas);
    }
}
