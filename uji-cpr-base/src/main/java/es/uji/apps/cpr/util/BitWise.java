package es.uji.apps.cpr.util;

public class BitWise {

    static public byte setBitTo1(byte value, int pos)
    {
        return value = (byte) (value | (1 << pos));
    }

    static public byte setBitTo0(byte value, int pos)
    {
        return value = (byte) (value & ~(1 << pos));
    }

    static public boolean isBitEnabled(int value, int pos)
    {
        return ((value>>pos) & 1) != 0;
    }

}
