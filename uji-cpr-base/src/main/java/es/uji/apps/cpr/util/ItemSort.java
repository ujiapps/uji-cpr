package es.uji.apps.cpr.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

public class ItemSort
{
    private final String field;
    private final String direction;

    public ItemSort(String field, String direction)
    {
        this.field = field;
        this.direction = direction;
    }

    public static List<ItemSort> jsonDecode(String json) throws IOException
    {
        List<ItemSort> itemFilters = new ArrayList<ItemSort>();
        if (json != null && json.length() > 0)
        {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory factory = mapper.getJsonFactory();
            JsonParser jp = factory.createJsonParser(json);
            JsonNode actualObj = mapper.readTree(jp);
            for (int i = 0; i < actualObj.size(); i++)
            {
                String sp = actualObj.get(i).get("property").getTextValue();
                String sd = actualObj.get(i).get("direction").getTextValue();
                itemFilters.add(new ItemSort(sp, sd));
            }
        }
        return itemFilters;
    }

    public String getField()
    {
        return field;
    }

    public String getDirection()
    {
        return direction;
    }

}
