package es.uji.apps.cpr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.model.Incidence;
import es.uji.apps.cpr.model.QCheck;
import es.uji.apps.cpr.model.QDepartments;
import es.uji.apps.cpr.model.QDepartmentsAreas;
import es.uji.apps.cpr.model.QDirDegree;
import es.uji.apps.cpr.model.QEvent;
import es.uji.apps.cpr.model.QIncidence;
import es.uji.apps.cpr.model.QSubDegree;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class StatsDAO extends BaseDAODatabaseImpl {


    public List<Event> getIncidencesByDegree(Long connectedUserId) {

        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subquery = new JPAQuery(entityManager);

//        SELECT ev.* FROM CPR_VW_EVENTS_HISTORICO ev JOIN CPR_INCIDENCES inc ON ev.ID=inc.EVENT_ID WHERE
//        ev.CODE in (SELECT ast.ID_ASIGNATURA FROM CPR_EXT_ASI_TITULACION ast
//        JOIN CPR_EXT_DIR_TITULACION dt ON dt.ID_TITULACION=ast.ID_TITULACION WHERE dt.ID_PERSONA=54341);

        QEvent qEvent = QEvent.event;
        QIncidence qIncidence = QIncidence.incidence;
        QSubDegree qSubDegree = QSubDegree.subDegree;
        QDirDegree qDirDegree = QDirDegree.dirDegree;

        query.from(qEvent).join(qEvent.incidences, qIncidence)
                .where(qEvent.code.in(
                        subquery.from(qSubDegree).join(qSubDegree.dirDegree, qDirDegree)
                                .where(qDirDegree.userId.eq(connectedUserId)).list(qSubDegree.subjectId)));

        return query.list(qEvent);

    }

    public List<Incidence> getIncidencesByDepartament(Integer departamentId) {

        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subquery = new JPAQuery(entityManager);

//        SELECT * FROM CPR_INCIDENCES WHERE AREA_ID in (
// SELECT dar.area_id FROM CPR_VW_DEPARTAMENTOS_AREAS dar JOIN CPR_VW_DEPARTAMENTOS dep ON dar.DEP_ID=dep.UBICACION_ID WHERE dep.per_id=60224);

        QIncidence qIncidence = QIncidence.incidence;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;
        QDepartments qDepartments = QDepartments.departments;

        query.from(qIncidence)
                .where(qIncidence.areaId.in(
                        subquery.from(qDepartmentsAreas).join(qDepartmentsAreas.departments, qDepartments)
                                .where(qDepartments.locationId.eq(departamentId)).list(qDepartmentsAreas.areaId))).list(qIncidence);

        return query.list(qIncidence);

    }

    public List<Incidence> getIncidencesPendingByUser(Long connectedUserId) {

        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subquery = new JPAQuery(entityManager);
        JPAQuery subquery2 = new JPAQuery(entityManager);


        QIncidence qIncidence = QIncidence.incidence;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;
        QDepartments qDepartments = QDepartments.departments;
        QEvent qEvent = QEvent.event;
        QSubDegree qSubDegree = QSubDegree.subDegree;
        QDirDegree qDirDegree = QDirDegree.dirDegree;
        QCheck qCheck = QCheck.check;

//        WHERE uev.PERSONA_ID=64931 AND EVENT_ID not in (
//                SELECT IDEVENT FROM (
//                SELECT IDEVENT, DIRECTION FROM CPR_TERMINAL_CHECKS WHERE iduser=64931 and DIRECTION is not null GROUP BY IDEVENT, DIRECTION
//        ) GROUP BY IDEVENT HAVING count(idevent)=2
//        ) AND event_id not in(SELECT EVENT_ID FROM CPR_INCIDENCES WHERE TYPE!=3)
//        ORDER BY ev.START_EVENT DESC;


        return query.list(qIncidence);

    }
}
