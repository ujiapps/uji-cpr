package es.uji.apps.cpr.ui;


import java.io.Serializable;
public class EventUserUI implements Serializable
{

    private EventUI eventUI;
    private UserUI userUI;

    public EventUserUI() {

    }

    public UserUI getUserUI() {
        return userUI;
    }

    public void setUserUI(UserUI userUI) {
        this.userUI = userUI;
    }

    public EventUI getEventUI() {
        return eventUI;
    }

    public void setEventUI(EventUI eventUI) {
        this.eventUI = eventUI;
    }

    public static EventUserUI toUI(EventUI eventUI, UserUI userUI)
    {
        if (eventUI == null || userUI == null)
            return null;

        EventUserUI eventUserUI = new EventUserUI();
        eventUserUI.setEventUI(eventUI);
        eventUserUI.setUserUI(userUI);

        return eventUserUI;
    }

}
