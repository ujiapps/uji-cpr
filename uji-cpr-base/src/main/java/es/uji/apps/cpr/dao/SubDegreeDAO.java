package es.uji.apps.cpr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.QSubDegree;
import es.uji.apps.cpr.model.SubDegree;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SubDegreeDAO extends BaseDAODatabaseImpl {

    public List<SubDegree> getSubDegreeByIdSubject(String subjectId) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);
        QSubDegree qSubDegree = QSubDegree.subDegree;

        query.from(qSubDegree).where(qSubDegree.subjectId.eq(subjectId));

        return query.list(qSubDegree);
    }

    public List<SubDegree> getSubDegreeByIdDegree(int degreeId) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);
        QSubDegree qSubDegree = QSubDegree.subDegree;

        query.from(qSubDegree).where(qSubDegree.dirDegree.degreeId.eq(degreeId));

        return query.list(qSubDegree);
    }

    public List<SubDegree> getSubDegreeByDirId(Long dirId) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);
        QSubDegree qSubDegree = QSubDegree.subDegree;

        query.from(qSubDegree).where(qSubDegree.dirDegree.userId.eq(dirId));

        return query.list(qSubDegree);
    }

}
