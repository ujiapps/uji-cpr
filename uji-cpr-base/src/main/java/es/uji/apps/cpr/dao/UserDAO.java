package es.uji.apps.cpr.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.*;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class UserDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem>
{

    static final int USERID_UJI_DEFAULT = 40000;
    static final int HALF_HOUR_MILIS = 1800000;

    public User getUserById(long userId) throws GeneralCPRException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QUser qUser = QUser.user;

        query.from(qUser).where(qUser.userId.eq(userId));

        return query.uniqueResult(qUser);
    }

    public List<User> getUsersByQuery(String search, Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QUser qUser = QUser.user;
        QDepartments qDepartments = QDepartments.departments;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;

        JPAQuery subquery = new JPAQuery(entityManager);

        query.from(qUser).where(
                (qUser.name.concat(" ")
                        .concat(qUser.surname1)
                        .concat(" ").concat(qUser.surname2)).lower()
                        .like("%" + search.toLowerCase() + "%")
                        .or(qUser.login.lower().like("%" + search.toLowerCase() + "%")
                        )
        ).limit(5);

        return query.list(qUser);
    }


    public List<User> searchUsersByArea(Integer areaId, String search)
    {
        JPAQuery q = new JPAQuery(entityManager);
        JPAQuery q2 = new JPAQuery(entityManager);

        JPAQuery subq = new JPAQuery(entityManager);
        JPAQuery subq2 = new JPAQuery(entityManager);

        QUser qUser = QUser.user;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;
        QDepartments qDepartments = QDepartments.departments;

        QDepartmentsAreas qDepartmentsAreas2 = QDepartmentsAreas.departmentsAreas;

        q.from(qUser).where(qUser.areaId.in(
                subq.from(qDepartmentsAreas).where(qDepartmentsAreas.depId.in(
                        subq2.from(qDepartmentsAreas2).where(qDepartmentsAreas2.areaId.eq(areaId)).list(qDepartmentsAreas2.depId)))
                        .list(qDepartmentsAreas.areaId))
                .and((qUser.name.concat(" ")
                        .concat(qUser.surname1)
                        .concat(" ").concat(qUser.surname2)).lower()
                        .like("%" + search.toLowerCase() + "%")
                        .or(qUser.login.lower().like("%" + search.toLowerCase() + "%"))
                ));
        List<User> userList = q.list(qUser);

        JPAQuery subq3 = new JPAQuery(entityManager);
        JPAQuery subq4 = new JPAQuery(entityManager);
        q2.from(qUser).where(qUser.userId.in(
                subq3.from(qDepartments).where(
                        qDepartments.locationId.in(
                                subq4.from(qDepartmentsAreas2).where(qDepartmentsAreas2.areaId.eq(areaId)).list(qDepartmentsAreas2.depId)))
                        .list(qDepartments.user.userId))
                .and((qUser.name.concat(" ")
                        .concat(qUser.surname1)
                        .concat(" ").concat(qUser.surname2)).lower()
                        .like("%" + search.toLowerCase() + "%")
                        .or(qUser.login.lower().like("%" + search.toLowerCase() + "%"))
                ));

        userList.addAll(q2.list(qUser));
        return userList;
    }

    public List<User> getUsersByAreaId(int areaId, int start, int limit)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QUser qUser = QUser.user;

        query.from(qUser).where(qUser.areaId.eq(areaId));

        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qUser);
    }

    public List<User> getUsersPDIByDates(long userId, String eventId, Date startEvent, Date endEvent, List<ItemFilter> filterList, int start, int limit) throws ParseException
    {

        JPAQuery query = new JPAQuery(entityManager);
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyyH:m:s");

        QCheck qCheck = QCheck.check;
        QUser qUser = QUser.user;
        QEvent qEvent = QEvent.event;

        Date startDate = new Date(startEvent.getTime() - HALF_HOUR_MILIS);
        Date endDate = new Date(startEvent.getTime() + HALF_HOUR_MILIS);

        query.from(qUser).join(qUser.checks, qCheck).where(qUser.type.notLike("PDI")
                .and(qCheck.event.isNull())
                .and(qUser.userId.ne(Long.valueOf((USERID_UJI_DEFAULT))))
                .and(qCheck.time.between(startDate, endDate)));

        return query.distinct().list(qUser);
    }

    public List<User> getUsersByLocation(long userId, String location, Date startEvent, Date endEvent, List<ItemFilter> filterList, int start, int limit) throws ParseException
    {

        JPAQuery query = new JPAQuery(entityManager);
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyyH:m:s");

        QCheck qCheck = QCheck.check;
        QUser qUser = QUser.user;
        QEvent qEvent = QEvent.event;
        QTerminal qTerminal = QTerminal.terminal;

        Date startDate = new Date(startEvent.getTime() - HALF_HOUR_MILIS);
        Date endDate = new Date(startEvent.getTime() + HALF_HOUR_MILIS);

        query.from(qUser).join(qUser.checks, qCheck).fetch().join(qCheck.terminal, qTerminal).where(qCheck.event.isNull()
                .and(qUser.userId.ne((long) USERID_UJI_DEFAULT)).and(qTerminal.location.eq(location))
                .and(qCheck.time.between(startDate, endDate)));

        return query.distinct().list(qUser);
    }

    public List<User> getUsersPDIByEventId(String eventId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;
        QUser qUser = QUser.user;
        QEventUser qEventUser = QEventUser.eventUser;

        query.from(qUser).join(qUser.eventUsers, qEventUser)
                .where(qUser.type.eq("PDI").and(qEventUser.event.eventId.eq(eventId)));

        return query.distinct().list(qUser);

    }

    public List<User> getUsersConAvisoDia(Date dia)
    {
        JPAQuery query = new JPAQuery(entityManager);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        QUser qUser = QUser.user;
        QEventPendent qEventPendent = QEventPendent.eventPendent;
        QLog qLog = QLog.log;

        try
        {
            Date beginday = df.parse(df.format(dia));
            Date endday = df2.parse(df.format(dia) + " 23:59:59");
            Date fecha = df.parse(df.format(new Date()));
            JPASubQuery jpaSubquery = new JPASubQuery();


            query.from(qUser).join(qUser.eventPendents, qEventPendent)
                    .where(qEventPendent.startEvent.goe(beginday).and(qEventPendent.startEvent.loe(endday))
                            .and(qUser.userId.notIn(jpaSubquery.from(qLog).where(qLog.fecha.eq(fecha).and(qLog.tipo.eq("1")))
                                    .list(qLog.perId))));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return query.distinct().list(qUser);
    }

    public List<AvisoUsuario> getUsersConAvisoSemanal(Date diaInicial, Date diaFinal)
    {
        JPAQuery query = new JPAQuery(entityManager);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        QUser qUser = QUser.user;
        QEventPendent qEventPendent = QEventPendent.eventPendent;
        QLog qLog = QLog.log;

        try
        {
            Date beginday = df.parse(df.format(diaInicial));
            Date endday = df2.parse(df.format(diaFinal) + " 23:59:59");
            Date fecha = df.parse(df.format(new Date()));
            JPASubQuery jpaSubquery = new JPASubQuery();

            query.from(qUser).join(qUser.eventPendents, qEventPendent)
                    .where(qEventPendent.startEvent.goe(beginday).and(qEventPendent.startEvent.loe(endday))
                            .and(qUser.userId.notIn(jpaSubquery.from(qLog).where(qLog.fecha.eq(fecha).and(qLog.tipo.eq("2")))
                                    .list(qLog.perId))));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return query.distinct().list(new QAvisoUsuario(qUser.login,qUser.name,qUser.userId,qEventPendent.code,qEventPendent.tipoId,qEventPendent.endEvent,
                qEventPendent.name,qEventPendent.startEvent, qEventPendent.eventId));
    }

    public List<User> getDirectoresAreaAviso(Date diaInicial, Date diaFinal)
    {
        JPAQuery subquery = new JPAQuery(entityManager);
        JPASubQuery subquery2 = new JPASubQuery();
        JPAQuery subquery3 = new JPAQuery(entityManager);
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery q2 = new JPAQuery(entityManager);
        JPAQuery q = new JPAQuery(entityManager);
        JPAQuery s = new JPAQuery(entityManager);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        QUser qUser = QUser.user;
        QEventPendent qEventPendent = QEventPendent.eventPendent;
        QDepartments qDepartments = QDepartments.departments;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;
        QAreasResponsible qAreasResponsible = QAreasResponsible.areasResponsible;
        QIncidence qIncidence = QIncidence.incidence;
        QEvent qEvent = QEvent.event;
        QLog qLog = QLog.log;

        try
        {
            Date beginday = df.parse(df.format(diaInicial));
            Date endday = df2.parse(df.format(diaFinal) + " 23:59:59");
            Date fecha = df.parse(df.format(new Date()));
            JPASubQuery jpaSubquery = new JPASubQuery();
            //TODO enviar mail si hay incidencias pendientes.


            List<Integer> areaList = subquery.from(qEventPendent)
                    .where(qEventPendent.startEvent.goe(beginday).and(qEventPendent.startEvent.loe(endday))
                            .and(qEventPendent.areaId.isNotNull()))
                    .distinct().list(qEventPendent.areaId);

            List<Integer> areaList2 = s.from(qIncidence).join(qIncidence.event).join(qIncidence.userRequest, qUser)
                    .where(qIncidence.insertTime.goe(beginday).and(qIncidence.insertTime.loe(endday)).and(qIncidence.status.eq(0))
                            .and(qIncidence.userRequest.areaId.isNotNull()))
                    .distinct().list(qIncidence.userRequest.areaId);

            for (Integer x : areaList2)
            {
                if (!areaList.contains(x))
                {
                    areaList.add(x);
                }
            }

//            List<Integer> l
// ocationList = subquery2.from(qDepartmentsAreas)
//                    .where(qDepartmentsAreas.areaId.in(areaList)).distinct().list(qDepartmentsAreas.depId);

            List<Long> userList = subquery3.from(qDepartments)
                    .where(qDepartments.locationId.in(subquery2.from(qDepartmentsAreas)
                            .where(qDepartmentsAreas.areaId.in(areaList)).distinct().list(qDepartmentsAreas.depId)).and(qDepartments.type.eq("PDI"))).distinct().list(qDepartments.user.userId);

            query.from(qUser).where(
                    qUser.userId.in(
                            q2.from(qAreasResponsible).where(
                                    qAreasResponsible.departmentsAreas.areaId.in(areaList)).list(qAreasResponsible.user.userId))
                            .or(qUser.userId.in(userList)
                            ).and(qUser.userId.notIn(jpaSubquery.from(qLog).where(qLog.fecha.eq(fecha).and(qLog.tipo.eq("3")))
                            .list(qLog.perId)))
            );

        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return query.distinct().list(qUser);

    }

    public List<User> getDirectoresTitulacionAviso(Date diaInicial, Date diaFinal)
    {
        JPAQuery subquery = new JPAQuery(entityManager);
        JPAQuery query = new JPAQuery(entityManager);


        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        QUser qUser = QUser.user;
        QEventPendent qEventPendent = QEventPendent.eventPendent;
        QDirDegree qDirDegree = QDirDegree.dirDegree;
        QLog qLog = QLog.log;
        try
        {
            Date beginday = df.parse(df.format(diaInicial));
            Date endday = df2.parse(df.format(diaFinal) + " 23:59:59");
            Date fecha = df.parse(df.format(new Date()));
            JPASubQuery jpaSubquery = new JPASubQuery();

            List<Integer> titulacionList = subquery.from(qEventPendent)
                    .where(qEventPendent.startEvent.goe(beginday).and(qEventPendent.startEvent.loe(endday)))
                    .distinct().list(qEventPendent.titulacionId);

            query.from(qUser).join(qUser.dirDegrees, qDirDegree)
                    .where(qDirDegree.degreeId.in(titulacionList).and(qUser.userId.notIn(jpaSubquery.from(qLog).where(qLog.fecha.eq(fecha).and(qLog.tipo.eq("4")))
                            .list(qLog.perId))));

        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return query.distinct().list(qUser);
    }

    @Override
    public List<LookupItem> search(String queryString)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QUser qUser = QUser.user;
        QDepartments qDepartments = QDepartments.departments;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;

        List<LookupItem> result = new ArrayList<LookupItem>();
        String cadenaFiltrada = StringUtils.limpiaAcentos(queryString).trim().toUpperCase();
        if (cadenaFiltrada != null && !cadenaFiltrada.isEmpty())
        {
            query.from(qUser).where(
                    qUser.nombreBusqueda.like("%" + cadenaFiltrada + "%")
                            .or(qUser.login.lower().like("%" + cadenaFiltrada.toLowerCase() + "%"))
            );

            for (User persona : query.list(qUser))
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(persona.getUserId()));
                lookupItem.setNombre(persona.getName() + " " + persona.getSurname1() + " " + persona.getSurname2());
                lookupItem.addExtraParam("login", persona.getLogin());

                result.add(lookupItem);
            }
        }

        return result;
    }

    public List<User> getUsersByCodeAsignatura(String code, Integer areaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QUser qUser = QUser.user;
        QEvent qEvent = QEvent.event;
        QEventUser qEventUser = QEventUser.eventUser;

        query.from(qUser).join(qUser.eventUsers, qEventUser).join(qEventUser.event, qEvent)
                .where(qEvent.tipoId.in("TE", "PR", "LA","SE").and(qUser.type.eq("PDI")));

        if (ParamUtils.isNotNull(areaId) && areaId != -1)
        {
            query.where(qUser.areaId.eq(areaId));
        }
        if (ParamUtils.isNotNull(code) && !code.equals("-1"))
        {
            query.where(qEvent.code.eq(code));
        }
        return query.distinct().list(qUser);
    }

    public Boolean isAlumno(Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QUser qUser = QUser.user;

        query.from(qUser).where(qUser.userId.eq(userId));
        User user = query.uniqueResult(qUser);

        if (user.getType().equals("AL"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
