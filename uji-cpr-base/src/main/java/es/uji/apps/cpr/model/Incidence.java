package es.uji.apps.cpr.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "CPR_INCIDENCES", schema = "UJI_CONTROLPRESENCIA")
@Entity
public class Incidence implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "TYPE")
    private int type;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "STATUS")
    private int status;

    @ManyToOne
    @JoinColumn(name = "USER_ID_REQUEST")
    @NotFound(action = NotFoundAction.IGNORE)
    private User userRequest;

    @ManyToOne
    @JoinColumn(name = "EVENT_ID")
    private Event event;

    @Column(name = "RESOLUTION")
    private String resolution;

    @ManyToOne
    @JoinColumn(name = "USER_ID_RESOLUTION")
    private User userResolution;

    @OneToOne
    @JoinColumn(name = "CHECK_IN")
    private Check checkIn;

    @OneToOne
    @JoinColumn(name = "CHECK_OUT")
    private Check checkOut;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INSERT_TIME")
    private Date insertTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESOLUTION_TIME")
    private Date resolutionTime;

    @Column(name = "AREA_ID")
    private Integer areaId;

    @ManyToOne
    @JoinColumn(name = "USER_ID_ASSOCIATED")
    private User userIDAssociated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Date getResolutionTime() {
        return resolutionTime;
    }

    public void setResolutionTime(Date resolutionTime) {
        this.resolutionTime = resolutionTime;
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public User getUserRequest() {
        return userRequest;
    }

    public void setUserRequest(User userRequest) {
        this.userRequest = userRequest;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getUserResolution() {
        return userResolution;
    }

    public void setUserResolution(User userResolution) {
        this.userResolution = userResolution;
    }

    public Check getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Check checkIn) {
        this.checkIn = checkIn;
    }

    public Check getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Check checkOut) {
        this.checkOut = checkOut;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public User getUserIDAssociated() {
        return userIDAssociated;
    }

    public void setUserIDAssociated(User userUIAssociated) {
        this.userIDAssociated = userUIAssociated;
    }
}
