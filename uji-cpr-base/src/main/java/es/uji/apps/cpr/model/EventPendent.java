package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CPR_VMC_EVENTOS_PENDIENTES", schema = "UJI_CONTROLPRESENCIA")
public class EventPendent implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Column(name = "CODE")
    private String code;

    @Column(name = "CURSO_ACA")
    private long cursoAca;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_EVENT")
    private Date endEvent;

    @Id
    @Column(name = "ID")
    private String eventId;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "NAME")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "START_EVENT")
    private Date startEvent;

    @Column(name = "GRUPO_COMP")
    private Integer grupoComp;

    @Column(name = "GRUPO_NOM")
    private String grupoNom;

    @Column(name = "AREA_ID")
    private Integer areaId;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "ID_TITULACION")
    private Integer titulacionId;

    @Column(name = "USUARIOS")
    private String usuarios;

    @Column(name = "TIPO_ID")
    private String tipoId;

    @ManyToOne
    @JoinColumn(name = "ID", insertable = false, updatable = false)
    private Event event;
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID", insertable = false, updatable = false)
    private User user;

    public Event getEvent()
    {
        return event;
    }

    public void setEvent(Event event)
    {
        this.event = event;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public long getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(long cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Date getEndEvent()
    {
        return endEvent;
    }

    public void setEndEvent(Date endEvent)
    {
        this.endEvent = endEvent;
    }

    public String getEventId()
    {
        return eventId;
    }

    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getStartEvent()
    {
        return startEvent;
    }

    public void setStartEvent(Date startEvent)
    {
        this.startEvent = startEvent;
    }

    public Integer getGrupoComp()
    {
        return grupoComp;
    }

    public void setGrupoComp(Integer grupoComp)
    {
        this.grupoComp = grupoComp;
    }

    public String getGrupoNom()
    {
        return grupoNom;
    }

    public void setGrupoNom(String grupoNom)
    {
        this.grupoNom = grupoNom;
    }

    public String getTipoId()
    {
        return tipoId;
    }

    public void setTipoId(String tipoId)
    {
        this.tipoId = tipoId;
    }

    public Integer getAreaId()
    {
        return areaId;
    }

    public void setAreaId(Integer areaId)
    {
        this.areaId = areaId;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Integer getTitulacionId()
    {
        return titulacionId;
    }

    public void setTitulacionId(Integer titulacionId)
    {
        this.titulacionId = titulacionId;
    }

    public String getUsuarios()
    {
        return usuarios;
    }

    public void setUsuarios(String usuarios)
    {
        this.usuarios = usuarios;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }
}
