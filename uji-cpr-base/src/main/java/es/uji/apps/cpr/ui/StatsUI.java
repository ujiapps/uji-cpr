package es.uji.apps.cpr.ui;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class StatsUI implements Serializable
{

    private List<String> titles;
    private List<String> dataTitles;
    private List<List<Integer>> values;
    private int unit;
    private Date startDate;
    private Date endDate;

    public StatsUI()
    {

    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public List<String> getDataTitles() {
        return dataTitles;
    }

    public void setDataTitles(List<String> dataTitles) {
        this.dataTitles = dataTitles;
    }

    public List<List<Integer>> getValues() {
        return values;
    }

    public void setValues(List<List<Integer>> values) {
        this.values = values;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


}
