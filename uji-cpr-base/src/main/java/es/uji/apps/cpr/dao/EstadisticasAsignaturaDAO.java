package es.uji.apps.cpr.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.cpr.model.*;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EstadisticasAsignaturaDAO extends BaseDAODatabaseImpl
{

    private QEstadisticasAsignatura qEstadisticasAsignatura = QEstadisticasAsignatura.estadisticasAsignatura;

    public List<EstadisticasAsignatura> getAllEstadisticas()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEstadisticasAsignatura).limit(100);

        return query.list(qEstadisticasAsignatura);
    }


    public List<EstadisticasAsignatura> searchEstadisticas(List<DepartmentsAreas> areas, Long personaId, String code, Paginacion paginacion, Long direcTitu, Boolean isAdmin)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery query2 = new JPAQuery(entityManager);
        JPAQuery query3 = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;

        List<Integer> areaIds = new ArrayList<>();
        for (DepartmentsAreas area : areas)
        {
            areaIds.add(area.getAreaId());
        }

        query.from(qEstadisticasAsignatura);


        if (ParamUtils.isNotNull(code) && !code.equals("-1"))
        {
            query.where(qEstadisticasAsignatura.grupoNombre.like('%' + code.toUpperCase() + '%'));
        }
        if (areaIds.size() >= 1 && !isAdmin)
        {
            query2.from(qEventUser).where(qEventUser.user.areaId.in(areaIds)
                    .and(qEventUser.tipoId.in("TE", "PR", "LA","SE"))
                    .and(qEventUser.type.eq("PDI"))
                    .and(qEventUser.event.grupoComp.isNull())
                    .and(qEventUser.event.startEvent.gt(GeneralUtils.getInitialDate())));

            query3.from(qEventUser).where(qEventUser.user.areaId.in(areaIds)
                    .and(qEventUser.tipoId.in("TE", "PR", "LA","SE"))
                    .and(qEventUser.type.eq("PDI"))
                    .and(qEventUser.event.grupoComp.isNotNull())
                    .and(qEventUser.event.startEvent.gt(GeneralUtils.getInitialDate())));
            if (ParamUtils.isNotNull(direcTitu))
            {
                QSubDegree qSubDegree = QSubDegree.subDegree;
                JPAQuery querytitu = new JPAQuery(entityManager);
                querytitu.from(qSubDegree).where(qSubDegree.dirDegree.userId.eq(direcTitu));
                List<String> codes = querytitu.distinct().list(qSubDegree.subjectId);
                query2.where(qEventUser.event.code.in(codes));
                query3.where(qEventUser.event.code.in(codes));
            }
            if (ParamUtils.isNotNull(personaId) && personaId != -1)
            {
                query2.where(qEventUser.user.userId.eq(personaId));
                query3.where(qEventUser.user.userId.eq(personaId));

            }

            List<String> asig = query2.distinct().list(qEventUser.event.eventId.substring(20).concat(qEventUser.user.areaId.stringValue()));
            List<Tuple> asigAgrupadas = query3.distinct().list(qEventUser.event.grupoComp, qEventUser.event.eventId.substring(26), qEventUser.user.areaId);

            List<String> asigAgrupadasString = new ArrayList<String>();
            for (Tuple i : asigAgrupadas)
            {
                asigAgrupadasString.add(i.get(qEventUser.event.grupoComp) + i.get(qEventUser.event.eventId.substring(26)) + i.get(qEventUser.user.areaId).toString());
            }
            if (asig.size() <= 0)
            {
                asig.add("-1");

            }
            if (asigAgrupadasString.size() <= 0)
            {
                asigAgrupadasString.add("-1");

            }
            query.where((qEstadisticasAsignatura.id.in(asig)
                    .or(qEstadisticasAsignatura.id.in(asigAgrupadasString)).and(qEstadisticasAsignatura.areaId.in(areaIds)))
            );
        }

        if (paginacion != null)
        {
            paginacion.setTotalCount(query.count());
            query.offset(paginacion.getStart()).limit(paginacion.getLimit());
        }
        query.orderBy(qEstadisticasAsignatura.grupoNombre.asc());
        return query.distinct().list(qEstadisticasAsignatura);
    }

    public List<EstadisticasAsignatura> searchEstadisticasArea(DepartmentsAreas area, Long personaId, String code, Long direcTitu)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery query2 = new JPAQuery(entityManager);
        JPAQuery query3 = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;

        query.from(qEstadisticasAsignatura).orderBy(qEstadisticasAsignatura.grupoNombre.asc());


        if (ParamUtils.isNotNull(code) && !code.equals("-1"))
        {
            query.where(qEstadisticasAsignatura.grupoNombre.like('%' + code.toUpperCase() + '%'));
        }

        query2.from(qEventUser).where(qEventUser.user.areaId.eq(area.getAreaId())
                .and(qEventUser.tipoId.in("TE", "PR", "LA","SE"))
                .and(qEventUser.type.eq("PDI"))
                .and(qEventUser.event.grupoComp.isNull())
                .and(qEventUser.event.startEvent.gt(GeneralUtils.getInitialDate())));

        query3.from(qEventUser).where(qEventUser.user.areaId.eq(area.getAreaId())
                .and(qEventUser.tipoId.in("TE", "PR", "LA","SE"))
                .and(qEventUser.type.eq("PDI"))
                .and(qEventUser.event.grupoComp.isNotNull())
                .and(qEventUser.event.startEvent.gt(GeneralUtils.getInitialDate())));
        if (ParamUtils.isNotNull(direcTitu))
        {
            QSubDegree qSubDegree = QSubDegree.subDegree;
            JPAQuery querytitu = new JPAQuery(entityManager);
            querytitu.from(qSubDegree).where(qSubDegree.dirDegree.userId.eq(direcTitu));
            List<String> codes = querytitu.distinct().list(qSubDegree.subjectId);
            query2.where(qEventUser.event.code.in(codes));
            query3.where(qEventUser.event.code.in(codes));
        }

        if (ParamUtils.isNotNull(personaId) && personaId != -1)
        {
            query2.where(qEventUser.user.userId.eq(personaId));
            query3.where(qEventUser.user.userId.eq(personaId));

        }
        List<String> asig = query2.distinct().list(qEventUser.event.eventId.substring(20).concat(String.valueOf(area.getAreaId())));
        List<Tuple> asigAgrupadas = query3.distinct().list(qEventUser.event.grupoComp, qEventUser.event.eventId.substring(26));

        List<String> asigAgrupadasString = new ArrayList<String>();
        for (Tuple i : asigAgrupadas)
        {
            asigAgrupadasString.add(i.get(qEventUser.event.grupoComp) + i.get(qEventUser.event.eventId.substring(26)) + String.valueOf(area.getAreaId()));
        }
        if (asig.size() <= 0)
        {
            asig.add("-1");

        }
        if (asigAgrupadasString.size() <= 0)
        {
            asigAgrupadasString.add("-1");

        }
        query.where((qEstadisticasAsignatura.id.in(asig)
                .or(qEstadisticasAsignatura.id.in(asigAgrupadasString)).and(qEstadisticasAsignatura.areaId.eq(area.getAreaId())))
        );
        query.orderBy(qEstadisticasAsignatura.grupoNombre.asc());
        return query.distinct().list(qEstadisticasAsignatura);
    }

    public List<EstadisticasProfesorTit> searchEstadisticasProfesor(DepartmentsAreas da, Long directorTitu)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstadisticasProfesorTit qEstadisticasProfesor = QEstadisticasProfesorTit.estadisticasProfesorTit;

        query.from(qEstadisticasProfesor).orderBy(qEstadisticasProfesor.nombre.asc());

        query.where(qEstadisticasProfesor.areaId.eq(Long.valueOf(da.getAreaId())));
        query.orderBy(qEstadisticasProfesor.nombre.asc());

        QSubDegree qSubDegree = QSubDegree.subDegree;
        JPASubQuery subquery = new JPASubQuery();
        subquery.from(qSubDegree).where(qSubDegree.dirDegree.userId.eq(directorTitu));

        query.where(qEstadisticasProfesor.titulacionId.in(subquery.distinct().list(qSubDegree.titulacion)));

        return query.list(qEstadisticasProfesor);
    }

    public List<EstadisticasProfesor> searchEstadisticasProfesor(DepartmentsAreas da)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstadisticasProfesor qEstadisticasProfesor = QEstadisticasProfesor.estadisticasProfesor;

        query.from(qEstadisticasProfesor).orderBy(qEstadisticasProfesor.nombre.asc());

        query.where(qEstadisticasProfesor.areaId.eq(Long.valueOf(da.getAreaId())));
        query.orderBy(qEstadisticasProfesor.nombre.asc());

        return query.list(qEstadisticasProfesor);
    }

    public List<EstadisticasProfesorTit> searchEstadisticasProfesor(List<DepartmentsAreas> areas, Long personaId, Long directorTitu)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstadisticasProfesorTit qEstadisticasProfesor = QEstadisticasProfesorTit.estadisticasProfesorTit;

        query.from(qEstadisticasProfesor).orderBy(qEstadisticasProfesor.area.asc(), qEstadisticasProfesor.nombre.asc());

        List<Long> areaIds = new ArrayList<>();
        for (DepartmentsAreas area : areas)
        {
            areaIds.add((long) area.getAreaId());
        }

        query.where(qEstadisticasProfesor.areaId.in(areaIds));

        if (ParamUtils.isNotNull(personaId) && personaId != -1)
        {
            query.where(qEstadisticasProfesor.personaId.eq(personaId));
        }
        QSubDegree qSubDegree = QSubDegree.subDegree;
        JPASubQuery subquery = new JPASubQuery();
        subquery.from(qSubDegree).where(qSubDegree.dirDegree.userId.eq(directorTitu));

        query.where(qEstadisticasProfesor.titulacionId.in(subquery.distinct().list(qSubDegree.titulacion)));
        return query.list(qEstadisticasProfesor);
    }

    public List<EstadisticasProfesor> searchEstadisticasProfesor(List<DepartmentsAreas> areas, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstadisticasProfesor qEstadisticasProfesor = QEstadisticasProfesor.estadisticasProfesor;

        query.from(qEstadisticasProfesor).orderBy(qEstadisticasProfesor.area.asc(), qEstadisticasProfesor.nombre.asc());

        List<Long> areaIds = new ArrayList<>();
        for (DepartmentsAreas area : areas)
        {
            areaIds.add((long) area.getAreaId());
        }

        query.where(qEstadisticasProfesor.areaId.in(areaIds));

        if (ParamUtils.isNotNull(personaId) && personaId != -1)
        {
            query.where(qEstadisticasProfesor.personaId.eq(personaId));
        }

        return query.list(qEstadisticasProfesor);
    }
}
