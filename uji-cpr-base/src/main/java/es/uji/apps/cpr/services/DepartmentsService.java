package es.uji.apps.cpr.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.dao.DepartmentsDAO;
import es.uji.apps.cpr.model.Departments;

@Service
public class DepartmentsService {

    @Autowired
    private DepartmentsDAO departmentsDAO;

    //@Role({ "ADMIN", "DIRECTOR_DE_DEPARTAMENTO" })
    public Departments getDepartmentByDepartments(Long connectedUserId) {
        return departmentsDAO.getDepartmentByDepartments(connectedUserId);
    }

    public List<Departments> getDepartamentsByArea(Integer areaId)
    {
        return departmentsDAO.getDepartamentsByArea(areaId);
    }

    public Departments getDepartment(Long id)
    {
       List<Departments> departments = departmentsDAO.get(Departments.class,id);
        if(departments!=null && !departments.isEmpty()){
            return departments.get(0);
        }
        return null;
    }
}
