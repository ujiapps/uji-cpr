package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CPR_VW_EVENTS2", schema = "UJI_CONTROLPRESENCIA")
public class Event implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Column(name = "CODE")
    private String code;

    @Column(name = "CURSO_ACA")
    private long cursoAca;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_EVENT")
    private Date endEvent;

    @Id
    @Column(name = "ID")
    private String eventId;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "NAME")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "START_EVENT")
    private Date startEvent;

    @OneToMany(mappedBy = "event")
    private Set<Check> checks;

    @OneToMany(mappedBy = "event")
    private Set<EventUser> eventUsers;

    @OneToMany(mappedBy = "event")
    private Set<Incidence> incidences;

    @Column(name = "GRUPO_COMP")
    private Integer grupoComp;

    @Column(name = "GRUPO_NOM")
    private String grupoNom;

    @Column(name = "TIPO_ID")
    private String tipoId;

    @OneToMany(mappedBy = "event")
    private Set<EventPendent> eventPendents;

    public Event()
    {
    }


    public Event(String eventId, String code, String name, Date startEvent, Date endEvent, String location, long cursoAca, Integer grupoComp, String grupoNom, String tipoId)
    {
        this.code = code;
        this.cursoAca = cursoAca;
        this.endEvent = endEvent;
        this.eventId = eventId;
        this.location = location;
        this.name = name;
        this.startEvent = startEvent;
        this.grupoComp = grupoComp;
        this.grupoNom = grupoNom;
        this.tipoId = tipoId;
    }

    public String getCode()
    {
        return this.code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public long getCursoAca()
    {
        return this.cursoAca;
    }

    public void setCursoAca(long cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Date getEndEvent()
    {
        return this.endEvent;
    }

    public void setEndEvent(Date endEvent)
    {
        this.endEvent = endEvent;
    }

    public String getEventId()
    {
        return eventId;
    }

    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }

    public String getLocation()
    {
        return this.location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getStartEvent()
    {
        return this.startEvent;
    }

    public void setStartEvent(Date startEvent)
    {
        this.startEvent = startEvent;
    }

    public Set<Check> getChecks()
    {
        return checks;
    }

    public void setChecks(Set<Check> checks)
    {
        this.checks = checks;
    }

    public Set<EventUser> getEventUsers()
    {
        return eventUsers;
    }

    public void setEventUsers(Set<EventUser> eventUsers)
    {
        this.eventUsers = eventUsers;
    }

    public Set<Incidence> getIncidences()
    {
        return incidences;
    }

    public void setIncidences(Set<Incidence> incidences)
    {
        this.incidences = incidences;
    }

    public Integer getGrupoComp()
    {
        return grupoComp;
    }

    public void setGrupoComp(Integer grupoComp)
    {
        this.grupoComp = grupoComp;
    }

    public String getGrupoNom()
    {
        return grupoNom;
    }

    public void setGrupoNom(String grupoNom)
    {
        this.grupoNom = grupoNom;
    }

    public String getTipoId()
    {
        return tipoId;
    }

    public void setTipoId(String tipoId)
    {
        this.tipoId = tipoId;
    }

    public Set<EventPendent> getEventPendents()
    {
        return eventPendents;
    }

    public void setEventPendents(Set<EventPendent> eventPendents)
    {
        this.eventPendents = eventPendents;
    }
}
