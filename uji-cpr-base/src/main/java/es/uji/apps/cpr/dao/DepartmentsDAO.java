package es.uji.apps.cpr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cpr.model.Departments;
import es.uji.apps.cpr.model.QDepartments;
import es.uji.apps.cpr.model.QDepartmentsAreas;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DepartmentsDAO extends BaseDAODatabaseImpl
{

    private List<Departments> userDepartaments;

    public Departments getDepartmentByDepartments(long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDepartments qDepartments = QDepartments.departments;

        return query.from(qDepartments).where(qDepartments.user.userId.eq(userId))
                .uniqueResult(qDepartments);
    }

    public List<Departments> getDepartments()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDepartments qDepartments = QDepartments.departments;

        return query.from(qDepartments).list(qDepartments);
    }

    public List<Departments> getDepartamentsByArea(Integer areaid)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDepartments qDepartments = QDepartments.departments;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;

        return query.from(qDepartments)
                .where(qDepartments.locationId.in(
                        new JPASubQuery().from(qDepartmentsAreas).where(qDepartmentsAreas.areaId.eq(areaid)).list(qDepartmentsAreas.depId)))
                .distinct().list(qDepartments);
    }
}
