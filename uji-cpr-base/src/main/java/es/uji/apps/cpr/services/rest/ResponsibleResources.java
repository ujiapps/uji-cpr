package es.uji.apps.cpr.services.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cpr.model.AreasResponsible;
import es.uji.apps.cpr.model.Departments;
import es.uji.apps.cpr.model.DepartmentsAreas;
import es.uji.apps.cpr.services.AreasResponsibleService;
import es.uji.apps.cpr.services.DepartmentsAreasService;
import es.uji.apps.cpr.services.DepartmentsService;
import es.uji.apps.cpr.services.VerifyUsersService;
import es.uji.apps.cpr.ui.AreasResponsibleUI;
import es.uji.apps.cpr.ui.ResponseItemUI;
import es.uji.apps.cpr.ui.ResponseListUI;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

@Path("responsibles")
public class ResponsibleResources extends CoreBaseService
{

    @InjectParam
    private AreasResponsibleService areasResponsibleService;

    @InjectParam
    private DepartmentsService departmentsService;

    @InjectParam
    private DepartmentsAreasService departmentsAreasService;

    @InjectParam
    private VerifyUsersService verifyUsersService;

    @GET
    public Response getAreasResponsibles(
            @QueryParam("start") @DefaultValue("-1") Integer start,
            @QueryParam("limit") @DefaultValue("25") Integer limit,
            @QueryParam("filter") @DefaultValue("") String filter,
            @QueryParam("sort") @DefaultValue("") String sort,
            @QueryParam("output") @DefaultValue("") String output,
            @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);

        Response.ResponseBuilder res = null;
        ResponseListUI<AreasResponsibleUI> response = new ResponseListUI<AreasResponsibleUI>();
        String msg = "OK";
        Boolean success = true;

        List<AreasResponsibleUI> areasResponsiblesList = new ArrayList<AreasResponsibleUI>();
        //TODO cambiar
        try
        {
            Boolean isAdmin = verifyUsersService.isAdmin(connectedUserId);
            List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();

            if (isAdmin)
            {
                departmentsAreas = departmentsAreasService.getDepartmentsAreas();
            } else
            {
                Departments department = departmentsService.getDepartmentByDepartments(connectedUserId);
                departmentsAreas = departmentsAreasService
                        .getDepartmentsAreasByDepartments(department.getLocationId());
            }

            List<Integer> areaIds = new ArrayList<Integer>();

            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                areaIds.add(departmentsArea.getAreaId());
            }

            List<AreasResponsible> areasResponsibles = null;

            if (output.length() > 0)
            {
                areasResponsibles = areasResponsibleService.getAreasResponsiblesByAreaIds(areaIds, connectedUserId, -1, limit, filterList, sortList);
            } else
            {
                areasResponsibles = areasResponsibleService.getAreasResponsiblesByAreaIds(areaIds, connectedUserId, start, limit, filterList, sortList);
            }

            for (AreasResponsible areasResponsible : areasResponsibles)
            {
                areasResponsiblesList.add(AreasResponsibleUI.toUI(areasResponsible));
            }

            response.setData(areasResponsiblesList);
            response.setCount(areasResponsibles.size());
        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);

        if (output.length() > 0)
        {
            res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = " + output + ".csv");

            res.entity(AreasResponsibleUI.toCSV(response.getData()));
            res.type(MediaType.TEXT_PLAIN);

        } else
        {
            res = Response.ok(response, MediaType.APPLICATION_JSON);
        }

        return res.build();
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public ResponseItemUI<AreasResponsibleUI> getAreasResponsibleById(@PathParam("id") long id,
                                                                      @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ResponseItemUI<AreasResponsibleUI> response = new ResponseItemUI<AreasResponsibleUI>();
        String msg = "OK";
        Boolean success = true;
        try
        {
            AreasResponsible areasResponsible = areasResponsibleService.getAreasResponsibleById(id);
            response.setData(AreasResponsibleUI.toUI(areasResponsible));
        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseItemUI<AreasResponsibleUI> createAreasResponsible(
            AreasResponsibleUI areasResponsibleUI)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ResponseItemUI<AreasResponsibleUI> response = new ResponseItemUI<AreasResponsibleUI>();
        String msg = "OK";
        Boolean success = false;
        Boolean isAdmin = false;

        try
        {
            isAdmin = verifyUsersService.isAdmin(connectedUserId);
            List<DepartmentsAreas> departmentsAreasVerif = new ArrayList<DepartmentsAreas>();

            if (isAdmin)
            {
                success = true;
            } else
            {
                Departments departmentVerif = departmentsService.getDepartmentByDepartments(connectedUserId);
                departmentsAreasVerif = departmentsAreasService
                        .getDepartmentsAreasByDepartments(departmentVerif.getLocationId());

                for (DepartmentsAreas departmentAreaVerif : departmentsAreasVerif)
                {
                    if (areasResponsibleUI.getAreaId() == departmentAreaVerif.getAreaId())
                    {
                        success = true;
                    }
                }
            }

            if (success)
            {
                AreasResponsible areasResponsible = areasResponsibleService.insert(AreasResponsibleUI.toModel(areasResponsibleUI), connectedUserId);
                response.setData(AreasResponsibleUI.toUI(areasResponsible));
            } else
            {
                response.setData(null);
            }

        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseItemUI<AreasResponsibleUI> updateAreasResponsible(
            AreasResponsibleUI areasResponsibleUI)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ResponseItemUI<AreasResponsibleUI> response = new ResponseItemUI<AreasResponsibleUI>();
        String msg = "OK";
        Boolean success = false;

        try
        {
            Boolean isAdmin = verifyUsersService.isAdmin(connectedUserId);
            List<DepartmentsAreas> departmentsAreasVerif = new ArrayList<DepartmentsAreas>();

            if (isAdmin)
            {
                success = true;
            } else
            {
                Departments departmentVerif = departmentsService.getDepartmentByDepartments(connectedUserId);
                departmentsAreasVerif = departmentsAreasService
                        .getDepartmentsAreasByDepartments(departmentVerif.getLocationId());
                for (DepartmentsAreas departmentAreaVerif : departmentsAreasVerif)
                {
                    if (areasResponsibleUI.getAreaId() == departmentAreaVerif.getAreaId())
                    {
                        success = true;
                    }
                }
            }

            if (success)
            {
                AreasResponsible areasResponsible = areasResponsibleService.update(AreasResponsibleUI.toModel(areasResponsibleUI), connectedUserId);
                response.setData(AreasResponsibleUI.toUI(areasResponsible));
            } else
            {
                response.setData(null);
            }

        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseItemUI<AreasResponsibleUI> deleteAreasResponsibleUI(@PathParam("id") long id)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ResponseItemUI<AreasResponsibleUI> response = new ResponseItemUI<AreasResponsibleUI>();
        String msg = "OK";
        Boolean success = false;

        try
        {
            Boolean isAdmin = verifyUsersService.isAdmin(connectedUserId);

            if (isAdmin)
            {
                success = true;
            } else
            {
                Departments departmentVerif = departmentsService.getDepartmentByDepartments(connectedUserId);

                List<DepartmentsAreas> departmentsAreasVerif = departmentsAreasService
                        .getDepartmentsAreasByDepartments(departmentVerif.getLocationId());

                AreasResponsible areasResponsible = areasResponsibleService.getAreasResponsibleById(id);
                AreasResponsibleUI res = AreasResponsibleUI.toUI(areasResponsible);

                for (DepartmentsAreas departmentAreaVerif : departmentsAreasVerif)
                {
                    if (res.getAreaId() == departmentAreaVerif.getAreaId())
                    {
                        success = true;
                        break;
                    }
                }
            }

            if (success)
            {
                areasResponsibleService.delete(id);
            }

        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

}
