package es.uji.apps.cpr.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.Key;
import es.uji.apps.cpr.model.QKey;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class KeyDAO extends BaseDAODatabaseImpl {

    public Key getKeyByKeyId(String key) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);

        QKey qKey = QKey.key1;

        query.from(qKey).where(qKey.key.eq(key));

        return query.uniqueResult(qKey);
    }

}
