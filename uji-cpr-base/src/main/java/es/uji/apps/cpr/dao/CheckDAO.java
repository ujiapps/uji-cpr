package es.uji.apps.cpr.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.Check;
import es.uji.apps.cpr.model.QCheck;
import es.uji.apps.cpr.model.QEvent;
import es.uji.apps.cpr.model.QIncidence;
import es.uji.apps.cpr.model.QTerminal;
import es.uji.apps.cpr.model.QUser;
import es.uji.apps.cpr.util.DynamicFilter;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class CheckDAO extends BaseDAODatabaseImpl
{

    static final long USERID_UJI_DEFAULT = 40000;
    static final int ONE_HOUR_MILIS = 3600000;
    static final int HALF_HOUR_MILIS = 1800000;


    public Check getCheckById(Long id) throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);
        QCheck qCheck = QCheck.check;
        QUser qUser = QUser.user;

        Check checkDTO = query.from(qCheck).leftJoin(qCheck.user).fetch().where(qCheck.id.eq(id)).uniqueResult(qCheck);

        if (checkDTO == null)
        {
            throw new GeneralCPRException(
                    "No se ha encontrado la entidad Check solicitada a traves del ID");
        }

        return checkDTO;
    }

    public List<Check> getChecksByUserId(long userId, int start, int limit,
                                         List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);
        QCheck qCheck = QCheck.check;

        query.from(qCheck).where(qCheck.user.userId.eq(userId));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qCheck.id);
        validFilters.put("userId", qCheck.user.userId);
        validFilters.put("itime", qCheck.itime);
        validFilters.put("key", qCheck.key);
        validFilters.put("service", qCheck.service);
        validFilters.put("time", qCheck.time);
        validFilters.put("type", qCheck.type);
        validFilters.put("eventId", qCheck.event.eventId);
        validFilters.put("terminal", qCheck.terminal);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qCheck.id);
        validSorts.put("userId", qCheck.user.userId);
        validSorts.put("itime", qCheck.itime);
        validSorts.put("key", qCheck.key);
        validSorts.put("service", qCheck.service);
        validSorts.put("time", qCheck.time);
        validSorts.put("type", qCheck.type);
        validSorts.put("eventId", qCheck.event.eventId);
        validSorts.put("terminal", qCheck.terminal);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }
        return query.list(qCheck);
    }

    public List<Check> getChecksByUserIdAndDate(long userId, Date date, int start, int limit)
            throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;
        QIncidence qIncidence = QIncidence.incidence;

        Date startDate = new Date(date.getTime() - ONE_HOUR_MILIS);
        Date endDate = new Date(date.getTime() + ONE_HOUR_MILIS);

        query.from(qCheck, qIncidence).where(
                (qCheck.user.userId.eq(userId).or(qCheck.user.userId.eq(USERID_UJI_DEFAULT))).and(qCheck.direction.isNull())
                        .and(qCheck.time.between(startDate, endDate))
                        .and(qCheck.id.ne(qIncidence.checkIn.id).or(qCheck.id.ne(qIncidence.checkOut.id))));
        try
        {
            return query.distinct().list(qCheck);
        } catch (Exception e)
        {
            e.printStackTrace();
            return new ArrayList<Check>();
        }
    }

    public List<Check> getChecksByEventId(String eventId, int start, int limit,
                                          List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;

        query.from(qCheck).where(qCheck.event.eventId.eq(eventId));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qCheck.id);
        validFilters.put("userId", qCheck.user.userId);
        validFilters.put("itime", qCheck.itime);
        validFilters.put("key", qCheck.key);
        validFilters.put("service", qCheck.service);
        validFilters.put("time", qCheck.time);
        validFilters.put("type", qCheck.type);
        validFilters.put("eventId", qCheck.event.eventId);
        validFilters.put("terminal", qCheck.terminal);
        validFilters.put("direction", qCheck.direction);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qCheck.id);
        validSorts.put("userId", qCheck.user.userId);
        validSorts.put("itime", qCheck.itime);
        validSorts.put("key", qCheck.key);
        validSorts.put("service", qCheck.service);
        validSorts.put("time", qCheck.time);
        validSorts.put("type", qCheck.type);
        validSorts.put("eventId", qCheck.event.eventId);
        validSorts.put("terminal", qCheck.terminal);
        validSorts.put("direction", qCheck.direction);
        if (sortList != null)
        {
            for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
            {
                query.orderBy(o);
            }
        }

        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qCheck);
    }

    public List<Check> getChecksByEvent(String eventId) throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);
        QCheck qCheck = QCheck.check;

        query.from(qCheck).where(qCheck.event.eventId.eq(eventId));
        return query.list(qCheck);
    }


    public List<Check> getChecksByEventIdPDI(String eventId, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
    {

        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;

        query.from(qCheck).where(
                qCheck.event.eventId.eq(eventId).and(qCheck.user.type.eq("PDI")));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qCheck.id);
        validFilters.put("userId", qCheck.user.userId);
        validFilters.put("itime", qCheck.itime);
        validFilters.put("key", qCheck.key);
        validFilters.put("service", qCheck.service);
        validFilters.put("time", qCheck.time);
        validFilters.put("type", qCheck.type);
        validFilters.put("eventId", qCheck.event.eventId);
        validFilters.put("terminal", qCheck.terminal);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qCheck.id);
        validSorts.put("userId", qCheck.user.userId);
        validSorts.put("itime", qCheck.itime);
        validSorts.put("key", qCheck.key);
        validSorts.put("service", qCheck.service);
        validSorts.put("time", qCheck.time);
        validSorts.put("type", qCheck.type);
        validSorts.put("eventId", qCheck.event.eventId);
        validSorts.put("terminal", qCheck.terminal);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qCheck);
    }

    public List<Check> getChecksByEventIdAndUserId(String eventId, long connectedUserId)
    {

        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;

        query.from(qCheck).where(
                qCheck.event.eventId.eq(eventId).and(qCheck.user.userId.eq(connectedUserId)));

        return query.list(qCheck);
    }

    public List<Check> getChecksByEventIdAndUserIds(String eventId, List<Long> userIds, int start,
                                                    int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;
        query.from(qCheck).where(
                qCheck.event.eventId.eq(eventId).and(qCheck.user.userId.in(userIds)));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qCheck.id);
        validFilters.put("userId", qCheck.user.userId);
        validFilters.put("itime", qCheck.itime);
        validFilters.put("key", qCheck.key);
        validFilters.put("service", qCheck.service);
        validFilters.put("time", qCheck.time);
        validFilters.put("type", qCheck.type);
        validFilters.put("eventId", qCheck.event.eventId);
        validFilters.put("terminal", qCheck.terminal);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qCheck.id);
        validSorts.put("userId", qCheck.user.userId);
        validSorts.put("itime", qCheck.itime);
        validSorts.put("key", qCheck.key);
        validSorts.put("service", qCheck.service);
        validSorts.put("time", qCheck.time);
        validSorts.put("type", qCheck.type);
        validSorts.put("eventId", qCheck.event.eventId);
        validSorts.put("terminal", qCheck.terminal);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qCheck);
    }

    public List<Check> getChecksByKeyIdAndDate(long keyId, Calendar date, int start, int limit,
                                               List<ItemFilter> filterList, List<ItemSort> sortList)
    {

        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;

        query.from(qCheck).where(
                qCheck.user.userId.eq(keyId).and(
                        qCheck.time.dayOfMonth().eq(date.get(Calendar.DAY_OF_MONTH))
                                .and(qCheck.time.month().eq(date.get(Calendar.MONTH) + 1))
                                .and(qCheck.time.year().eq(date.get(Calendar.YEAR)))));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qCheck.id);
        validFilters.put("userId", qCheck.user.userId);
        validFilters.put("itime", qCheck.itime);
        validFilters.put("key", qCheck.key);
        validFilters.put("service", qCheck.service);
        validFilters.put("time", qCheck.time);
        validFilters.put("type", qCheck.type);
        validFilters.put("eventId", qCheck.event.eventId);
        validFilters.put("terminal", qCheck.terminal.id);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qCheck.id);
        validSorts.put("userId", qCheck.user.userId);
        validSorts.put("itime", qCheck.itime);
        validSorts.put("key", qCheck.key);
        validSorts.put("service", qCheck.service);
        validSorts.put("time", qCheck.time);
        validSorts.put("type", qCheck.type);
        validSorts.put("eventId", qCheck.event.eventId);
        validSorts.put("terminal", qCheck.terminal);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qCheck);

    }

    public List<Check> getChecksByDate(Date startDate, Date endDate)
    {

        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;

        query.from(qCheck).leftJoin(qCheck.user).orderBy(qCheck.time.asc());

        if (startDate != null || endDate != null)
        {
            query.where(qCheck.time.between(startDate, endDate));
        }

        return query.list(qCheck);
    }

    public List<Check> getChecksByLocation(String location, Date startEvent, Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCheck qCheck = QCheck.check;
        QUser qUser = QUser.user;
        QEvent qEvent = QEvent.event;
        QTerminal qTerminal = QTerminal.terminal;

        Date startDate = new Date(startEvent.getTime() - HALF_HOUR_MILIS);
        Date endDate = new Date(startEvent.getTime() + HALF_HOUR_MILIS);

        query.from(qCheck).join(qCheck.terminal, qTerminal).where(qCheck.direction.isNull()
                .and(qCheck.user.userId.ne(USERID_UJI_DEFAULT)).and(qCheck.user.userId.ne(userId)).and(qTerminal.location.eq(location))
                .and(qCheck.time.between(startDate, endDate)));

        return query.distinct().list(qCheck);

    }

    public List<Check> searchChecks(String location, String fecha, String userLogin) throws ParseException
    {
        JPAQuery query = new JPAQuery(entityManager);


        QCheck qCheck = QCheck.check;
        QUser qUser = QUser.user;
        QEvent qEvent = QEvent.event;
        QTerminal qTerminal = QTerminal.terminal;


        query.from(qCheck).join(qCheck.terminal, qTerminal).where(qCheck.time.gt(GeneralUtils.getInitialDate()));

        if (ParamUtils.isNotNull(fecha))
        {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date beginday = df.parse(fecha);
            Date endday = df2.parse(fecha + " 23:59:59");
            query.where(qCheck.time.between(beginday, endday));
        }

        if (ParamUtils.isNotNull(userLogin))
        {
            query.where(qCheck.user.login.like('%' + userLogin + '%'));
        }

        if (ParamUtils.isNotNull(location))
        {
            query.where(qTerminal.location.like('%' + location + '%'));
        }

        return query.distinct().list(qCheck);
    }
}
