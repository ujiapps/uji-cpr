package es.uji.apps.cpr.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.dao.StatsDAO;
import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.model.Incidence;

@Service
public class StatsService {

    static final Logger logger = LoggerFactory.getLogger(StatsService.class);

    @Autowired
    private StatsDAO statsDAO;

    public List<Event> getIncidencesByDegree(Long connectedUserId) {
        return statsDAO.getIncidencesByDegree(connectedUserId);
    }

    public List<Incidence> getIncidencesByDepartament(Integer departamentId) {

        return statsDAO.getIncidencesByDepartament(departamentId);
    }
}
