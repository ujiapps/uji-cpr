package es.uji.apps.cpr.ui;

public class AsignaturaUI
{
    private String id;
    private String code;
    private String grupo;
    private String nombreAsig;
    private Long totalSesiones;
    private Long totalIncidenciasCreadas;
    private Long sinIncidencia;
    private Long pendientes;
    private Long incidenciasAbiertas;
    private Long incidenciasAuto;
    private Long incidenciasRevocadas;
    private Long incidenciasResueltas;
    private Integer sinIncidenciaPor;
    private Integer pendientesPor;
    private Integer incidenciasAbiertasPor;
    private Integer incidenciasResueltasPor;
    private Integer incidenciasRevocadasPor;
    private Integer incidenciasAutoResPor;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public String getNombreAsig()
    {
        return nombreAsig;
    }

    public void setNombreAsig(String nombreAsig)
    {
        this.nombreAsig = nombreAsig;
    }

    public Long getTotalSesiones()
    {
        return totalSesiones;
    }

    public void setTotalSesiones(Long totalSesiones)
    {
        this.totalSesiones = totalSesiones;
    }

    public Long getTotalIncidenciasCreadas()
    {
        return totalIncidenciasCreadas;
    }

    public void setTotalIncidenciasCreadas(Long totalIncidenciasCreadas)
    {
        this.totalIncidenciasCreadas = totalIncidenciasCreadas;
    }

    public Long getSinIncidencia()
    {
        return sinIncidencia;
    }

    public void setSinIncidencia(Long sinIncidencia)
    {
        this.sinIncidencia = sinIncidencia;
    }

    public Long getPendientes()
    {
        return pendientes;
    }

    public void setPendientes(Long pendientes)
    {
        this.pendientes = pendientes;
    }

    public Long getIncidenciasAbiertas()
    {
        return incidenciasAbiertas;
    }

    public void setIncidenciasAbiertas(Long incidenciasAbiertas)
    {
        this.incidenciasAbiertas = incidenciasAbiertas;
    }

    public Long getIncidenciasAuto()
    {
        return incidenciasAuto;
    }

    public void setIncidenciasAuto(Long incidenciasAuto)
    {
        this.incidenciasAuto = incidenciasAuto;
    }

    public Long getIncidenciasRevocadas()
    {
        return incidenciasRevocadas;
    }

    public void setIncidenciasRevocadas(Long incidenciasRevocadas)
    {
        this.incidenciasRevocadas = incidenciasRevocadas;
    }

    public Long getIncidenciasResueltas()
    {
        return incidenciasResueltas;
    }

    public void setIncidenciasResueltas(Long incidenciasResueltas)
    {
        this.incidenciasResueltas = incidenciasResueltas;
    }

    public Integer getSinIncidenciaPor()
    {
        return sinIncidenciaPor;
    }

    public void setSinIncidenciaPor(Integer sinIncidenciaPor)
    {
        this.sinIncidenciaPor = sinIncidenciaPor;
    }

    public Integer getPendientesPor()
    {
        return pendientesPor;
    }

    public void setPendientesPor(Integer pendientesPor)
    {
        this.pendientesPor = pendientesPor;
    }

    public Integer getIncidenciasAbiertasPor()
    {
        return incidenciasAbiertasPor;
    }

    public void setIncidenciasAbiertasPor(Integer incidenciasAbiertasPor)
    {
        this.incidenciasAbiertasPor = incidenciasAbiertasPor;
    }

    public Integer getIncidenciasResueltasPor()
    {
        return incidenciasResueltasPor;
    }

    public void setIncidenciasResueltasPor(Integer incidenciasResueltasPor)
    {
        this.incidenciasResueltasPor = incidenciasResueltasPor;
    }

    public Integer getIncidenciasRevocadasPor()
    {
        return incidenciasRevocadasPor;
    }

    public void setIncidenciasRevocadasPor(Integer incidenciasRevocadasPor)
    {
        this.incidenciasRevocadasPor = incidenciasRevocadasPor;
    }

    public Integer getIncidenciasAutoResPor()
    {
        return incidenciasAutoResPor;
    }

    public void setIncidenciasAutoResPor(Integer incidenciasAutoResPor)
    {
        this.incidenciasAutoResPor = incidenciasAutoResPor;
    }
}
