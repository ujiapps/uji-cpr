package es.uji.apps.cpr.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.dao.LogDAO;
import es.uji.apps.cpr.model.Log;

@Service
public class LogService
{

    private LogDAO logDAO;

    @Autowired
    public LogService(LogDAO logDAO)
    {
        this.logDAO = logDAO;
    }

    public List<Log> get()
    {
        return logDAO.get(Log.class);
    }

    public Log getById(Long logId)
    {
        return logDAO.get(Log.class, logId).get(0);
    }

    public Log insert(Log log)
    {
        return logDAO.insert(log);
    }

    public Log update(Log log)
    {
        return logDAO.update(log);
    }

    public void delete(Long logId)
    {
        logDAO.delete(Log.class, logId);
    }
}
