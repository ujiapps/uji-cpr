package es.uji.apps.cpr.services.rest;


import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.cpr.model.DepartmentsAreas;
import es.uji.apps.cpr.model.User;
import es.uji.apps.cpr.services.*;
import es.uji.apps.cpr.ui.AreasUI;
import es.uji.apps.cpr.ui.ResponseListUI;
import es.uji.apps.cpr.ui.UserUI;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("areas")
public class AreasResources extends CoreBaseService
{

    @InjectParam
    private AreasResponsibleService areasResponsibleService;

    @InjectParam
    private DepartmentsService departmentsService;

    @InjectParam
    private DepartmentsAreasService departmentsAreasService;

    @InjectParam
    private UserService userService;

    @InjectParam
    private VerifyUsersService verifyUsersService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<AreasUI> getAreas(@QueryParam("start") @DefaultValue("-1") Integer start,
                                            @QueryParam("limit") @DefaultValue("25") Integer limit,
                                            @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ResponseListUI<AreasUI> response = new ResponseListUI<AreasUI>();
        String msg = "OK";
        Boolean success = true;
        Boolean isAdmin = false;
        try
        {
            List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();
            isAdmin = verifyUsersService.isAdmin(connectedUserId);
            if (isAdmin)
            {
                departmentsAreas = departmentsAreasService.getDepartmentsAreas();
            } else if(verifyUsersService.isTitDir(connectedUserId)){
                departmentsAreas = departmentsAreasService.getDepartmentsAreasByTit(connectedUserId);
            } else
            {
                departmentsAreas = departmentsAreasService.getDepartmentsAreasByUserId(connectedUserId);
            }
            List<AreasUI> res = new ArrayList<AreasUI>();

            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                res.add(AreasUI.toUI(departmentsArea, departmentsArea.getAreasResponsible().size()));
            }
            response.setData(res);
            response.setCount(res.size());

        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<AreasUI> getAreasByRootId(@QueryParam("start") @DefaultValue("-1") Integer start,
                                            @QueryParam("limit") @DefaultValue("25") Integer limit,
                                            @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ResponseListUI<AreasUI> response = new ResponseListUI<AreasUI>();
        String msg = "OK";
        Boolean success = true;
        Boolean isAdmin = false;
        try
        {
            List<DepartmentsAreas> departmentsAreas = new ArrayList<DepartmentsAreas>();
            isAdmin = verifyUsersService.isAdmin(connectedUserId);
            if (isAdmin)
            {
                departmentsAreas = departmentsAreasService.getDepartmentsAreas();
            } else if(verifyUsersService.isTitDir(connectedUserId)){
                departmentsAreas = departmentsAreasService.getDepartmentsAreasByTit(connectedUserId);
            } else
            {
                departmentsAreas = departmentsAreasService.getDepartmentsAreasByUserId(connectedUserId);
            }
            List<AreasUI> res = new ArrayList<AreasUI>();

            for (DepartmentsAreas departmentsArea : departmentsAreas)
            {
                res.add(AreasUI.toUI(departmentsArea, departmentsArea.getAreasResponsible().size()));
            }
            response.setData(res);
            response.setCount(res.size());

        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/users")
    public ResponseListUI<UserUI> getAttendanceByEventId(@PathParam("id") Integer id,
                                                         @QueryParam("start") @DefaultValue("0") Integer start,
                                                         @QueryParam("limit") @DefaultValue("25") Integer limit,
                                                         @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException
    {

        ResponseListUI<UserUI> response = new ResponseListUI<UserUI>();
        Boolean success = true;
        String msg = "OK";
        try
        {
            List<User> userList = new ArrayList<User>();

            userList = userService.getUsersByAreaId(id, start, limit);
            List<UserUI> userUIList = new ArrayList<UserUI>();

            userUIList.addAll(UserUI.toUI(userList));

            response.setData(userUIList);
            response.setCount(userUIList.size());
        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setMessage(msg);
        response.setSuccess(success);
        return response;
    }

}
