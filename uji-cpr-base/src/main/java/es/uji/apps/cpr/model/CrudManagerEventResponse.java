package es.uji.apps.cpr.model;

import es.uji.apps.cpr.ui.EventUI;
import es.uji.apps.cpr.ui.ResourceUI;

import java.util.List;

public class CrudManagerEventResponse {
    private EventSection events;
    private ResourceSection resources;
    private Boolean success;
    private String message;
    private int count;

    // Constructor, getters y setters

    public static class EventSection {
        private List<EventUI> rows;

        public EventSection(List<EventUI> rows) {
            this.rows = rows;
        }

        public List<EventUI> getRows() {
            return rows;
        }

        public void setRows(List<EventUI> rows) {
            this.rows = rows;
        }
    }

    public static class ResourceSection {
        private List<ResourceUI> rows;

        public ResourceSection(List<ResourceUI> rows) {
            this.rows = rows;
        }

        public List<ResourceUI> getRows() {
            return rows;
        }

        public void setRows(List<ResourceUI> rows) {
            this.rows = rows;
        }
    }

    public EventSection getEvents() {
        return events;
    }

    public void setEvents(List<EventUI> events) {
        this.events = new EventSection(events);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ResourceSection getResources() {
        return resources;
    }

    public void setResources(List<ResourceUI> resources) {
        this.resources = new ResourceSection(resources);
    }
}