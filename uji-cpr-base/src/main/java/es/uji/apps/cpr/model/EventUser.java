package es.uji.apps.cpr.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CPR_VW_EVENTS2_USERS", schema = "UJI_CONTROLPRESENCIA")
public class EventUser implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @ManyToOne
    @JoinColumn(name = "EVENT_ID")
    private Event event;

    @Column(name = "NAME")
    private String name;

    @Id
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private User user;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "TIPO_ID")
    private String tipoId;

    public EventUser()
    {
    }

    public Event getEvent()
    {
        return event;
    }

    public void setEvent(Event event)
    {
        this.event = event;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public String getSurname()
    {
        return this.surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public String getType()
    {
        return this.type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getTipoId()
    {
        return tipoId;
    }

    public void setTipoId(String tipoId)
    {
        this.tipoId = tipoId;
    }
}
