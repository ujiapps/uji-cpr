package es.uji.apps.cpr.util;

public class CSVUtils {

    static public String parseStatus(int status) {

        switch (status) {
            case 0:
                return "Obert";
            case 1:
                return "Auto-resolt";
            case 2:
                return "Revocat";
            case 3:
                return "Resolt";
            default:
                return null;
        }

    }

    static public String parseType(byte type) {

        Boolean isEnabled0 = BitWise.isBitEnabled(Byte.valueOf(type), 0);
        Boolean isEnabled1 = BitWise.isBitEnabled(Byte.valueOf(type), 1);
        Boolean isEnabled2 = BitWise.isBitEnabled(Byte.valueOf(type), 2);
        Boolean isEnabled3 = BitWise.isBitEnabled(Byte.valueOf(type), 3);
        Boolean isEnabled4 = BitWise.isBitEnabled(Byte.valueOf(type), 4);
        Boolean isEnabled5 = BitWise.isBitEnabled(Byte.valueOf(type), 5);

        StringBuilder sB = new StringBuilder();

        if (isEnabled0) {
            sB.append("Sense entrada,");
        }

        if (isEnabled1) {
            sB.append("Sense sortida,");
        }

        if (isEnabled2) {
            sB.append("Duració incorrecta,");
        }

        if (isEnabled3) {
            sB.append("Targeta genérica,");
        }

        if (isEnabled4) {
            sB.append("Altra ubicació,");
        }

        if (isEnabled5) {
            sB.append("Event no associat,");
        }

        return sB.toString();
    }

    static public String parseType(int type) {

        String result = "";

        if (type == 0) {
            result = "Sense entrada";
        }

        if (type == 1) {
            result = "Sense sortida";
        }

        return result;
    }
}