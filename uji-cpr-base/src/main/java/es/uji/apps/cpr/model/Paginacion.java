package es.uji.apps.cpr.model;

public class Paginacion
{
    private Long start;
    private Long limit;
    private Long totalCount;

    public Paginacion(Long start, Long limit)
    {
        this.start = start;
        this.limit = limit;
    }

    public Long getStart()
    {
        return start;
    }

    public void setStart(Long start)
    {
        this.start = start;
    }

    public Long getLimit()
    {
        return limit;
    }

    public void setLimit(Long limit)
    {
        this.limit = limit;
    }

    public Long getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(Long totalCount)
    {
        this.totalCount = totalCount;
    }
}