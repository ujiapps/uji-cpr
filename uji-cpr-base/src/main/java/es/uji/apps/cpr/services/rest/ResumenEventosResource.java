package es.uji.apps.cpr.services.rest;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.DepartmentsAreas;
import es.uji.apps.cpr.model.EstadisticasAsignatura;
import es.uji.apps.cpr.model.EstadisticasProfesor;
import es.uji.apps.cpr.model.Paginacion;
import es.uji.apps.cpr.services.DepartmentsAreasService;
import es.uji.apps.cpr.services.DepartmentsService;
import es.uji.apps.cpr.services.EstadisticasAsignaturaService;
import es.uji.apps.cpr.services.ResumenEventosService;
import es.uji.apps.cpr.services.UserService;
import es.uji.apps.cpr.services.VerifyUsersService;
import es.uji.apps.cpr.ui.EstadisticasAreaUI;
import es.uji.apps.cpr.ui.EstadisticasProfesorUI;
import es.uji.apps.cpr.ui.EstadisticasUI;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

@Path("resumen")
public class ResumenEventosResource extends CoreBaseService
{
    @InjectParam
    private ResumenEventosService resumenEventosService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getResumenEventos(@QueryParam("personaId") Long personaId, @QueryParam("codigo") String codigo)
    {
        return UIEntity.toUI(resumenEventosService.getResumenEventos(personaId,codigo));
    }


}
