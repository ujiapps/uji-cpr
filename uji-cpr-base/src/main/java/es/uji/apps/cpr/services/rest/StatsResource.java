package es.uji.apps.cpr.services.rest;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cpr.model.Check;
import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.model.Incidence;
import es.uji.apps.cpr.services.CheckService;
import es.uji.apps.cpr.services.StatsService;
import es.uji.apps.cpr.ui.ResponseItemUI;
import es.uji.apps.cpr.ui.StatsUI;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

@Path("stats")
public class StatsResource extends CoreBaseService {
    @InjectParam
    private CheckService checkService;

    @InjectParam
    private StatsService statsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("incidences/titulacion")
    public ResponseItemUI<StatsUI> getIncidences(@QueryParam("startDate") @DefaultValue("") String startDate,
                                                 @QueryParam("endDate") @DefaultValue("") String endDate,
                                                 @QueryParam("unit") @DefaultValue("1") int unit) throws IOException {
        ResponseItemUI<StatsUI> response = new ResponseItemUI<StatsUI>();
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String msg = "OK";
        Boolean success = true;
        try {

            List<Event> eventsList = statsService.getIncidencesByDegree(connectedUserId);


            response.setData(null);

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("incidences/departament")
    public ResponseItemUI<StatsUI> getIncidencesByDepartament(@QueryParam("startDate") @DefaultValue("") String startDate,
                                                              @QueryParam("endDate") @DefaultValue("") String endDate,
                                                              @QueryParam("unit") @DefaultValue("1") int unit) throws IOException {
        ResponseItemUI<StatsUI> response = new ResponseItemUI<StatsUI>();
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String msg = "OK";
        Boolean success = true;
        try {

            List<Incidence> incidenceList = statsService.getIncidencesByDepartament(0);


            response.setData(null);

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("checks/users")
    public ResponseItemUI<StatsUI> getChecksUsers(@QueryParam("startDate") @DefaultValue("") String startDate,
                                                  @QueryParam("endDate") @DefaultValue("") String endDate,
                                                  @QueryParam("unit") @DefaultValue("1") int unit,
                                                  @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {


        ResponseItemUI<StatsUI> response = new ResponseItemUI<StatsUI>();
        String msg = "OK";
        Boolean success = true;
        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date startDateFormatted = null;
            Date endDateFormatted = null;
            try {
                if (!startDate.equals(""))
                    startDateFormatted = format.parse(startDate);
                if (!endDate.equals(""))
                    endDateFormatted = format.parse(endDate);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            List<Check> checks = checkService.getChecksByDate(startDateFormatted, endDateFormatted);
            List<String> dataTitles = new ArrayList<String>();
            dataTitles.add("Valid User");
            dataTitles.add("Generic User");
            dataTitles.add("Unknown User");

            StatsUI statsUI = composeStatsUI(unit, checks, dataTitles, 1);
            statsUI.setStartDate(startDateFormatted);
            statsUI.setEndDate(endDateFormatted);
            response.setData(statsUI);

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("checks/events")
    public ResponseItemUI<StatsUI> getChecksEvents(@QueryParam("startDate") @DefaultValue("") String startDate,
                                                   @QueryParam("endDate") @DefaultValue("") String endDate,
                                                   @QueryParam("unit") @DefaultValue("1") int unit,
                                                   @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {


        ResponseItemUI<StatsUI> response = new ResponseItemUI<StatsUI>();
        String msg = "OK";
        Boolean success = true;
        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date startDateFormatted = null;
            Date endDateFormatted = null;
            try {
                if (!startDate.equals(""))
                    startDateFormatted = format.parse(startDate);
                if (!endDate.equals(""))
                    endDateFormatted = format.parse(endDate);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            List<Check> checks = checkService.getChecksByDate(startDateFormatted, endDateFormatted);
            List<String> dataTitles = new ArrayList<String>();
            dataTitles.add("Events Checked");
            dataTitles.add("Events Unchecked");

            StatsUI statsUI = composeStatsUI(unit, checks, dataTitles, 2);
            statsUI.setStartDate(startDateFormatted);
            statsUI.setEndDate(endDateFormatted);
            response.setData(statsUI);

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("checks/faculty")
    public ResponseItemUI<StatsUI> getChecksFaculty(@QueryParam("startDate") @DefaultValue("") String startDate,
                                                    @QueryParam("endDate") @DefaultValue("") String endDate,
                                                    @QueryParam("unit") @DefaultValue("1") int unit,
                                                    @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {


        ResponseItemUI<StatsUI> response = new ResponseItemUI<StatsUI>();
        String msg = "OK";
        Boolean success = true;
        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date startDateFormatted = null;
            Date endDateFormatted = null;
            try {
                if (!startDate.equals(""))
                    startDateFormatted = format.parse(startDate);
                if (!endDate.equals(""))
                    endDateFormatted = format.parse(endDate);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            List<Check> checks = checkService.getChecksByDate(startDateFormatted, endDateFormatted);
            List<String> dataTitles = new ArrayList<String>();
            dataTitles.add("Tecnologicas");
            dataTitles.add("Juridicas");
            dataTitles.add("Humanas");
            dataTitles.add("Salud");
            dataTitles.add("Otras");

            StatsUI statsUI = composeStatsUI(unit, checks, dataTitles, 3);
            statsUI.setStartDate(startDateFormatted);
            statsUI.setEndDate(endDateFormatted);
            response.setData(statsUI);

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    private StatsUI composeStatsUI(int unit, List<Check> checks, List<String> dataTitles, int type) {

        GregorianCalendar calendar = new GregorianCalendar();

        List<String> titles = new ArrayList<String>();

        List<List<Integer>> values = new ArrayList<List<Integer>>();

        int unitCalendar = 0;
        SimpleDateFormat sdf = new SimpleDateFormat();

        if (unit == 1) {
            unitCalendar = Calendar.DAY_OF_MONTH;
            sdf.applyPattern("dd/MM/yyyy");
        } else if (unit == 2) {
            unitCalendar = Calendar.MONTH;
            sdf.applyPattern("MM/yyyy");
        } else if (unit == 3) {
            unitCalendar = Calendar.YEAR;
            sdf.applyPattern("yyyy");
        }

        calendar.setTime(checks.get(0).getTime());
        int unitInternal = calendar.get(unitCalendar);

        titles.add(sdf.format(calendar.getTime()));

        List<Integer> valuesInternal = new ArrayList<Integer>();

        initiateArray(valuesInternal, type);

        for (Check check : checks) {

            calendar.setTime(check.getTime());
            if (unitInternal == calendar.get(unitCalendar)) {

                setArrayValues(check, valuesInternal, type);

            } else {
                values.add(valuesInternal);
                valuesInternal = new ArrayList<Integer>();
                initiateArray(valuesInternal, type);

                setArrayValues(check, valuesInternal, type);

                unitInternal = calendar.get(unitCalendar);
                titles.add(sdf.format(check.getTime()));
            }

        }
        values.add(valuesInternal);

        StatsUI statsUI = new StatsUI();
        statsUI.setTitles(titles);
        statsUI.setDataTitles(dataTitles);
        statsUI.setValues(values);
        statsUI.setUnit(unit);

        return statsUI;
    }

    private void initiateArray(List<Integer> valuesInternal, int type) {
        if (type == 1) {
            valuesInternal.add(0);
            valuesInternal.add(0);
            valuesInternal.add(0);
        } else if (type == 2) {
            valuesInternal.add(0);
            valuesInternal.add(0);
        } else if (type == 3) {
            valuesInternal.add(0);
            valuesInternal.add(0);
            valuesInternal.add(0);
            valuesInternal.add(0);
            valuesInternal.add(0);
        }

    }

    private void setArrayValues(Check check, List<Integer> valuesInternal, int type) {

        int valueInt = 0;

        if (type == 1) {
            if (check.getUser() == null) {
                valueInt = valuesInternal.get(2);
                valueInt = valueInt + 1;
                valuesInternal.set(2, valueInt);
            } else if (check.getUser().getUserId() == 40000) {
                valueInt = valuesInternal.get(1);
                valueInt = valueInt + 1;
                valuesInternal.set(1, valueInt);
            } else {
                valueInt = valuesInternal.get(0);
                valueInt = valueInt + 1;
                valuesInternal.set(0, valueInt);
            }
        } else if (type == 2) {
            if (check.getEvent() == null) {
                valueInt = valuesInternal.get(1);
                valueInt = valueInt + 1;
                valuesInternal.set(1, valueInt);
            } else {
                valueInt = valuesInternal.get(0);
                valueInt = valueInt + 1;
                valuesInternal.set(0, valueInt);
            }
        } else if (type == 3) {
            if (check.getTerminal().getLocation().contains("TD")) {
                valueInt = valuesInternal.get(0);
                valueInt = valueInt + 1;
                valuesInternal.set(0, valueInt);
            } else if (check.getTerminal().getLocation().contains("JB") || check.getTerminal().getLocation().contains("JA")) {
                valueInt = valuesInternal.get(1);
                valueInt = valueInt + 1;
                valuesInternal.set(1, valueInt);
            } else if (check.getTerminal().getLocation().contains("HA") || check.getTerminal().getLocation().contains("HC")) {
                valueInt = valuesInternal.get(2);
                valueInt = valueInt + 1;
                valuesInternal.set(2, valueInt);
            } else if (check.getTerminal().getLocation().contains("HD")) {
                valueInt = valuesInternal.get(3);
                valueInt = valueInt + 1;
                valuesInternal.set(3, valueInt);
            } else {
                valueInt = valuesInternal.get(4);
                valueInt = valueInt + 1;
                valuesInternal.set(4, valueInt);
            }
        }

    }
}
