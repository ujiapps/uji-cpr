package es.uji.apps.cpr.services.rest;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.mysema.query.Tuple;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.*;
import es.uji.apps.cpr.services.CheckService;
import es.uji.apps.cpr.services.DirDegreeService;
import es.uji.apps.cpr.services.EventService;
import es.uji.apps.cpr.services.EventUserService;
import es.uji.apps.cpr.services.KeyService;
import es.uji.apps.cpr.services.SubDegreeService;
import es.uji.apps.cpr.services.UserService;
import es.uji.apps.cpr.services.VerifyUsersService;
import es.uji.apps.cpr.ui.*;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("events")
public class EventResources extends CoreBaseService
{

    @InjectParam
    private CheckService checkService;

    @InjectParam
    private EventService eventService;

    @InjectParam
    private EventUserService eventUserService;

    @InjectParam
    private KeyService keyService;

    @InjectParam
    private UserService userService;
    @InjectParam
    private DirDegreeService dirDegreeService;
    @InjectParam
    private SubDegreeService subDegreeService;
    @InjectParam
    private VerifyUsersService verifyUsersService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CrudManagerEventResponse getEvents(
            @QueryParam("startDate") @DefaultValue("") String startDate,
            @QueryParam("endDate") @DefaultValue("") String endDate,
            @QueryParam("start") @DefaultValue("0") Integer start,
            @QueryParam("limit") @DefaultValue("25") Integer limit,
            @QueryParam("filter") @DefaultValue("") String filter,
            @QueryParam("sort") @DefaultValue("") String sort,
            @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException
    {
        CrudManagerEventResponse response = new CrudManagerEventResponse();
        Boolean success = true;
        String msg = "OK";
        try
        {
            Long userId = AccessManager.getConnectedUserId(request);
            if(!verifyUsersService.isAlumno(userId)){
            List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
            List<ItemSort> sortList = ItemSort.jsonDecode(sort);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date startDateFormatted = null;
            Date endDateFormatted = null;
            try
            {
                if (!startDate.equals(""))
                    startDateFormatted = format.parse(startDate);
                if (!endDate.equals(""))
                    endDateFormatted = format.parse(endDate);
            } catch (ParseException ex)
            {
                ex.printStackTrace();
            }

            List<EventUI> eventUIList = eventService.getEventsByUserId(userId, startDateFormatted,
                    endDateFormatted, start, limit, filterList, sortList);

            ResourceUI resourceUI = new ResourceUI(0, "Calendar.NONE", "#cf2424");
            List<ResourceUI> resourceUIList = new ArrayList<>();
            resourceUIList.add(resourceUI);
            ResourceUI resourceUI2 = new ResourceUI(1, "Calendar.HALF", "#eda12a");
            resourceUIList.add(resourceUI2);
            ResourceUI resourceUI3 = new ResourceUI(2, "Calendar.ALL", "#83ad47");
            resourceUIList.add(resourceUI3);
            ResourceUI resourceUI4 = new ResourceUI(3, "Calendar.FUTURE", "#64b9d9");
            resourceUIList.add(resourceUI4);

            response.setEvents(eventUIList);
            response.setResources(resourceUIList);
            response.setCount(eventUIList.size());
            }else{
               msg="No access permes.";
            }
        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setMessage(msg);
        response.setSuccess(success);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/estimated/in")
    public ResponseListUI<CheckUI> getEstimatedChecksByEventId(
            @PathParam("id") String id, @QueryParam("start") @DefaultValue("0") Integer start,
            @QueryParam("limit") @DefaultValue("25") Integer limit)
    {
        ResponseListUI<CheckUI> response = new ResponseListUI<CheckUI>();
        Boolean success = true;
        String msg = "OK";
        try
        {
            Long userId = AccessManager.getConnectedUserId(request);

            List<CheckUI> checksUI = new ArrayList<CheckUI>();

            Event event = eventService.getEventById(id);
            List<Check> mineList = checkService.getAccessChecksByUserIdAndDate(userId, event.getStartEvent(), start, limit);
            for (Check check : mineList)
            {
                User user = check.getUser();
                checksUI.add(CheckUI.toUI(check, user, null, -1));
            }

            response.setData(checksUI);
            response.setCount(checksUI.size());
        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setMessage(msg);
        response.setSuccess(success);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/estimated/out")
    public ResponseListUI<CheckUI> getExitChecksByEventId(@PathParam("id") String id,
                                                          @QueryParam("start") @DefaultValue("0") Integer start,
                                                          @QueryParam("limit") @DefaultValue("25") Integer limit)
    {
        ResponseListUI<CheckUI> response = new ResponseListUI<CheckUI>();
        Boolean success = true;
        String msg = "OK";
        try
        {
            Long userId = AccessManager.getConnectedUserId(request);

            List<CheckUI> checksUI = new ArrayList<CheckUI>();

            Event event = eventService.getEventById(id);
            List<Check> mineList = checkService.getExitsChecksByUserIdAndDate(userId, event.getEndEvent(), start, limit);
            for (Check check : mineList)
            {
                User user = check.getUser();
                checksUI.add(CheckUI.toUI(check, user, null, -1));
            }

            response.setData(checksUI);
            response.setCount(checksUI.size());
        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setMessage(msg);
        response.setSuccess(success);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/att")
    public ResponseListUI<UserUI> getAttendanceByEventId(@PathParam("id") String id,
                                                         @QueryParam("start") @DefaultValue("0") Integer start,
                                                         @QueryParam("limit") @DefaultValue("25") Integer limit,
                                                         @QueryParam("filter") @DefaultValue("") String filter,
                                                         @QueryParam("sort") @DefaultValue("") String sort,
                                                         @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException
    {

        ResponseListUI<UserUI> response = new ResponseListUI<UserUI>();
        Boolean success = true;
        String msg = "OK";
        try
        {

            List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
            List<ItemSort> sortList = ItemSort.jsonDecode(sort);

            List<EventUser> eventUserList = eventUserService.getEventUsersByEventId(id, start, limit, filterList,
                    sortList);
            List<UserUI> userUIList = new ArrayList<UserUI>();

            for (EventUser eventUser : eventUserList)
            {
                List<Check> checksList = checkService.getChecksByEventIdAndUserId(id, eventUser
                        .getUser().getUserId());

                userUIList.add(UserUI.toUI(eventUser, CheckUI.toUI(checksList)));
            }

            response.setData(userUIList);
            response.setCount(userUIList.size());
        } catch (Exception e)
        {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setMessage(msg);
        response.setSuccess(success);
        return response;
    }

    @GET
    @Path("{id}/teachers")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<CheckUI> getUsersPDIByDates(@PathParam("id") String id,
                                                      @QueryParam("start") @DefaultValue("-1") Integer start,
                                                      @QueryParam("limit") @DefaultValue("25") Integer limit,
                                                      @QueryParam("filter") @DefaultValue("") String filter,
                                                      @QueryParam("sort") @DefaultValue("") String sort)
    {

        ResponseListUI<CheckUI> response = new ResponseListUI<CheckUI>();
        Boolean success = true;
        try
        {
            Long userId = AccessManager.getConnectedUserId(request);
            Event event = eventService.getEventById(id);
            Date startEvent = event.getStartEvent();
            Date endEvent = event.getEndEvent();
            User user = userService.getUserById(userId);
            List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);

            List<Check> checks = checkService.getChecksByLocation(event.getLocation(), startEvent, userId);
            List<CheckUI> checkUIList = new ArrayList<CheckUI>();
            for (Check check : checks)
            {
                User userCheck = check.getUser();
                if (userCheck.getAreaId().equals(user.getAreaId()) || eventService.checkSamePOD(event.getCode(), userCheck.getUserId()))
                {
                    checkUIList.add(CheckUI.toUI(check, userCheck, null, -1));
                }
            }

            response.setData(checkUIList);
            response.setCount(checkUIList.size());
        } catch (Exception e)
        {
            success = false;
            response.setMessage(e.getLocalizedMessage());
        }
        response.setSuccess(success);
        return response;
    }

    @GET
    @Path("area/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventsByArea(@PathParam("id") Integer areaId) throws GeneralCPRException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        List<Tuple> eventsList = new ArrayList<>();
        if (verifyUsersService.isTitDir(userId))
        {
            List<DirDegree> dirDegreeList = dirDegreeService.getDirDegreeByUsersId(userId);
            eventsList = eventService.getEventsByTitAndAreaId(dirDegreeList.get(0).getDegreeId(), areaId);
        } else
        {
            eventsList = eventService.getEventsByArea(areaId);
        }
        List<UIEntity> list = new ArrayList<>();
        for (Tuple tupla : eventsList)
        {
            UIEntity ui = new UIEntity();
            ui.put("code", tupla.get(0, Event.class));
            ui.put("name", tupla.get(1, Event.class));
            list.add(ui);

        }
        return list;

    }

}
