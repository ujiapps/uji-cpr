package es.uji.apps.cpr.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cpr_vmc_asi_titulacion", schema = "UJI_CONTROLPRESENCIA")
public class SubDegree implements Serializable
{

    @Id
    @Column(name = "ID_ASIGNATURA")
    private String subjectId;

    @Column(name = "ID_TITULACION")
    private Integer titulacion;

    @ManyToOne
    @JoinColumn(name = "ID_TITULACION", insertable = false, updatable = false)
    private DirDegree dirDegree;

    public SubDegree()
    {

    }

    public String getSubjectId()
    {
        return subjectId;
    }

    public void setSubjectId(String subjectId)
    {
        this.subjectId = subjectId;
    }

    public DirDegree getDirDegree()
    {
        return dirDegree;
    }

    public void setDirDegree(DirDegree dirDegree)
    {
        this.dirDegree = dirDegree;
    }

    public Integer getTitulacion()
    {
        return titulacion;
    }

    public void setTitulacion(Integer titulacion)
    {
        this.titulacion = titulacion;
    }
}
