package es.uji.apps.cpr.ui;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import es.uji.apps.cpr.model.EstadisticasAsignatura;

public class EstadisticasUI
{
    private String code;
    private String asigNombre;
    private String grupoNombre;
    private Integer totalGrupos;
    private Long totalSesionesGrupos;
    private Long totalSinIncidencias;
    private Long totalPendientes;
    private Long totalAbiertas;
    private Long totalResueltas;
    private Long totalRevocadas;
    private List<AsignaturaUI> asignaturas;
    private Long totalAutoResueltas;
    private Double totalSinIncidenciasPor;
    private Double totalPendientesPor;
    private Double totalAbiertasPor;
    private Double totalResueltasPor;
    private Double totalRevocadasPor;
    private Double totalAutoResueltasPor;

    public EstadisticasUI(List<EstadisticasAsignatura> asig)
    {
        this.totalGrupos = asig.size();
        this.totalSesionesGrupos = 0L;
        this.totalSinIncidencias = 0L;
        this.totalPendientes = 0L;
        this.totalAbiertas = 0L;
        this.totalResueltas = 0L;
        this.totalRevocadas = 0L;
        this.totalAutoResueltas = 0L;
        this.asignaturas = new ArrayList<>();
        this.code= asig.get(0).getCode();
        this.asigNombre= asig.get(0).getNombreAsig();
        this.grupoNombre= asig.get(0).getGrupoNombre();
        for (EstadisticasAsignatura estAsignatura : asig)
        {
//            AsignaturaUI asignaturaUI =new AsignaturaUI();
//            asignaturaUI.setId(estAsignatura.getId());
//            asignaturaUI.setCode(estAsignatura.getCode());
//            asignaturaUI.setGrupo(estAsignatura.getGrupo());
//            asignaturaUI.setNombreAsig(estAsignatura.getNombreAsig());
//            asignaturaUI.setTotalSesiones(estAsignatura.getTotalSesiones());
//            asignaturaUI.setTotalIncidenciasCreadas(estAsignatura.getTotalIncidenciasCreadas());
//            asignaturaUI.setSinIncidencia(estAsignatura.getSinIncidencia());
//            asignaturaUI.setPendientes(estAsignatura.getPendientes());
//            asignaturaUI.setIncidenciasAbiertas(estAsignatura.getIncidenciasAbiertas());
//            asignaturaUI.setIncidenciasAuto(estAsignatura.getIncidenciasAuto());
//            asignaturaUI.setIncidenciasRevocadas(estAsignatura.getIncidenciasRevocadas());
//            asignaturaUI.setIncidenciasResueltas(estAsignatura.getIncidenciasResueltas());
//            asignaturaUI.setSinIncidenciaPor(sinIncidenciaPor(estAsignatura.getSinIncidencia(), estAsignatura.getTotalSesiones()));
//            asignaturaUI.setPendientesPor(pendientesPor(estAsignatura.getSinIncidencia(), estAsignatura.getTotalSesiones(), estAsignatura.getPendientes()));
//            asignaturaUI.setIncidenciasAbiertasPor(incidenciaAbriertasPor(estAsignatura.getSinIncidencia(), estAsignatura.getTotalSesiones(), estAsignatura.getIncidenciasAbiertas()));
//            asignaturaUI.setIncidenciasResueltasPor(incidenciaResueltasPor(estAsignatura.getSinIncidencia(), estAsignatura.getTotalSesiones(), estAsignatura.getIncidenciasResueltas()));
//            asignaturaUI.setIncidenciasRevocadasPor(incidenciaRevocadaPor(estAsignatura.getSinIncidencia(), estAsignatura.getTotalSesiones(), estAsignatura.getIncidenciasRevocadas()));
//            asignaturaUI.setIncidenciasAutoResPor(incidenciaAutoResPor(estAsignatura.getSinIncidencia(), estAsignatura.getTotalSesiones(), estAsignatura.getIncidenciasAuto()));

            this.totalSesionesGrupos = this.totalSesionesGrupos + estAsignatura.getTotalSesiones();
            this.totalSinIncidencias = this.totalSinIncidencias + estAsignatura.getSinIncidencia();
            this.totalPendientes = this.totalPendientes + estAsignatura.getPendientes();
            this.totalAbiertas = this.totalAbiertas + estAsignatura.getIncidenciasAbiertas();
            this.totalResueltas = this.totalResueltas + estAsignatura.getIncidenciasResueltas();
            this.totalRevocadas = this.totalRevocadas + estAsignatura.getIncidenciasRevocadas();
            this.totalAutoResueltas = this.totalAutoResueltas + estAsignatura.getIncidenciasAuto();

//            this.asignaturas.add(asignaturaUI);
        }
        this.totalSinIncidenciasPor = sinIncidenciaPor(this.totalSinIncidencias, this.totalSesionesGrupos);
        this.totalPendientesPor = pendientesPor(this.totalSinIncidencias, totalSesionesGrupos, this.totalPendientes);
        this.totalAbiertasPor = incidenciaAbriertasPor(this.totalSinIncidencias, this.totalSesionesGrupos, this.totalAbiertas);
        this.totalResueltasPor = incidenciaResueltasPor(this.totalSinIncidencias, this.totalSesionesGrupos, this.totalResueltas);
        this.totalRevocadasPor = incidenciaRevocadaPor(this.totalSinIncidencias, this.totalSesionesGrupos, this.totalRevocadas);
        this.totalAutoResueltasPor = incidenciaAutoResPor(this.totalSinIncidencias, this.totalSesionesGrupos, this.totalAutoResueltas);

    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getAsigNombre()
    {
        return asigNombre;
    }

    public void setAsigNombre(String asigNombre)
    {
        this.asigNombre = asigNombre;
    }

    public String getGrupoNombre()
    {
        return grupoNombre;
    }

    public void setGrupoNombre(String grupoNombre)
    {
        this.grupoNombre = grupoNombre;
    }

    public Integer getTotalGrupos()
    {
        return totalGrupos;
    }

    public void setTotalGrupos(Integer totalGrupos)
    {
        this.totalGrupos = totalGrupos;
    }

    public Long getTotalSesionesGrupos()
    {
        return totalSesionesGrupos;
    }

    public void setTotalSesionesGrupos(Long totalSesionesGrupos)
    {
        this.totalSesionesGrupos = totalSesionesGrupos;
    }

    public Long getTotalSinIncidencias()
    {
        return totalSinIncidencias;
    }

    public void setTotalSinIncidencias(Long totalSinIncidencias)
    {
        this.totalSinIncidencias = totalSinIncidencias;
    }

    public Long getTotalPendientes()
    {
        return totalPendientes;
    }

    public void setTotalPendientes(Long totalPendientes)
    {
        this.totalPendientes = totalPendientes;
    }

    public Long getTotalAbiertas()
    {
        return totalAbiertas;
    }

    public void setTotalAbiertas(Long totalAbiertas)
    {
        this.totalAbiertas = totalAbiertas;
    }

    public Long getTotalResueltas()
    {
        return totalResueltas;
    }

    public void setTotalResueltas(Long totalResueltas)
    {
        this.totalResueltas = totalResueltas;
    }

    public Long getTotalRevocadas()
    {
        return totalRevocadas;
    }

    public void setTotalRevocadas(Long totalRevocadas)
    {
        this.totalRevocadas = totalRevocadas;
    }

    public Long getTotalAutoResueltas()
    {
        return totalAutoResueltas;
    }

    public void setTotalAutoResueltas(Long totalAutoResueltas)
    {
        this.totalAutoResueltas = totalAutoResueltas;
    }

    public Double getTotalSinIncidenciasPor()
    {
        return totalSinIncidenciasPor;
    }

    public void setTotalSinIncidenciasPor(Double totalSinIncidenciasPor)
    {
        this.totalSinIncidenciasPor = totalSinIncidenciasPor;
    }

    public Double getTotalPendientesPor()
    {
        return totalPendientesPor;
    }

    public void setTotalPendientesPor(Double totalPendientesPor)
    {
        this.totalPendientesPor = totalPendientesPor;
    }

    public Double getTotalAbiertasPor()
    {
        return totalAbiertasPor;
    }

    public void setTotalAbiertasPor(Double totalAbiertasPor)
    {
        this.totalAbiertasPor = totalAbiertasPor;
    }

    public Double getTotalResueltasPor()
    {
        return totalResueltasPor;
    }

    public void setTotalResueltasPor(Double totalResueltasPor)
    {
        this.totalResueltasPor = totalResueltasPor;
    }

    public Double getTotalRevocadasPor()
    {
        return totalRevocadasPor;
    }

    public void setTotalRevocadasPor(Double totalRevocadasPor)
    {
        this.totalRevocadasPor = totalRevocadasPor;
    }

    public Double getTotalAutoResueltasPor()
    {
        return totalAutoResueltasPor;
    }

    public void setTotalAutoResueltasPor(Double totalAutoResueltasPor)
    {
        this.totalAutoResueltasPor = totalAutoResueltasPor;
    }

    public static double round(double value, int places)
    {
        if (places < 0) throw new IllegalArgumentException();

        try
        {
            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        } catch (NumberFormatException e)
        {
            return 0D;
        }

    }

    private Double sinIncidenciaPor(Long sinIncidencia, Long totalSesiones)
    {
        try
        {
            Long incidentadas = totalSesiones - sinIncidencia;
            return round(((incidentadas * 100.0) / totalSesiones), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }

    }

    private Double pendientesPor(Long sinIncidencia, Long totalSesiones, Long pendientes)
    {
        try
        {
            return round(((pendientes * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    private Double incidenciaAbriertasPor(Long sinIncidencia, Long totalSesiones, Long incidenciasAbiertas)
    {
        try
        {
            return round(((incidenciasAbiertas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }

    }

    private Double incidenciaResueltasPor(Long sinIncidencia, Long totalSesiones, Long incidenciasResueltas)
    {
        try
        {
            return round(((incidenciasResueltas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    private Double incidenciaRevocadaPor(Long sinIncidencia, Long totalSesiones, Long incidenciasRevocadas)
    {
        try
        {
            return round(((incidenciasRevocadas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }


    }

    private Double incidenciaAutoResPor(Long sinIncidencia, Long totalSesiones, Long incidenciasAuto)
    {
        try
        {
            return round(((incidenciasAuto * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    public List<AsignaturaUI> getAsignaturas()
    {
        return asignaturas;
    }

    public void setAsignaturas(List<AsignaturaUI> asignaturas)
    {
        this.asignaturas = asignaturas;
    }
}
