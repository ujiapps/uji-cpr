package es.uji.apps.cpr.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CPR_AREAS_RESPONSABLES", schema = "UJI_CONTROLPRESENCIA")
public class AreasResponsible implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;

    @ManyToOne
    @JoinColumn(name = "AREA_ID")
    private DepartmentsAreas departmentsAreas;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DepartmentsAreas getDepartmentsAreas() {
        return departmentsAreas;
    }

    public void setDepartmentsAreas(DepartmentsAreas departmentsAreas) {
        this.departmentsAreas = departmentsAreas;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
