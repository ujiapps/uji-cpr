package es.uji.apps.cpr.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.model.EventUser;
import es.uji.apps.cpr.model.QAreasResponsible;
import es.uji.apps.cpr.model.QCheck;
import es.uji.apps.cpr.model.QDepartmentsAreas;
import es.uji.apps.cpr.model.QEvent;
import es.uji.apps.cpr.model.QEventUser;
import es.uji.apps.cpr.model.QIncidence;
import es.uji.apps.cpr.model.QTerminal;
import es.uji.apps.cpr.model.QUser;
import es.uji.apps.cpr.util.DynamicFilter;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EventsUsersDAO extends BaseDAODatabaseImpl
{

    public List<EventUser> getEventUsersByEventId(String eventId, int start, int limit,
                                                  List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;

        JPAQuery queryEvent = new JPAQuery(entityManager);
        QEvent qEvent = QEvent.event;
        queryEvent.from(qEvent).where(qEvent.eventId.eq(eventId));

        Event event = queryEvent.uniqueResult(qEvent);

        if (event != null)
        {
            if (event.getGrupoComp() == null)
            {
                query.from(qEventUser).where(qEventUser.event.eventId.eq(eventId).and(qEventUser.event.startEvent.gt(GeneralUtils.getInitialDate())).and(qEventUser.user.type.ne("PDI")));
            } else
            {
                query.from(qEventUser).where(qEventUser.event.grupoComp.eq(event.getGrupoComp()).and(qEventUser.event.startEvent.eq(event.getStartEvent())).and(qEventUser.user.type.ne("PDI")));
            }
        }

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("userId", qEventUser.user.userId);
        validFilters.put("name", qEventUser.name);
        validFilters.put("surname", qEventUser.surname);
        validFilters.put("type", qEventUser.type);

        if (filterList != null)
            query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("eventId", qEventUser.event.eventId);
        validSorts.put("userId", qEventUser.user.userId);
        validSorts.put("name", qEventUser.name);
        validSorts.put("surname", qEventUser.surname);
        validSorts.put("type", qEventUser.type);

        if (sortList != null)
        {
            for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
            {
                query.orderBy(o);
            }
        }

        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qEventUser);
    }

    public List<EventUser> getEventUsersByUserId(long userId, int start, int limit,
                                                 List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEventUser qEventUser = QEventUser.eventUser;

        if (userId != -1)
        {
            query.from(qEventUser).where(qEventUser.user.userId.eq(userId).and(qEventUser.event.startEvent.gt(GeneralUtils.getInitialDate())));
        }

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("eventId", qEventUser.event.eventId);
        validFilters.put("userId", qEventUser.user.userId);
        validFilters.put("name", qEventUser.name);
        validFilters.put("surname", qEventUser.surname);
        validFilters.put("type", qEventUser.type);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("eventId", qEventUser.event.eventId);
        validSorts.put("userId", qEventUser.user.userId);
        validSorts.put("name", qEventUser.name);
        validSorts.put("surname", qEventUser.surname);
        validSorts.put("type", qEventUser.type);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qEventUser);
    }

    public List<EventUser> getEventUsersByEventIdAndType(String eventId, String type, int start,
                                                         int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEventUser qEventUser = QEventUser.eventUser;

        query.from(qEventUser).where(
                qEventUser.event.eventId.eq(eventId).and(qEventUser.type.eq(type)).and(qEventUser.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("eventId", qEventUser.event.eventId);
        validFilters.put("userId", qEventUser.user.userId);
        validFilters.put("name", qEventUser.name);
        validFilters.put("surname", qEventUser.surname);
        validFilters.put("type", qEventUser.type);

        if (filterList != null)
            query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("eventId", qEventUser.event.eventId);
        validSorts.put("userId", qEventUser.user.userId);
        validSorts.put("name", qEventUser.name);
        validSorts.put("surname", qEventUser.surname);
        validSorts.put("type", qEventUser.type);

        if (sortList != null)
        {
            for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
            {
                query.orderBy(o);
            }
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qEventUser);
    }

    public List<EventUser> getEventUsersIncidencePendingByArea(Long userId, int start,
                                                               int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subquery = new JPAQuery(entityManager);
        JPAQuery subquery2 = new JPAQuery(entityManager);

        QEventUser qEventUser = QEventUser.eventUser;
        QUser qUser = QUser.user;
        QDepartmentsAreas qDepartmentsAreas = QDepartmentsAreas.departmentsAreas;
        QAreasResponsible qAreasResponsible = QAreasResponsible.areasResponsible;

        QEvent qEvent = QEvent.event;
        QTerminal qTerminal = QTerminal.terminal;
        QIncidence qIncidence = QIncidence.incidence;
        QCheck qCheck = QCheck.check;

        subquery2.from(qEvent)
                .join(qEvent.eventUsers, qEventUser)
                .leftJoin(qEvent.incidences).fetch()
                .leftJoin(qEvent.checks).fetch()
                .where(qEventUser.user.userId.eq(userId)
                        .and(qEvent.location.in(subquery.from(qTerminal).where(qTerminal.status.eq(1L).and(qTerminal.type.eq(1L))).list(qTerminal.location))))
                .orderBy(qEvent.eventId.asc()).orderBy(qEvent.grupoComp.asc());

        query.from(qEventUser).join(qEventUser.user, qUser).fetch()
                .where(qUser.areaId.in(
                        subquery.from(qDepartmentsAreas)
                                .join(qDepartmentsAreas.areasResponsible, qAreasResponsible)
                                .where(qAreasResponsible.user.userId.eq(userId)).list(qDepartmentsAreas.areaId))
                        .and(qEventUser.event.eventId.in(subquery2.from(qEvent)
                                .join(qEvent.eventUsers, qEventUser)
                                .leftJoin(qEvent.incidences).fetch()
                                .leftJoin(qEvent.checks).fetch()
                                .where(qEventUser.user.userId.eq(userId)
                                        .and(qEvent.location.in(subquery.from(qTerminal).where(qTerminal.status.eq(1L).and(qTerminal.type.eq(1L))).list(qTerminal.location)))).list(qEvent.eventId))));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("eventId", qEventUser.event.eventId);
        validFilters.put("userId", qEventUser.user.userId);
        validFilters.put("name", qEventUser.name);
        validFilters.put("surname", qEventUser.surname);
        validFilters.put("type", qEventUser.type);

        if (filterList != null)
            query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("eventId", qEventUser.event.eventId);
        validSorts.put("userId", qEventUser.user.userId);
        validSorts.put("name", qEventUser.name);
        validSorts.put("surname", qEventUser.surname);
        validSorts.put("type", qEventUser.type);

        if (sortList != null)
        {
            for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
            {
                query.orderBy(o);
            }
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qEventUser);
    }
}
