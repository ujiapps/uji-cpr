package es.uji.apps.cpr.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.*;
import es.uji.apps.cpr.util.DynamicFilter;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class EventsDAO extends BaseDAODatabaseImpl

{
    public Event getEventById(String id) throws GeneralCPRException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEvent qEvent = QEvent.event;

        Event eventDTO = query.from(qEvent).where(qEvent.eventId.eq(id).and(qEvent.startEvent.gt(GeneralUtils.getInitialDate()))).uniqueResult(qEvent);

        if (eventDTO == null)
        {
            throw new GeneralCPRException(
                    "No se ha encontrado la entidad UIEvent solicitada a traves del ID");
        }

        return eventDTO;
    }

    public List<Event> getEventsByUserId(long userId, Date startDate, Date endDate,
                                         List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subquery = new JPAQuery(entityManager);

        QEvent qEvent = QEvent.event;
        QEventUser qEventUser = QEventUser.eventUser;
        QTerminal qTerminal = QTerminal.terminal;
        QIncidence qIncidence = QIncidence.incidence;
        QCheck qCheck = QCheck.check;

        query.from(qEvent)
                .join(qEvent.eventUsers, qEventUser)
                .leftJoin(qEvent.incidences).fetch()
                .leftJoin(qEvent.checks).fetch()
                .where(qEventUser.user.userId.eq(userId).and(qEventUser.type.eq("PDI"))
                                .and(qEvent.location.in(subquery.from(qTerminal).where(qTerminal.status.eq(1L).and(qTerminal.type.eq(1L))).list(qTerminal.location)))
                                .and(qEvent.tipoId.in("TE", "PR", "LA","SE"))
                )
                .orderBy(qEvent.eventId.asc()).orderBy(qEvent.grupoComp.asc());


        if (startDate != null || endDate != null)
        {
            if (GeneralUtils.isValidDate(startDate))
            {
                query.where(qEvent.startEvent.between(startDate, endDate));
            } else
            {
                query.where(qEvent.startEvent.between(GeneralUtils.getInitialDate(), endDate));
            }
        }

        return query.distinct().list(qEvent);
    }

    public List<Event> getEventsPendingByUserId(long userId, Date startDate, Date endDate,
                                                List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery subquery = new JPAQuery(entityManager);

        QEvent qEvent = QEvent.event;
        QEventUser qEventUser = QEventUser.eventUser;
        QTerminal qTerminal = QTerminal.terminal;
        QIncidence qIncidence = QIncidence.incidence;
        QCheck qCheck = QCheck.check;
        QEventPendent qEventPendent = QEventPendent.eventPendent;

        query.from(qEvent)
                .join(qEvent.eventPendents, qEventPendent)
                .join(qEvent.eventUsers, qEventUser)
                .leftJoin(qEvent.incidences, qIncidence).fetch()
                .leftJoin(qEvent.checks, qCheck).fetch()
                .where(qEventPendent.personaId.eq(userId)
                )
                .orderBy(qEvent.eventId.asc()).orderBy(qEvent.grupoComp.asc());


        if (startDate != null || endDate != null)
        {
            if (GeneralUtils.isValidDate(startDate))
            {
                query.where(qEvent.startEvent.between(startDate, endDate));
            } else
            {
                query.where(qEvent.startEvent.between(GeneralUtils.getInitialDate(), endDate));
            }
        }


        return query.distinct().list(qEvent);
    }

    public List<Event> getEventsByUserIdAndCode(long userId, String code, Date startDate, Date endDate,
                                                List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEvent qEvent = QEvent.event;
        QEventUser qEventUser = QEventUser.eventUser;

        query.from(qEvent).join(qEvent.eventUsers, qEventUser)
                .where(qEventUser.user.userId.eq(userId).and(qEvent.code.eq(code)))
                .orderBy(qEvent.eventId.asc());

        if (startDate != null || endDate != null)
        {
            if (GeneralUtils.isValidDate(startDate))
            {
                query.where(qEvent.startEvent.between(startDate, endDate));
            } else
            {
                query.where(qEvent.startEvent.between(GeneralUtils.getInitialDate(), endDate));
            }
        }

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("location", qEvent.location);
        validFilters.put("code", qEvent.code);
        validFilters.put("cursoAca", qEvent.cursoAca);
        validFilters.put("endEvent", qEvent.endEvent);
        validFilters.put("eventId", qEvent.eventId);
        validFilters.put("name", qEvent.name);
        validFilters.put("startEvent", qEvent.startEvent);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("location", qEvent.location);
        validSorts.put("code", qEvent.code);
        validSorts.put("cursoAca", qEvent.cursoAca);
        validSorts.put("endEvent", qEvent.endEvent);
        validSorts.put("eventId", qEvent.eventId);
        validSorts.put("name", qEvent.name);
        validSorts.put("startEvent", qEvent.startEvent);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }

        return query.list(qEvent);
    }

    public List<Event> getEventsByUserIdAndDate(long userId, Calendar date, int start, int limit,
                                                List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEvent qEvent = QEvent.event;
        QEventUser qEventUser = QEventUser.eventUser;

        query.from(qEvent).join(qEvent.eventUsers, qEventUser)
                .where(qEventUser.user.userId.eq(userId)
                        .and(qEvent.tipoId.in("TE", "PR", "LA", "SE"))
                        .and(
                                (qEvent.startEvent.dayOfMonth().eq(date.get(Calendar.DAY_OF_MONTH))
                                        .and(qEvent.startEvent.month().eq(date.get(Calendar.MONTH) + 1))
                                        .and(qEvent.startEvent.year().eq(date.get(Calendar.YEAR)))
                                        .and(qEvent.startEvent.gt(GeneralUtils.getInitialDate())))))
                .orderBy(qEvent.startEvent.asc());


        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("location", qEvent.location);
        validFilters.put("code", qEvent.code);
        validFilters.put("cursoAca", qEvent.cursoAca);
        validFilters.put("endEvent", qEvent.endEvent);
        validFilters.put("eventId", qEvent.eventId);
        validFilters.put("name", qEvent.name);
        validFilters.put("startEvent", qEvent.startEvent);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("location", qEvent.location);
        validSorts.put("code", qEvent.code);
        validSorts.put("cursoAca", qEvent.cursoAca);
        validSorts.put("endEvent", qEvent.endEvent);
        validSorts.put("eventId", qEvent.eventId);
        validSorts.put("name", qEvent.name);
        validSorts.put("startEvent", qEvent.startEvent);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }
        return query.list(qEvent);
    }

    public List<Event> getEventsByLocationAndDate(String location, Calendar date, int start,
                                                  int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
            throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);

        QEvent qEvent = QEvent.event;

        query.from(qEvent)
                .where(qEvent.location.eq(location).and(
                        (qEvent.startEvent.dayOfMonth().eq(date.get(Calendar.DAY_OF_MONTH))
                                .and(qEvent.startEvent.month().eq(date.get(Calendar.MONTH) + 1))
                                .and(qEvent.startEvent.year().eq(date.get(Calendar.YEAR)))
                                .and(qEvent.startEvent.gt(GeneralUtils.getInitialDate())))))
                .orderBy(qEvent.startEvent.asc());

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("location", qEvent.location);
        validFilters.put("code", qEvent.code);
        validFilters.put("cursoAca", qEvent.cursoAca);
        validFilters.put("endEvent", qEvent.endEvent);
        validFilters.put("eventId", qEvent.eventId);
        validFilters.put("name", qEvent.name);
        validFilters.put("startEvent", qEvent.startEvent);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("location", qEvent.location);
        validSorts.put("code", qEvent.code);
        validSorts.put("cursoAca", qEvent.cursoAca);
        validSorts.put("endEvent", qEvent.endEvent);
        validSorts.put("eventId", qEvent.eventId);
        validSorts.put("name", qEvent.name);
        validSorts.put("startEvent", qEvent.startEvent);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qEvent);

    }

    public List<Event> getEventsByCode(String code, Date startDate, Date endDate, List<ItemFilter> filterList, List<ItemSort> sortList)
    {

        JPAQuery query = new JPAQuery(entityManager);

        QEvent qEvent = QEvent.event;

        query.from(qEvent).where(qEvent.code.eq(code));

        if (startDate != null || endDate != null)
        {
            if (GeneralUtils.isValidDate(startDate))
            {
                query.where(qEvent.startEvent.between(startDate, endDate));
            } else
            {
                query.where(qEvent.startEvent.between(GeneralUtils.getInitialDate(), endDate));
            }
        }

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("location", qEvent.location);
        validFilters.put("code", qEvent.code);
        validFilters.put("cursoAca", qEvent.cursoAca);
        validFilters.put("endEvent", qEvent.endEvent);
        validFilters.put("eventId", qEvent.eventId);
        validFilters.put("name", qEvent.name);
        validFilters.put("startEvent", qEvent.startEvent);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("location", qEvent.location);
        validSorts.put("code", qEvent.code);
        validSorts.put("cursoAca", qEvent.cursoAca);
        validSorts.put("endEvent", qEvent.endEvent);
        validSorts.put("eventId", qEvent.eventId);
        validSorts.put("name", qEvent.name);
        validSorts.put("startEvent", qEvent.startEvent);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }

        return query.list(qEvent);
    }

    public List<Tuple> getEventsIncidencePendingDegree(Long userId, Date startDate, Date endDate)
    {

        JPAQuery query = new JPAQuery(entityManager);
        QEventPendent qEventPendent = QEventPendent.eventPendent;
        QDirDegree qDirDegree = QDirDegree.dirDegree;

        query.from(qEventPendent, qDirDegree).where(qEventPendent.titulacionId.eq(qDirDegree.degreeId).and(qDirDegree.userId.eq(userId)));

        if (startDate != null || endDate != null)
        {
            if (GeneralUtils.isValidDate(startDate))
            {
                query.where(qEventPendent.startEvent.between(startDate, endDate));
            } else
            {
                query.where(qEventPendent.startEvent.between(GeneralUtils.getInitialDate(), endDate));
            }
        }
        query.orderBy(qEventPendent.startEvent.desc());

        return query.list(ConstructorExpression.create(Event.class, qEventPendent.eventId, qEventPendent.code,
                qEventPendent.name, qEventPendent.startEvent, qEventPendent.endEvent, qEventPendent.location,
                qEventPendent.cursoAca, qEventPendent.grupoComp, qEventPendent.grupoNom, qEventPendent.tipoId), qEventPendent.usuarios);

    }

    public List<Tuple> getEventsIncidencePendingArea(List<DepartmentsAreas> areasList, Date startDate, Date endDate)
    {
        List<Integer> areas = new ArrayList<>();
        for (DepartmentsAreas anAreasList : areasList)
        {
            areas.add(anAreasList.getAreaId());
        }
        JPAQuery query = new JPAQuery(entityManager);
        QEventPendent qEventPendent = QEventPendent.eventPendent;

        query.from(qEventPendent).where(qEventPendent.areaId.in(areas));
        if (startDate != null || endDate != null)
        {
            if (GeneralUtils.isValidDate(startDate))
            {
                query.where(qEventPendent.startEvent.between(startDate, endDate));
            } else
            {
                query.where(qEventPendent.startEvent.between(GeneralUtils.getInitialDate(), endDate));
            }
        }
        query.orderBy(qEventPendent.startEvent.desc());

        return query.list(ConstructorExpression.create(Event.class, qEventPendent.eventId, qEventPendent.code,
                qEventPendent.name, qEventPendent.startEvent, qEventPendent.endEvent, qEventPendent.location,
                qEventPendent.cursoAca, qEventPendent.grupoComp, qEventPendent.grupoNom, qEventPendent.tipoId), qEventPendent.usuarios);
    }

    public Boolean checkSamePOD(String code, Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;
        QEvent qEvent = QEvent.event;
        QUser qUser = QUser.user;

        query.from(qEventUser)
                .join(qEventUser.event, qEvent)
                .join(qEventUser.user, qUser).where(qEventUser.event.eventId.like("%" + code + "%").and(qEventUser.user.userId.eq(userId)));

        List<EventUser> list = query.list(qEventUser);

        return list.size() > 0;
    }

    public List<Tuple> getEventsByArea(Integer areaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;
        QEvent qEvent = QEvent.event;
        QUser qUser = QUser.user;

        query.from(qEvent).join(qEvent.eventUsers, qEventUser).join(qEventUser.user, qUser).where(qUser.areaId.eq(areaId).and(qEvent.tipoId.in("TE", "PR", "LA","SE")));
        query.orderBy(qEvent.code.asc(), qEvent.name.asc());

        return query.distinct().list(qEvent.code, qEvent.name);
    }

    public List<Tuple> getEventsByTit(Integer tit)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;
        QEvent qEvent = QEvent.event;
        QSubDegree qSubDegree = QSubDegree.subDegree;

        query.from(qEvent, qSubDegree).where(qEvent.code.eq(qSubDegree.subjectId)
                .and(qSubDegree.titulacion.eq(tit)).and(qEvent.tipoId.in("TE", "PR", "LA","SE")));

        return query.distinct().list(qEvent.code, qEvent.name);

    }

    public List<Tuple> getEventsByTitAndAreaId(int tit, Integer areaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEventUser qEventUser = QEventUser.eventUser;
        QEvent qEvent = QEvent.event;
        QUser qUser = QUser.user;
        QSubDegree qSubDegree = QSubDegree.subDegree;

        query.from(qEvent, qSubDegree).join(qEvent.eventUsers, qEventUser).join(qEventUser.user, qUser).where(qEvent.code.eq(qSubDegree.subjectId)
                .and(qSubDegree.titulacion.eq(tit)).and(qEvent.tipoId.in("TE", "PR", "LA", "SE")).and(qUser.areaId.eq(areaId)));

        return query.distinct().list(qEvent.code, qEvent.name);
    }
}
