package es.uji.apps.cpr.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.UserKeyDAO;
import es.uji.apps.cpr.model.UserKey;

@Service
public class UserKeyService
{

    static final Logger logger = LoggerFactory.getLogger(UserKeyService.class);

    @Autowired
    private UserKeyDAO keyDAO;

    public List<UserKey> getUserKeysByPerId(long perId) throws GeneralCPRException
    {
        return keyDAO.getUserKeysByUserId(perId);
    }

}
