package es.uji.apps.cpr.services.rest;

import com.mysema.query.Tuple;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.*;
import es.uji.apps.cpr.services.*;
import es.uji.apps.cpr.ui.*;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.apps.cpr.util.IncidenceManager;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Path("incidences")
public class IncidenceResources extends CoreBaseService {

    @InjectParam
    private IncidenceService incidenceService;

    @InjectParam
    private EventUserService eventUserService;

    @InjectParam
    private UserService userService;

    @InjectParam
    private CheckService checkService;

    @InjectParam
    private EventService eventService;

    @InjectParam
    private DepartmentsAreasService departmentsAreasService;

    @InjectParam
    private AreasResponsibleService areasResponsibleService;

    @InjectParam
    private DepartmentsService departmentsService;

    @InjectParam
    private SubDegreeService subDegreeService;

    @InjectParam
    private DirDegreeService dirDegreeService;

    @InjectParam
    private VerifyUsersService verifyUsersService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public ResponseItemUI<IncidenceUI> getIncidenceById(@PathParam("id") long id,
                                                        @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {
        ResponseItemUI<IncidenceUI> response = new ResponseItemUI<IncidenceUI>();
        String msg = "OK";
        Boolean success = true;
        try {
            response.setData(IncidenceUI.toUI(incidenceService.getIncidenceById(id)));
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    public Response getIncidencesByUserId(
            @QueryParam("start") @DefaultValue("-1") Integer start,
            @QueryParam("limit") @DefaultValue("25") Integer limit,
            @QueryParam("filter") @DefaultValue("") String filter,
            @QueryParam("sort") @DefaultValue("") String sort,
            @QueryParam("output") @DefaultValue("") String output,
            @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);

        Response.ResponseBuilder res = null;
        ResponseListUI<IncidenceUI> response = new ResponseListUI<IncidenceUI>();
        String msg = "OK";
        Boolean success = true;

        List<IncidenceUI> incidences = new ArrayList<IncidenceUI>();

        try {
            if (output.length() > 0) {
                incidences = IncidenceUI.toUI(incidenceService.getIncidencesByUserId(connectedUserId, -1, limit, filterList, sortList));
            } else {
                incidences = IncidenceUI.toUI(incidenceService.getIncidencesByUserId(connectedUserId, start, limit, filterList, sortList));
            }

            response.setData(incidences);
            response.setCount(incidences.size());
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);

        if (output.length() > 0) {
            res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = " + output + ".csv");

            res.entity(IncidenceUI.toCSV(response.getData(), false));
            res.type(MediaType.TEXT_PLAIN);

        } else {
            res = Response.ok(response, MediaType.APPLICATION_JSON);
        }

        return res.build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseItemUI<IncidenceUI> createIncidence(IncidenceUI incidenceUI) throws GeneralCPRException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ResponseItemUI<IncidenceUI> response = new ResponseItemUI<IncidenceUI>();

        if (GeneralUtils.getPeriodoDocente().equals("0")) {
            response.setSuccess(false);
            response.setMessage("Aplicació oberta només per a consulta.");
            return response;
        }

        String msg = "OK";
        Boolean success = true;
        Boolean auto = false;

        try {
            User user = userService.getUserById(connectedUserId);
            UserUI userUI = UserUI.toUI(user, null);
            incidenceUI.setUserUIRequest(userUI);

            Event event = eventService.getEventById(incidenceUI.getEvent().getId());
            EventUI eventUI = EventUI.toUI(event);
            incidenceUI.setEvent(eventUI);

            if (incidenceUI.getUserUIAssociated() != null) {
                Check check = checkService.getCheckById(incidenceUI.getUserUIAssociated().getId());
                incidenceUI.setCheckIn(CheckUI.toUI(check, null, null, -1));
                User userAssociated = userService.getUserById(check.getUser().getUserId());
                UserUI userAssociatedUI = UserUI.toUI(userAssociated, null);
                incidenceUI.setUserUIAssociated(userAssociatedUI);
                auto = eventService.checkSamePOD(event.getCode(), incidenceUI.getUserUIAssociated().getId());
            }

            Check checkIn = new Check();
            if (incidenceUI.getCheckIn() != null) {
                checkIn = checkService.getCheckById(incidenceUI.getCheckIn().getId());
                CheckUI checkUI = CheckUI.toUI(checkIn, checkIn.getUser(), null, -1);
                incidenceUI.setCheckIn(checkUI);
            }


            ItemFilter itemIn = new ItemFilter("direction", "0");
            List<ItemFilter> filterListIn = new ArrayList<ItemFilter>();
            filterListIn.add(itemIn);

            List<Check> checksIn = checkService.getChecksByEventId(incidenceUI.getEvent().getId(), -1, 25, filterListIn, null);

            IncidenceManager incidenceManager = new IncidenceManager(incidenceUI, auto);
            incidenceUI = incidenceManager.createIncidence(checksIn);

            Incidence incidence = incidenceService.insert(IncidenceUI.toModel(incidenceUI));
            if (incidence.getStatus() == 1) {
                Check check = checkService.getCheckById(incidence.getCheckIn().getId());
                check.setEvent(incidence.getEvent());
                check.setDirection(0);
                checkService.update(check);
            }

            response.setData(IncidenceUI.toUI(incidence));
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("area/{id}")
    public ResponseListUI<IncidenceUI> getIncidenceByAreaId(@PathParam("id") int id,
                                                            @QueryParam("start") @DefaultValue("-1") Integer start,
                                                            @QueryParam("limit") @DefaultValue("25") Integer limit,
                                                            @QueryParam("filter") @DefaultValue("") String filter,
                                                            @QueryParam("sort") @DefaultValue("") String sort,
                                                            @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        ResponseListUI<IncidenceUI> response = new ResponseListUI<IncidenceUI>();
        String msg = "OK";
        Boolean success = true;
        try {

            if (verifyByAreaId(connectedUserId, id)) {
                List<IncidenceUI> incidences = IncidenceUI.toUI(incidenceService.getIncidenceByAreaId(id, connectedUserId, start, limit, filterList, sortList));
                response.setData(incidences);
                response.setCount((int) incidenceService.getIncidencesByAreaIdCount(id, filterList));
            }

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    @Path("area")
    public Response getIncidenceByArea(@QueryParam("start") @DefaultValue("-1") Integer start,
                                       @QueryParam("limit") @DefaultValue("25") Integer limit,
                                       @QueryParam("filter") @DefaultValue("") String filter,
                                       @QueryParam("sort") @DefaultValue("") String sort,
                                       @QueryParam("output") @DefaultValue("") String output,
                                       @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);

        Response.ResponseBuilder res = null;
        ResponseListUI<IncidenceUI> response = new ResponseListUI<IncidenceUI>();
        String msg = "OK";
        Boolean success = true;

        List<IncidenceUI> incidences = new ArrayList<IncidenceUI>();

        try {
            List<DepartmentsAreas> areas = new ArrayList<DepartmentsAreas>();

            if (verifyUsersService.isAdmin(connectedUserId)) {
                areas = departmentsAreasService.getDepartmentsAreas();
            } else if (verifyUsersService.isDepDir(connectedUserId)) {
                Departments department = departmentsService.getDepartmentByDepartments(connectedUserId);
                areas = departmentsAreasService.getDepartmentsAreasByDepartments(department.getLocationId());
            } else if (verifyUsersService.isRespArea(connectedUserId)) {
                List<AreasResponsible> areasResponsibles = areasResponsibleService.getAreasResponsiblesByUserId(connectedUserId, start, limit, filterList, sortList);
                for (AreasResponsible areasResponsible : areasResponsibles) {
                    areas.add(areasResponsible.getDepartmentsAreas());
                }
            }

            List<Integer> areaIds = new ArrayList<Integer>();

            for (DepartmentsAreas area : areas) {
                areaIds.add(area.getAreaId());
            }

            if (output.length() > 0) {
                incidences.addAll(IncidenceUI.toUI(incidenceService.getIncidenceByAreaIds(areaIds, connectedUserId, -1, limit, filterList, sortList)));
            } else {
                incidences.addAll(IncidenceUI.toUI(incidenceService.getIncidenceByAreaIds(areaIds, connectedUserId, start, limit, filterList, sortList)));
            }

            Collections.sort(incidences, new Comparator<IncidenceUI>() {
                @Override
                public int compare(IncidenceUI i1, IncidenceUI i2) {
                    Integer ip = i1.getStatus();
                    int value = ip.compareTo(i2.getStatus());
                    if (value == 0) {
                        return i2.getInsertTime().compareTo(i1.getInsertTime());
                    }
                    return value;
                }
            });

            response.setData(incidences);
            response.setCount(incidences.size());

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }

        response.setSuccess(success);
        response.setMessage(msg);

        if (output.length() > 0) {
            res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = " + output + ".csv");

            res.entity(IncidenceUI.toCSV(response.getData(), true));
            res.type(MediaType.TEXT_PLAIN);

        } else {
            res = Response.ok(response, MediaType.APPLICATION_JSON);
        }

        return res.build();
    }

    @PUT
    @Path("area/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseItemUI<IncidenceUI> updateIncidence(IncidenceUI incidenceUI) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ResponseItemUI<IncidenceUI> response = new ResponseItemUI<IncidenceUI>();
        if (GeneralUtils.getPeriodoDocente().equals("0")) {
            response.setSuccess(false);
            response.setMessage("Aplicació oberta només per a consulta.");
            return response;
        }
        String msg = "OK";
        Boolean success = true;

        try {
            if (verifyByIncidence(connectedUserId, incidenceUI)) {
                Incidence incidenceNow = incidenceService.getIncidenceById(incidenceUI.getId());
                UserUI userUI = UserUI.toUI(userService.getUserById(connectedUserId), null);
                IncidenceUI incidenceUINow = IncidenceUI.toUI(incidenceNow);
                incidenceUINow.setStatus(incidenceUI.getStatus());
                incidenceUINow.setResolution(incidenceUI.getResolution());

                IncidenceManager incidenceManager = new IncidenceManager(incidenceUI, false);
                IncidenceUI incidenceUIRes = incidenceManager.updateIncidence(userUI, incidenceUINow);

                Check checkIn = new Check();
                if (incidenceUIRes.getCheckIn() != null) {
                    checkIn = checkService.getCheckById(incidenceUIRes.getCheckIn().getId());
                    if (incidenceUIRes.getStatus() != 3) {
                        checkIn.setEvent(null);
                    } else {
                        Event event = new Event();
                        event.setEventId(incidenceUIRes.getEvent().getId());
                        checkIn.setEvent(event);
                    }
                    if (incidenceUIRes.getCheckIn().getDirection() != null) {
                        checkIn.setDirection(incidenceUIRes.getCheckIn().getDirection());
                    }
                    checkService.update(checkIn);
                }

                Incidence incidenceRes = incidenceService.update(IncidenceUI.toModel(incidenceUIRes));
                IncidenceUI res = IncidenceUI.toUI(incidenceRes);

                response.setData(res);
            }

        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;
    }

    @GET
    @Path("pending")
    public Response getPendings(@QueryParam("startDate") @DefaultValue("") String startDate,
                                @QueryParam("endDate") @DefaultValue("") String endDate,
                                @QueryParam("start") @DefaultValue("0") Integer start,
                                @QueryParam("limit") @DefaultValue("25") Integer limit,
                                @QueryParam("filter") @DefaultValue("") String filter,
                                @QueryParam("sort") @DefaultValue("") String sort,
                                @QueryParam("output") @DefaultValue("") String output,
                                @CookieParam("uji-lang") @DefaultValue("ca") String idioma) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Response.ResponseBuilder res = null;
        ResponseListUI<IncidencePendingUI> response = new ResponseListUI<IncidencePendingUI>();
        String msg = "OK";
        Boolean success = true;

        try {
            List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
            List<ItemSort> sortList = ItemSort.jsonDecode(sort);

            for (ItemFilter itemFilter : filterList) {
                if (itemFilter.getField().equals("startDate")) startDate = itemFilter.getValue();
                if (itemFilter.getField().equals("endDate")) endDate = itemFilter.getValue();
            }

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDateFormatted = null;
            Date endDateFormatted = null;
            try {
                if (!startDate.equals("")) {
                    String sDate = df.format(Date.from(Instant.parse(startDate)));
                    startDateFormatted = format.parse(sDate + " 00:00:00");
                }
                if (!endDate.equals("")) {
                    String eDate = df.format(Date.from(Instant.parse(endDate)));
                    endDateFormatted = format.parse(eDate + " 23:59:59");
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            List<EventUI> eventUIList = null;

            if (output.length() > 0) {
                eventUIList = eventService.getEventsPendingByUserId(connectedUserId, startDateFormatted,
                        endDateFormatted, -1, limit, filterList, sortList);
            } else {
                eventUIList = eventService.getEventsPendingByUserId(connectedUserId, startDateFormatted,
                        endDateFormatted, start, limit, filterList, sortList);
            }

            User user = userService.getUserById(connectedUserId);

            List<IncidencePendingUI> incidencePendingUIList = new ArrayList<IncidencePendingUI>();
            IncidencePendingUI incidencePendingUI;


            for (EventUI eventUI : eventUIList) {
                incidencePendingUI = IncidencePendingUI.toUI(eventUI, UserUI.toUI(user, null), (byte) 1);
                incidencePendingUIList.add(incidencePendingUI);

            }

            response.setData(incidencePendingUIList);
            response.setCount(incidencePendingUIList.size());
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);

        if (output.length() > 0) {
            res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = " + output + ".csv");

            res.entity(IncidencePendingUI.toCSV(response.getData()));
            res.type(MediaType.TEXT_PLAIN);

        } else {
            res = Response.ok(response, MediaType.APPLICATION_JSON);
        }

        return res.build();
    }

    @GET
    @Path("area/pending")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIncidencePendingByArea(@QueryParam("start") @DefaultValue("-1") Integer start,
                                              @QueryParam("startDate") @DefaultValue("") String startDate,
                                              @QueryParam("endDate") @DefaultValue("") String endDate,
                                              @QueryParam("limit") @DefaultValue("200") Integer limit,
                                              @QueryParam("page") @DefaultValue("0") Integer page,
                                              @QueryParam("filter") @DefaultValue("") String filter,
                                              @QueryParam("sort") @DefaultValue("") String sort,
                                              @QueryParam("output") @DefaultValue("") String output,
                                              @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);
        Response.ResponseBuilder res = null;
        ResponseListUI<IncidencePendingUI> response = new ResponseListUI<IncidencePendingUI>();
        String msg = "OK";
        Boolean success = true;

        try {
            List<DepartmentsAreas> areas = new ArrayList<DepartmentsAreas>();
            if (verifyUsersService.isAdmin(connectedUserId)) {
                areas = departmentsAreasService.getDepartmentsAreas();
            } else if (verifyUsersService.isDepDir(connectedUserId)) {
                Departments department = departmentsService.getDepartmentByDepartments(connectedUserId);
                areas = departmentsAreasService.getDepartmentsAreasByDepartments(department.getLocationId());
            } else if (verifyUsersService.isRespArea(connectedUserId)) {
                List<AreasResponsible> areasResponsibles = areasResponsibleService.getAreasResponsiblesByUserId(connectedUserId, start, limit, filterList, sortList);
                for (AreasResponsible areasResponsible : areasResponsibles) {
                    areas.add(areasResponsible.getDepartmentsAreas());
                }
            }

            for (ItemFilter itemFilter : filterList) {
                if (itemFilter.getField().equals("startDate")) startDate = itemFilter.getValue();
                if (itemFilter.getField().equals("endDate")) endDate = itemFilter.getValue();
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDateFormatted = null;
            Date endDateFormatted = null;
            try {
                if (!startDate.equals("")) {
                    String sDate = df.format(Date.from(Instant.parse(startDate)));
                    startDateFormatted = format.parse(sDate + " 00:00:00");
                }
                if (!endDate.equals("")) {
                    String eDate = df.format(Date.from(Instant.parse(endDate)));
                    endDateFormatted = format.parse(eDate + " 23:59:59");
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            List<IncidencePendingUI> incidencePendingUIList = new ArrayList<IncidencePendingUI>();
            List<Tuple> eventList = eventService.getEventsIncidencePendingArea(areas, startDateFormatted, endDateFormatted);
            for (Tuple event : eventList) {
                IncidencePendingUI incidencePendingUI = new IncidencePendingUI();
                List<User> userList = new ArrayList<>();
                User user = new User();
                if (ParamUtils.isNotNull(event.get(1, String.class))) {
                    user.setName(event.get(1, String.class));
                    userList.add(user);
                } else {
                    user.setName("Clase sin PDI asociado");
                    userList.add(user);
                }
                incidencePendingUI = IncidencePendingUI.toUI(event.get(0, Event.class), userList, (byte) 1);
                incidencePendingUIList.add(incidencePendingUI);
            }
            response.setData(IncidencePendingUI.eliminaDuplicados(incidencePendingUIList));
            response.setCount(eventList.size());
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();

        }
        response.setSuccess(success);
        response.setMessage(msg);

        if (output.length() > 0) {
            res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = " + output + ".csv");

            res.entity(IncidencePendingUI.toCSV(response.getData()));
            res.type(MediaType.TEXT_PLAIN);

        } else {
            res = Response.ok(response, MediaType.APPLICATION_JSON);
        }

        return res.build();
    }

    @GET
    @Path("degree")
    public Response getIncidenceByDegree(@QueryParam("start") @DefaultValue("-1") Integer start,
                                         @QueryParam("limit") @DefaultValue("25") Integer limit,
                                         @QueryParam("filter") @DefaultValue("") String filter,
                                         @QueryParam("sort") @DefaultValue("") String sort,
                                         @QueryParam("output") @DefaultValue("") String output,
                                         @CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);
        List<ItemSort> sortList = ItemSort.jsonDecode(sort);

        Response.ResponseBuilder res = null;
        ResponseListUI<IncidenceUI> response = new ResponseListUI<IncidenceUI>();
        String msg = "OK";
        Boolean success = true;
        if (!verifyUsersService.isTitDir(connectedUserId)) {
            response.setSuccess(success);
            response.setMessage("No access permes.");
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        List<IncidenceUI> incidences = new ArrayList<IncidenceUI>();

        try {
            int responseCount = 0;

            List<DirDegree> dirDegreeList = dirDegreeService.getDirDegreeByUsersId(connectedUserId);

            List<SubDegree> subDegreeList = new ArrayList<SubDegree>();

            for (DirDegree dirDegree : dirDegreeList) {
                subDegreeList.addAll(subDegreeService.getSubDegreeByIdDegree(dirDegree.getDegreeId()));
            }

            List<String> codes = new ArrayList<String>();

            for (SubDegree subDegree : subDegreeList) {
                codes.add(subDegree.getSubjectId());
            }

            if (output.length() > 0) {
                incidences.addAll(IncidenceUI.toUI(incidenceService.getIncidencesByCodes(codes, -1, limit, filterList, sortList)));
            } else {
                incidences.addAll(IncidenceUI.toUI(incidenceService.getIncidencesByCodes(codes, start, limit, filterList, sortList)));
            }
            responseCount = responseCount + (int) incidenceService.getIncidencesByCodesCount(codes, filterList);


            response.setData(incidences);
            response.setCount(responseCount);
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);

        if (output.length() > 0) {
            res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = " + output + ".csv");

            res.entity(IncidenceUI.toCSV(response.getData(), true));
            res.type(MediaType.TEXT_PLAIN);

        } else {
            res = Response.ok(response, MediaType.APPLICATION_JSON);
        }

        return res.build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("degree/pending")
    public Response getIncidencePendingByDegree(@QueryParam("startDate") @DefaultValue("") String startDate,
                                                @QueryParam("endDate") @DefaultValue("") String endDate,
                                                @QueryParam("filter") @DefaultValue("") String filter,
                                                @QueryParam("output") @DefaultValue("") String output) throws IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Response.ResponseBuilder res = null;
        ResponseListUI<IncidencePendingUI> response = new ResponseListUI<IncidencePendingUI>();
        String msg = "OK";
        Boolean success = true;
        if (!verifyUsersService.isTitDir(connectedUserId)) {
            response.setSuccess(success);
            response.setMessage("No access permes.");
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        try {
            List<ItemFilter> filterList = ItemFilter.jsonDecode(filter);

            for (ItemFilter itemFilter : filterList) {
                if (itemFilter.getField().equals("startDate")) startDate = itemFilter.getValue();
                if (itemFilter.getField().equals("endDate")) endDate = itemFilter.getValue();
            }
            verifyUsersService.isTitDir(connectedUserId);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDateFormatted = null;
            Date endDateFormatted = null;
            try {
                if (!startDate.equals("")) {
                    String sDate = df.format(Date.from(Instant.parse(startDate)));
                    startDateFormatted = format.parse(sDate + " 00:00:00");
                }
                if (!endDate.equals("")) {
                    String eDate = df.format(Date.from(Instant.parse(endDate)));
                    endDateFormatted = format.parse(eDate + " 23:59:59");
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            List<IncidencePendingUI> incidencePendingUIList = new ArrayList<IncidencePendingUI>();
            List<Tuple> eventList = eventService.getEventsIncidencePendingDegree(connectedUserId, startDateFormatted, endDateFormatted);
            for (Tuple event : eventList) {
                IncidencePendingUI incidencePendingUI = new IncidencePendingUI();
                List<User> userList = new ArrayList<>();
                User user = new User();
                if (ParamUtils.isNotNull(event.get(1, String.class))) {
                    user.setName(event.get(1, String.class));
                    userList.add(user);
                } else {
                    user.setName("Clase sin PDI asociado");
                    userList.add(user);
                }
                incidencePendingUI = IncidencePendingUI.toUI(event.get(0, Event.class), userList, (byte) 1);
                incidencePendingUIList.add(incidencePendingUI);
            }

            response.setData(IncidencePendingUI.eliminaDuplicados(incidencePendingUIList));
            response.setCount(incidencePendingUIList.size());
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);

        if (output.length() > 0) {
            res = Response.ok();
            res.header("Content-Disposition", "attachment; filename = " + output + ".csv");

            res.entity(IncidencePendingUI.toCSV(response.getData()));
            res.type(MediaType.TEXT_PLAIN);

        } else {
            res = Response.ok(response, MediaType.APPLICATION_JSON);
        }

        return res.build();
    }

    private boolean verifyByIncidence(long connectedUserId, IncidenceUI incidenceUI) throws GeneralCPRException {
        Boolean result = false;

        if (verifyUsersService.isAdmin(connectedUserId)) {
            result = true;
        } else if (verifyUsersService.isDepDir(connectedUserId)) {

            List<DepartmentsAreas> departmentsAreas = departmentsAreasService.getDepartmentsAreas();

            if (departmentsAreas != null) {
                for (DepartmentsAreas departmentArea : departmentsAreas) {
                    if (departmentArea.getAreaId() == incidenceUI.getAreaId()) {
                        result = true;
                    }
                }
            }
        } else if (verifyUsersService.isRespArea(connectedUserId)) {

            List<AreasResponsible> areasResponsibles = areasResponsibleService.getAreasResponsiblesByUserId(connectedUserId, -1, -1, null, null);
            if (areasResponsibles != null) {
                for (AreasResponsible areasResponsible : areasResponsibles) {
                    if (areasResponsible.getDepartmentsAreas().getAreaId() == incidenceUI.getAreaId()) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    private boolean verifyByAreaId(long connectedUserId, int areaId) throws GeneralCPRException {
        Boolean result = false;

        if (verifyUsersService.isAdmin(connectedUserId)) {
            result = true;
        } else if (verifyUsersService.isDepDir(connectedUserId)) {

            List<DepartmentsAreas> departmentsAreas = departmentsAreasService.getDepartmentsAreas();

            if (departmentsAreas != null) {
                for (DepartmentsAreas departmentArea : departmentsAreas) {
                    if (departmentArea.getAreaId() == areaId) {
                        result = true;
                    }
                }
            }
        } else if (verifyUsersService.isRespArea(connectedUserId)) {

            List<AreasResponsible> areasResponsibles = areasResponsibleService.getAreasResponsiblesByUserId(connectedUserId, -1, -1, null, null);
            if (areasResponsibles != null) {
                for (AreasResponsible areasResponsible : areasResponsibles) {
                    if (areasResponsible.getDepartmentsAreas().getAreaId() == areaId) {
                        result = true;
                    }
                }
            }
        } else if (verifyUsersService.isPDI(connectedUserId)) {
            User user = userService.getUserById(connectedUserId);
            if (user != null) {
                if (user.getAreaId() == areaId) {
                    result = true;
                }
            }
        }
        return result;
    }

}
