package es.uji.apps.cpr.model;

import com.mysema.query.annotations.QueryProjection;

import java.util.Date;

public class AvisoUsuario {

    private String login;
    private String nameUsuario;
    private Long userId;

    private String code;

    private String tipo;

    private Date endEvent;
    private String nameAsignatura;
    private Date startEvent;
    private String subGrupo;

    public AvisoUsuario() {
    }

    @QueryProjection
    public AvisoUsuario(String login, String nameUsuario, Long userId, String code, String tipo, Date endEvent, String nameAsignatura, Date startEvent, String eventId) {
        this.login = login;
        this.nameUsuario = nameUsuario;
        this.userId = userId;
        this.code = code;
        this.tipo = tipo;
        this.endEvent = endEvent;
        this.nameAsignatura = nameAsignatura;
        this.startEvent = startEvent;
        this.subGrupo = eventId.substring(eventId.indexOf(tipo));
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNameUsuario() {
        return nameUsuario;
    }

    public void setNameUsuario(String nameUsuario) {
        this.nameUsuario = nameUsuario;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getEndEvent() {
        return endEvent;
    }

    public void setEndEvent(Date endEvent) {
        this.endEvent = endEvent;
    }

    public String getNameAsignatura() {
        return nameAsignatura;
    }

    public void setNameAsignatura(String nameAsignatura) {
        this.nameAsignatura = nameAsignatura;
    }

    public Date getStartEvent() {
        return startEvent;
    }

    public void setStartEvent(Date startEvent) {
        this.startEvent = startEvent;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubGrupo() {
        return subGrupo;
    }

    public void setSubGrupo(String subGrupo) {
        this.subGrupo = subGrupo;
    }
}
