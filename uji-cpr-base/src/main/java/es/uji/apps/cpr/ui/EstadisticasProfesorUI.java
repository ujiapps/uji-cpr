package es.uji.apps.cpr.ui;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import es.uji.apps.cpr.model.DepartmentsAreas;
import es.uji.apps.cpr.model.EstadisticasProfesor;

public class EstadisticasProfesorUI
{
    private DepartmentsAreas area;
    private List<EstadisticasProfesor> estadisticasProfesor;
    private Long sumSesiones;
    private Long sumSinIn;
    private Long sumPen;
    private Long sumAbier;
    private Long sumResueltas;
    private Long sumRevocadas;
    private Long sumAuto;

    private Double sumSinInPor;
    private Double sumPenPor;
    private Double sumAbierPor;
    private Double sumResueltasPor;
    private Double sumRevocadasPor;
    private Double sumAutoPor;

    public DepartmentsAreas getArea()
    {
        return area;
    }

    public void setArea(DepartmentsAreas area)
    {
        this.area = area;
    }

    public List<EstadisticasProfesor> getEstadisticasProfesor()
    {
        return estadisticasProfesor;
    }

    public void setEstadisticasProfesor(List<EstadisticasProfesor> estadisticasProfesor)
    {
        this.estadisticasProfesor = estadisticasProfesor;
    }

    public Long getSumSesiones()
    {
        return sumSesiones;
    }

    public void setSumSesiones(Long sumSesiones)
    {
        this.sumSesiones = sumSesiones;
    }

    public Long getSumSinIn()
    {
        return sumSinIn;
    }

    public void setSumSinIn(Long sumSinIn)
    {
        this.sumSinIn = sumSinIn;
    }

    public Long getSumPen()
    {
        return sumPen;
    }

    public void setSumPen(Long sumPen)
    {
        this.sumPen = sumPen;
    }

    public Long getSumAbier()
    {
        return sumAbier;
    }

    public void setSumAbier(Long sumAbier)
    {
        this.sumAbier = sumAbier;
    }

    public Long getSumResueltas()
    {
        return sumResueltas;
    }

    public void setSumResueltas(Long sumResueltas)
    {
        this.sumResueltas = sumResueltas;
    }

    public Long getSumRevocadas()
    {
        return sumRevocadas;
    }

    public void setSumRevocadas(Long sumRevocadas)
    {
        this.sumRevocadas = sumRevocadas;
    }

    public Long getSumAuto()
    {
        return sumAuto;
    }

    public void setSumAuto(Long sumAuto)
    {
        this.sumAuto = sumAuto;
    }

    public Double getSumSinInPor()
    {
        return sumSinInPor;
    }

    public void setSumSinInPor(Double sumSinInPor)
    {
        this.sumSinInPor = sumSinInPor;
    }

    public Double getSumPenPor()
    {
        return sumPenPor;
    }

    public void setSumPenPor(Double sumPenPor)
    {
        this.sumPenPor = sumPenPor;
    }

    public Double getSumAbierPor()
    {
        return sumAbierPor;
    }

    public void setSumAbierPor(Double sumAbierPor)
    {
        this.sumAbierPor = sumAbierPor;
    }

    public Double getSumResueltasPor()
    {
        return sumResueltasPor;
    }

    public void setSumResueltasPor(Double sumResueltasPor)
    {
        this.sumResueltasPor = sumResueltasPor;
    }

    public Double getSumRevocadasPor()
    {
        return sumRevocadasPor;
    }

    public void setSumRevocadasPor(Double sumRevocadasPor)
    {
        this.sumRevocadasPor = sumRevocadasPor;
    }

    public Double getSumAutoPor()
    {
        return sumAutoPor;
    }

    public void setSumAutoPor(Double sumAutoPor)
    {
        this.sumAutoPor = sumAutoPor;
    }

    public void calculaSumatorios()
    {
        this.sumSesiones = 0L;
        this.sumSinIn =0L;
        this.sumPen = 0L;
        this.sumAbier = 0L;
        this.sumResueltas = 0L;
        this.sumRevocadas = 0L;
        this.sumAuto = 0L;
        for (EstadisticasProfesor est : this.getEstadisticasProfesor())
        {
            est.calculaSumatorios();
            this.sumSesiones = this.sumSesiones + est.getTotalSesiones();
            this.sumSinIn = this.sumSinIn + est.getSinIncidencia();
            this.sumPen = this.sumPen + est.getPendientes();
            this.sumAbier = this.sumAbier + est.getIncidenciasAbiertas();
            this.sumResueltas = this.sumResueltas + est.getIncidenciasResueltas();
            this.sumRevocadas = this.sumRevocadas + est.getIncidenciasRevocadas();
            this.sumAuto = this.sumAuto + est.getIncidenciasAuto();
        }


        this.sumSinInPor = sinIncidenciaPor(this.sumSinIn, this.sumSesiones);
        this.sumPenPor = pendientesPor(this.sumSinIn, this.sumSesiones, this.sumPen);
        this.sumAbierPor = incidenciaAbriertasPor(this.sumSinIn, this.sumSesiones, this.sumAbier);
        this.sumResueltasPor = incidenciaResueltasPor(this.sumSinIn, this.sumSesiones, this.sumResueltas);
        this.sumRevocadasPor = incidenciaRevocadaPor(this.sumSinIn, this.sumSesiones, this.sumRevocadas);
        this.sumAutoPor = incidenciaAutoResPor(this.sumSinIn, this.sumSesiones, this.sumAuto);
    }

    public static double round(double value, int places)
    {
        if (places < 0) throw new IllegalArgumentException();

        try
        {
            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        } catch (NumberFormatException e)
        {
            return 0D;
        }

    }

    private Double sinIncidenciaPor(Long sinIncidencia, Long totalSesiones)
    {
        try
        {
            Long incidentadas = totalSesiones - sinIncidencia;
            return round(((incidentadas * 100.0) / totalSesiones), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }

    }

    private Double pendientesPor(Long sinIncidencia, Long totalSesiones, Long pendientes)
    {
        try
        {
            return round(((pendientes * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    private Double incidenciaAbriertasPor(Long sinIncidencia, Long totalSesiones, Long incidenciasAbiertas)
    {
        try
        {
            return round(((incidenciasAbiertas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }

    }

    private Double incidenciaResueltasPor(Long sinIncidencia, Long totalSesiones, Long incidenciasResueltas)
    {
        try
        {
            return round(((incidenciasResueltas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }

    private Double incidenciaRevocadaPor(Long sinIncidencia, Long totalSesiones, Long incidenciasRevocadas)
    {
        try
        {
            return round(((incidenciasRevocadas * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }


    }

    private Double incidenciaAutoResPor(Long sinIncidencia, Long totalSesiones, Long incidenciasAuto)
    {
        try
        {
            return round(((incidenciasAuto * 100.0) / (totalSesiones)), 1);
        } catch (ArithmeticException e)
        {
            return 0D;
        }
    }
}
