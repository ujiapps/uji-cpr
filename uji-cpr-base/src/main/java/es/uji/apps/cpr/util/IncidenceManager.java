package es.uji.apps.cpr.util;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.*;
import es.uji.apps.cpr.ui.CheckUI;
import es.uji.apps.cpr.ui.IncidenceUI;
import es.uji.apps.cpr.ui.UserUI;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class IncidenceManager {

    public static Logger log = LoggerFactory.getLogger(IncidenceManager.class);


    static final int VEINTEMIN_MILIS = 1200000;
    private byte result;
    private boolean auto;
    private IncidenceUI incidenceUI;

    public IncidenceManager() {
    }

    public IncidenceManager(IncidenceUI incidenceUI, Boolean auto) {
        this.incidenceUI = incidenceUI;
        this.result = 0x00;
        this.auto = auto;
    }


    public IncidenceUI createIncidence(List<Check> checksIn) throws IOException, GeneralCPRException {

        CheckUI checkIn = null;

        if (incidenceUI.getCheckIn() != null) {
            // Incidence type 1
            result = BitWise.setBitTo1(result, 0);
            incidenceUI.setStatus(0);
            checkIn = incidenceUI.getCheckIn();

        } else {
            if (checksIn.size() > 0) {
                checkIn = CheckUI.toUI(checksIn.get(0), checksIn.get(0).getUser(), checksIn.get(0).getEvent(), -1);
            } else {
                // Incidence type 1
                result = BitWise.setBitTo1(result, 0);
                incidenceUI.setStatus(0);
            }

        }
//        {id: 1, name: 'IncidenceType.NoEnter'.localize()},
//        {id: 2, name: 'IncidenceType.NoExit'.localize()},
//        {id: 3, name: 'IncidenceType.InvalidDuration'.localize()},
//        {id: 4, name: 'IncidenceType.GenericUser'.localize()},
//        {id: 5, name: 'IncidenceType.OtherLocation'.localize()},
//        {id: 6, name: 'IncidenceType.UnassocUser'.localize()}
        if (checkIn != null) {

            if (!incidenceUI.getEvent().getLocation().equals(checkIn.getLocation())) {
                incidenceUI.setStatus(0);
                if (incidenceUI.getUserUIRequest().getId() == checkIn.getUser().getId()) {
                    long timeCheck = checkIn.getTime().getTime();
                    long timeEvent = incidenceUI.getEvent().getStartEvent().getTime();
                    long ini = timeEvent - VEINTEMIN_MILIS;
                    long fin = timeEvent + VEINTEMIN_MILIS;
                    if (timeCheck > ini && timeCheck < fin) {
                        incidenceUI.setStatus(1);
                    }
                }
                // Incidence type 5
                result = BitWise.setBitTo1(result, 4);
            } else if (checkIn.getUser().getId() == 40000) {
                incidenceUI.setStatus(0);
                long timeCheck = checkIn.getTime().getTime();
                long timeEvent = incidenceUI.getEvent().getStartEvent().getTime();
                long ini = timeEvent - VEINTEMIN_MILIS;
                long fin = timeEvent + VEINTEMIN_MILIS;
                if (timeCheck > ini && timeCheck < fin) {
                    incidenceUI.setStatus(1);
                }
                result = BitWise.setBitTo1(result, 3);
            }


        }

        if (incidenceUI.getUserUIRequest().getId() == 40000) {
            // Incidence type 4
            result = BitWise.setBitTo1(result, 3);
            incidenceUI.setStatus(0);
        }

        if (incidenceUI.getUserUIAssociated() != null) {
            incidenceUI.setStatus(0);
            if (checkIn != null) {
                long timeCheck = checkIn.getTime().getTime();
                long timeEvent = incidenceUI.getEvent().getStartEvent().getTime();
                long ini = timeEvent - VEINTEMIN_MILIS;
                long fin = timeEvent + VEINTEMIN_MILIS;
                if (timeCheck > ini && timeCheck < fin) {
                    if (auto) {
                        incidenceUI.setStatus(1);
                    }
                }
            }
            // Incidence type 6
            result = BitWise.setBitTo1(result, 5);

        }

        incidenceUI.setType(result);

        if (incidenceUI.getInsertTime() == null) {
            incidenceUI.setInsertTime(new Date());
        }

        incidenceUI.setAreaId(incidenceUI.getUserUIRequest().getAreaId());

        return incidenceUI;
    }

    public IncidenceUI updateIncidence(UserUI userUI, IncidenceUI incidenceUINow) throws GeneralCPRException {

        incidenceUI.setId(incidenceUINow.getId());
        incidenceUI.setType(incidenceUINow.getType());
        incidenceUI.setInsertTime(incidenceUINow.getInsertTime());
        incidenceUI.setCheckIn(incidenceUINow.getCheckIn());
        incidenceUI.setDescription(incidenceUINow.getDescription());
        incidenceUI.setUserUIRequest(incidenceUINow.getUserUIRequest());
        incidenceUI.setEvent(incidenceUINow.getEvent());
        incidenceUI.setAreaId(incidenceUINow.getAreaId());
        incidenceUI.setUserUIAssociated(incidenceUINow.getUserUIAssociated());

        if (incidenceUINow.getStatus() == 0) {
            incidenceUI.setStatus(0);
            incidenceUI.setResolution("");
            incidenceUI.setResolutionTime(null);
            incidenceUI.setUserUIResolution(null);

            if (incidenceUI.getCheckIn() != null) {
                incidenceUI.getCheckIn().setEvent(null);
                incidenceUI.getCheckIn().setDirection(null);
            }
        } else {

            if (incidenceUI.getStatus() == 3) {

                if (incidenceUI.getCheckIn() != null) {
                    incidenceUI.getCheckIn().setEvent(incidenceUI.getEvent());
                    incidenceUI.getCheckIn().setDirection(0);
                }
                incidenceUI.setStatus(incidenceUINow.getStatus());

            } else {
                if (incidenceUI.getCheckIn() != null) {
                    incidenceUI.getCheckIn().setEvent(null);
                    incidenceUI.getCheckIn().setDirection(null);
                }
            }

            if (incidenceUINow.getStatus() == 2) {
                incidenceUI.setStatus(incidenceUINow.getStatus());
                enviarMailRevocar(incidenceUI.getUserUIRequest());

            }

            incidenceUI.setResolutionTime(new Date());
            incidenceUI.setResolution(incidenceUINow.getResolution());
            incidenceUI.setUserUIResolution(userUI);
        }

        return incidenceUI;
    }

    public void enviarMailRevocar(UserUI user) throws GeneralCPRException {
        String cuerpo = null;
        try {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cpr/templates/revocarIncidencia.template")));
            cuerpo = MessageFormat.format(cuerpo, user.getName(), "//ujiapps.uji.es/cpr");
            MailMessage mensaje = new MailMessage("CPR");
            mensaje.setTitle("Sol.licitud de revisió d'incidència");
            mensaje.setContentType(MediaType.TEXT_HTML_VALUE);
            mensaje.setSender("Seguiment docent <seguiment_docent@uji.es>");
            mensaje.setContent(cuerpo);
            mensaje.addToRecipient(user.getLogin());
            MessagingClient client = new MessagingClient();
            client.send(mensaje);
        } catch (IOException | MessageNotSentException e) {
            throw new GeneralCPRException("Error al enviar mail.");
        }

    }

    public void enviarMailPermiso(IncidenciaPermiso incidenciaPermiso) throws GeneralCPRException {
        try {
            String cuerpoOrig = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cpr/templates/indicenciaPermiso.template")));
            Map<String, String> directores = Arrays.stream(incidenciaPermiso.getDirector().split(","))
                    .collect(Collectors.toMap(dir -> dir.split("<")[1].replace(">", ""), dir -> dir.split("<")[0]));

            directores.keySet().stream().forEach(correoKey -> {
                String cuerpo = MessageFormat.format(cuerpoOrig, directores.get(correoKey), incidenciaPermiso.getNombreUsuario(), incidenciaPermiso.getDescripcion(), "//ujiapps.uji.es/cpr");
                MailMessage mensaje = new MailMessage("CPR");
                mensaje.setTitle("Avís de revisió d'incidència");
                mensaje.setContentType(MediaType.TEXT_HTML_VALUE);
                mensaje.setSender("Seguiment docent <seguiment_docent@uji.es>");
                mensaje.setContent(cuerpo);
                mensaje.addToRecipient(correoKey);

                MessagingClient client = new MessagingClient();
                try {
                    client.send(mensaje);
                } catch (MessageNotSentException e) {
                    log.error("Error al enviar mail.", e);
                }
            });
        } catch (IOException e) {
            throw new GeneralCPRException("Error al enviar mail.");
        }

    }

    public Incidence createIncidenceAuto(IncidenciasAutoResueltas incidenciasAutoResueltas) {
        Incidence incidence = new Incidence();
        incidence.setDescription("Incidència creada automàticament.");
        incidence.setStatus(1);
        User persona = new User();
        persona.setUserId(incidenciasAutoResueltas.getPersonaId());
        incidence.setUserRequest(persona);
        Event event = new Event();
        event.setEventId(incidenciasAutoResueltas.getId());
        incidence.setEvent(event);
        incidence.setCheckIn(incidenciasAutoResueltas.getCheckIn());
        incidence.setInsertTime(new Date());
        incidence.setAreaId(incidenciasAutoResueltas.getAreaId());
        this.result = 0x00;
        result = BitWise.setBitTo1(result, 0);

        if (incidenciasAutoResueltas.getLocation().equals(incidenciasAutoResueltas.getLocationCheck())) {
            if (incidenciasAutoResueltas.getUserId() == 40000) {
                long timeCheck = incidenciasAutoResueltas.getTime().getTime();
                long timeEvent = incidenciasAutoResueltas.getStartEvent().getTime();
                long ini = timeEvent - VEINTEMIN_MILIS;
                long fin = timeEvent + VEINTEMIN_MILIS;
                if (timeCheck > ini && timeCheck < fin) {
                    incidence.setStatus(1);
                }
                result = BitWise.setBitTo1(result, 3);
                incidence.setDescription("Incidència auto-resolta automàticament. La sessió s'ha registrat amb una targeta genèrica.");
            } else if (!incidenciasAutoResueltas.getPersonaId().equals(incidenciasAutoResueltas.getUserId())) {
                // Incidence type 6
                User user = new User();
                user.setUserId(incidenciasAutoResueltas.getUserId());
                incidence.setUserIDAssociated(user);
                result = BitWise.setBitTo1(result, 5);
                incidence.setDescription("Incidència auto-resolta automàticament. La sessió ha sigut registrada per un altre professor de l'assignatura.");
            }
        } else {
            if (incidenciasAutoResueltas.getPersonaId().equals(incidenciasAutoResueltas.getUserId())) {
                long timeCheck = incidenciasAutoResueltas.getTime().getTime();
                long timeEvent = incidenciasAutoResueltas.getStartEvent().getTime();
                long ini = timeEvent - VEINTEMIN_MILIS;
                long fin = timeEvent + VEINTEMIN_MILIS;
                if (timeCheck > ini && timeCheck < fin) {
                    incidence.setStatus(1);
                }
                // Incidence type 5
                result = BitWise.setBitTo1(result, 4);
                incidence.setDescription("Incidència auto-resolta automàticament. El professor ha registrat la sessió en una altra ubicació.");

            }
        }


        incidence.setType(result);
        return incidence;
    }

    public Incidence createIncidencePermiso(IncidenciaPermiso incidenciaPermiso) {
        Incidence incidence = new Incidence();
        incidence.setStatus(1);
        User persona = new User();
        persona.setUserId(incidenciaPermiso.getPersonaId());
        incidence.setUserRequest(persona);
        Event event = new Event();
        event.setEventId(incidenciaPermiso.getEventId());
        incidence.setEvent(event);
        incidence.setInsertTime(new Date());
        incidence.setAreaId(incidenciaPermiso.getAreaId());
        incidence.setDescription("Incidència creada automàticament - ".concat(incidenciaPermiso.getDescripcion()));
        incidence.setType(7);
        incidence.setStatus(0);
        return incidence;
    }


    public Incidence createIncidenceEventPendent(EventPendent eventPendent) {
        Incidence incidence = new Incidence();
        incidence.setStatus(1);
        User persona = new User();
        persona.setUserId(eventPendent.getPersonaId());
        incidence.setUserRequest(persona);
        Event event = new Event();
        event.setEventId(eventPendent.getEventId());
        incidence.setEvent(event);
        incidence.setInsertTime(new Date());
        incidence.setAreaId(eventPendent.getAreaId());
        incidence.setDescription("Incidència oberta automàticament després de tres setmanes pendent");
        incidence.setType(1);
        incidence.setStatus(0);
        return incidence;

    }
}
