package es.uji.apps.cpr.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cpr.model.User;
import es.uji.apps.cpr.services.UserService;
import es.uji.apps.cpr.ui.ResponseListUI;
import es.uji.apps.cpr.ui.UserUI;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

@Path("users")
public class UsersResources extends CoreBaseService {

    @InjectParam
    private UserService userService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<UserUI> getUsers(@QueryParam("query") String query) {

        ResponseListUI<UserUI> response = new ResponseListUI<UserUI>();
        String msg = "OK";
        Boolean success = true;

        try {
            if (query.length() < 3) {
                response.setData(null);
                response.setCount(0);
            } else {
                List<User> users = userService.getUsersByQuery(query);
                response.setData(UserUI.toUI(users));
                response.setCount(users.size());
            }
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;

    }

    @GET
    @Path("area")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<UserUI> searchUsersByArea(@QueryParam("query") String query) {

        ResponseListUI<UserUI> response = new ResponseListUI<UserUI>();
        String msg = "OK";
        Boolean success = true;
        Long userId = AccessManager.getConnectedUserId(request);
        try {
            if (query.length() < 3) {
                response.setData(null);
                response.setCount(0);
            } else {
                User user = userService.getUserById(userId);
                List<User> users = userService.searchUsersByArea(user.getAreaId(), query);
                response.setData(UserUI.toUI(users));
                response.setCount(users.size());
            }
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;

    }

    @GET
    @Path("asignatura/{code}/area/{areaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseListUI<UserUI> getUserByCode(@PathParam("code") String code, @PathParam("areaId") Integer areaId) {

        ResponseListUI<UserUI> response = new ResponseListUI<UserUI>();
        String msg = "OK";
        Boolean success = true;
        Long userId = AccessManager.getConnectedUserId(request);
        try {
                List<User> users = userService.getUsersByCodeAsignatura(code,areaId);
                response.setData(UserUI.toUI(users));
                response.setCount(users.size());
        } catch (Exception e) {
            success = false;
            msg = e.getLocalizedMessage();
        }
        response.setSuccess(success);
        response.setMessage(msg);
        return response;

    }

}
