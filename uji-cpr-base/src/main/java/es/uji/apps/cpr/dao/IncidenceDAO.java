package es.uji.apps.cpr.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.*;
import es.uji.apps.cpr.util.DynamicFilter;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class IncidenceDAO extends BaseDAODatabaseImpl
{


    public Incidence getIncidenceById(Long id) throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);


        QIncidence qIncidence = QIncidence.incidence;

        return query.from(qIncidence).where(qIncidence.id.eq(id).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate()))).uniqueResult(qIncidence);
    }

    public List<Incidence> getIncidencesByUserId(long userId, int start, int limit,
                                                 List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;
        QUser qUser = QUser.user;

        query.from(qIncidence).where(qIncidence.userRequest.userId.eq(userId).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qUser.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qIncidence.id);
        validSorts.put("type", qIncidence.type);
        validSorts.put("description", qIncidence.description);
        validSorts.put("status", qIncidence.status);
        validSorts.put("userIdRequest", qUser.userId);
        validSorts.put("event", qIncidence.event.eventId);
        validSorts.put("resolution", qIncidence.resolution);
        validSorts.put("userIdResolution", qIncidence.resolution);
        validSorts.put("checkIn", qIncidence.checkIn.id);
        validSorts.put("checkOut", qIncidence.checkOut.id);
        validSorts.put("date", qIncidence.insertTime);
        validSorts.put("date", qIncidence.event.startEvent);
        validSorts.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qIncidence);
    }

    public List<Incidence> getIncidencesByUserIds(List<Long> userIds, int start, int limit,
                                                  List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;
        QUser qUser = QUser.user;

        query.from(qIncidence).where(qIncidence.userRequest.userId.in(userIds).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qUser.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qIncidence.id);
        validSorts.put("type", qIncidence.type);
        validSorts.put("description", qIncidence.description);
        validSorts.put("status", qIncidence.status);
        validSorts.put("userIdRequest", qUser.userId);
        validSorts.put("eventId", qIncidence.event.eventId);
        validSorts.put("resolution", qIncidence.resolution);
        validSorts.put("userIdResolution", qIncidence.resolution);
        validSorts.put("checkIn", qIncidence.checkIn.id);
        validSorts.put("checkOut", qIncidence.checkOut.id);
        validSorts.put("insertTime", qIncidence.insertTime);
        validSorts.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qIncidence);
    }

    public List<Incidence> getIncidenceByAreaId(int areaId, int start, int limit,
                                                List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;
        QUser qUser = QUser.user;

        query.from(qIncidence).where(qIncidence.areaId.eq(areaId).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qUser.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qIncidence.id);
        validSorts.put("type", qIncidence.type);
        validSorts.put("description", qIncidence.description);
        validSorts.put("status", qIncidence.status);
        validSorts.put("userIdRequest", qUser.userId);
        validSorts.put("eventId", qIncidence.event.eventId);
        validSorts.put("resolution", qIncidence.resolution);
        validSorts.put("userIdResolution", qIncidence.resolution);
        validSorts.put("checkIn", qIncidence.checkIn.id);
        validSorts.put("checkOut", qIncidence.checkOut.id);
        validSorts.put("insertTime", qIncidence.insertTime);
        validSorts.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qIncidence);
    }

    public List<Incidence> getIncidenceByAreaIds(List<Integer> areaId, int start, int limit,
                                                 List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;
        QUser qUserReq = QUser.user;
        QUser qUserRes = QUser.user;
        QUser qUserAssoc = QUser.user;
        QEvent qEvent = QEvent.event;
        QCheck qCheckOut = QCheck.check;
        QCheck qCheckIn = QCheck.check;


        query.from(qIncidence)
                .leftJoin(qIncidence.userRequest, qUserReq).fetch()
                .leftJoin(qIncidence.userResolution,qUserRes).fetch()
                .leftJoin(qIncidence.userIDAssociated, qUserAssoc).fetch()
                .leftJoin(qIncidence.event, qEvent).fetch()
                .leftJoin(qIncidence.checkIn, qCheckIn).fetch()
                .leftJoin(qIncidence.checkOut, qCheckOut).fetch()
                .where(qIncidence.areaId.in(areaId).and(qEvent.startEvent.gt(GeneralUtils.getInitialDate())));

//        Map<String, Object> validFilters = new HashMap<String, Object>();
//        validFilters.put("id", qIncidence.id);
//        validFilters.put("type", qIncidence.type);
//        validFilters.put("description", qIncidence.description);
//        validFilters.put("status", qIncidence.status);
//        validFilters.put("userIdRequest", qUser.userId);
//        validFilters.put("eventId", qIncidence.event.eventId);
//        validFilters.put("resolution", qIncidence.resolution);
//        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
//        validFilters.put("checkIn", qIncidence.checkIn.id);
//        validFilters.put("checkOut", qIncidence.checkOut.id);
//        validFilters.put("insertTime", qIncidence.insertTime);
//        validFilters.put("resolutionTime", qIncidence.resolutionTime);
//        validFilters.put("areaId", qIncidence.areaId);
//
//        query.where(DynamicFilter.applyFilter(filterList, validFilters));
//
//        Map<String, Object> validSorts = new HashMap<String, Object>();
//        validSorts.put("id", qIncidence.id);
//        validSorts.put("type", qIncidence.type);
//        validSorts.put("description", qIncidence.description);
//        validSorts.put("status", qIncidence.status);
//        validSorts.put("userIdRequest", qUser.userId);
//        validSorts.put("eventId", qIncidence.event.eventId);
//        validSorts.put("resolution", qIncidence.resolution);
//        validSorts.put("userIdResolution", qIncidence.resolution);
//        validSorts.put("checkIn", qIncidence.checkIn.id);
//        validSorts.put("checkOut", qIncidence.checkOut.id);
//        validSorts.put("insertTime", qIncidence.insertTime);
//        validSorts.put("resolutionTime", qIncidence.resolutionTime);
//        validFilters.put("areaId", qIncidence.areaId);
//
//        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
//        {
//            query.orderBy(o);
//        }
//        if (start != -1) {
//            query.offset(start).limit(limit);
//        }

        return query.list(qIncidence);
    }

    public long getIncidencesByAreaIdCount(int areaId, List<ItemFilter> filterList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;
        QUser qUser = QUser.user;

        query.from(qIncidence).where(qIncidence.areaId.eq(areaId).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qUser.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        return query.count();
    }

    public long getIncidencesByAreaIdsCount(List<Integer> areaId, List<ItemFilter> filterList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;
        QUser qUser = QUser.user;

        query.from(qIncidence).where(qIncidence.areaId.in(areaId).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qUser.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        return query.count();
    }

    public List<Incidence> getIncidenceByEventId(String eventId) throws GeneralCPRException
    {

        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;

        query.from(qIncidence).where(qIncidence.event.eventId.eq(eventId).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        return query.list(qIncidence);
    }

    public List<Incidence> getIncidencesByCode(String code, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;

        query.from(qIncidence).where(qIncidence.event.code.eq(code).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qIncidence.userRequest.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qIncidence.id);
        validSorts.put("type", qIncidence.type);
        validSorts.put("description", qIncidence.description);
        validSorts.put("status", qIncidence.status);
        validSorts.put("userIdRequest", qIncidence.userRequest.userId);
        validSorts.put("eventId", qIncidence.event.eventId);
        validSorts.put("resolution", qIncidence.resolution);
        validSorts.put("userIdResolution", qIncidence.resolution);
        validSorts.put("checkIn", qIncidence.checkIn.id);
        validSorts.put("checkOut", qIncidence.checkOut.id);
        validSorts.put("insertTime", qIncidence.insertTime);
        validSorts.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }
        if (start != -1)
        {
            query.offset(start).limit(limit);
        }

        return query.list(qIncidence);
    }

    public long getIncidencesByCodeCount(String code, List<ItemFilter> filterList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;

        query.from(qIncidence).where(qIncidence.event.code.eq(code).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qIncidence.userRequest.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        return query.count();
    }

    public List<Incidence> getIncidencesByCodes(List<String> codes, int start, int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;

        QUser qUserReq = QUser.user;
        QUser qUserRes = QUser.user;
        QUser qUserAssoc = QUser.user;
        QEvent qEvent = QEvent.event;
        QCheck qCheckOut = QCheck.check;
        QCheck qCheckIn = QCheck.check;

        query.from(qIncidence)
                .leftJoin(qIncidence.userRequest, qUserReq).fetch()
                .leftJoin(qIncidence.userResolution, qUserRes).fetch()
                .leftJoin(qIncidence.userIDAssociated, qUserAssoc).fetch()
                .leftJoin(qIncidence.event, qEvent).fetch()
                .leftJoin(qIncidence.checkIn, qCheckIn).fetch()
                .leftJoin(qIncidence.checkOut, qCheckOut).fetch()
                .where(qEvent.code.in(codes).and(qEvent.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qIncidence.userRequest.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        Map<String, Object> validSorts = new HashMap<String, Object>();
        validSorts.put("id", qIncidence.id);
        validSorts.put("type", qIncidence.type);
        validSorts.put("description", qIncidence.description);
        validSorts.put("status", qIncidence.status);
        validSorts.put("userUIRequest", qIncidence.userRequest.name);
        validSorts.put("eventId", qIncidence.event.eventId);
        validSorts.put("resolution", qIncidence.resolution);
        validSorts.put("userUIResolution", qIncidence.resolution);
        validSorts.put("checkIn", qIncidence.checkIn.id);
        validSorts.put("checkOut", qIncidence.checkOut.id);
        validSorts.put("insertTime", qIncidence.insertTime);
        validSorts.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        for (OrderSpecifier o : DynamicFilter.applySort(sortList, validSorts))
        {
            query.orderBy(o);
        }

        return query.list(qIncidence);
    }

    public long getIncidencesByCodesCount(List<String> codes, List<ItemFilter> filterList)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIncidence qIncidence = QIncidence.incidence;

        query.from(qIncidence).where(qIncidence.event.code.in(codes).and(qIncidence.event.startEvent.gt(GeneralUtils.getInitialDate())));

        Map<String, Object> validFilters = new HashMap<String, Object>();
        validFilters.put("id", qIncidence.id);
        validFilters.put("type", qIncidence.type);
        validFilters.put("description", qIncidence.description);
        validFilters.put("status", qIncidence.status);
        validFilters.put("userIdRequest", qIncidence.userRequest.userId);
        validFilters.put("eventId", qIncidence.event.eventId);
        validFilters.put("resolution", qIncidence.resolution);
        validFilters.put("userIdResolution", qIncidence.userResolution.userId);
        validFilters.put("checkIn", qIncidence.checkIn.id);
        validFilters.put("checkOut", qIncidence.checkOut.id);
        validFilters.put("insertTime", qIncidence.insertTime);
        validFilters.put("resolutionTime", qIncidence.resolutionTime);
        validFilters.put("areaId", qIncidence.areaId);

        query.where(DynamicFilter.applyFilter(filterList, validFilters));

        return query.count();
    }

}
