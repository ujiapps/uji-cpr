package es.uji.apps.cpr.ui;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import es.uji.apps.cpr.model.Check;
import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.model.Incidence;
import es.uji.apps.cpr.model.User;
import es.uji.apps.cpr.util.CSVUtils;

@XmlRootElement(name = "IncidenceUI")
@JsonIgnoreProperties(ignoreUnknown = true)
public class IncidenceUI implements Serializable {

    private Long id;
    private byte type;
    private String description;
    private int status;
    private UserUI userUIRequest;
    private EventUI event;
    private String resolution;
    private UserUI userUIResolution;
    private CheckUI checkIn;
    private CheckUI checkOut;
    private Date insertTime;
    private Date resolutionTime;
    private Integer areaId;
    private UserUI userUIAssociated;

    public IncidenceUI() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public UserUI getUserUIRequest() {
        return userUIRequest;
    }

    public void setUserUIRequest(UserUI userUIRequest) {
        this.userUIRequest = userUIRequest;
    }

    public EventUI getEvent() {
        return event;
    }

    public void setEvent(EventUI event) {
        this.event = event;
    }

    public UserUI getUserUIResolution() {
        return userUIResolution;
    }

    public void setUserUIResolution(UserUI userUIResolution) {
        this.userUIResolution = userUIResolution;
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public Date getResolutionTime() {
        return resolutionTime;
    }

    public void setResolutionTime(Date resolutionTime) {
        this.resolutionTime = resolutionTime;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public CheckUI getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(CheckUI checkIn) {
        this.checkIn = checkIn;
    }

    public CheckUI getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(CheckUI checkOut) {
        this.checkOut = checkOut;
    }

    public UserUI getUserUIAssociated() {
        return userUIAssociated;
    }

    public void setUserUIAssociated(UserUI userUIAssociated) {
        this.userUIAssociated = userUIAssociated;
    }

    public static IncidenceUI toUI(Incidence incidence) {
        if (incidence == null) return null;

        User userUIRequest = incidence.getUserRequest();
        User userUIResolution = incidence.getUserResolution();
        Event event = incidence.getEvent();
        Check checkIn = incidence.getCheckIn();
        Check checkOut = incidence.getCheckOut();
        User userUIAssociated = incidence.getUserIDAssociated();

        IncidenceUI incidenceUI = new IncidenceUI();
        incidenceUI.setId(incidence.getId());
        if (event != null)
            incidenceUI.setEvent(EventUI.toUI(event));
        incidenceUI.setDescription(incidence.getDescription());
        incidenceUI.setResolution(incidence.getResolution());
        incidenceUI.setStatus(incidence.getStatus());
        incidenceUI.setType((byte) incidence.getType());
        if (userUIRequest != null)
            incidenceUI.setUserUIRequest(UserUI.toUI(userUIRequest, null));
        if (userUIResolution != null)
            incidenceUI.setUserUIResolution(UserUI.toUI(userUIResolution, null));
        if (checkIn != null)
            incidenceUI.setCheckIn(CheckUI.toUI(checkIn, null, null, -1));
        if (checkOut != null)
            incidenceUI.setCheckOut(CheckUI.toUI(checkOut, null, null, -1));
        if (userUIAssociated != null)
            incidenceUI.setUserUIAssociated(UserUI.toUI(userUIAssociated, null));
        incidenceUI.setInsertTime(incidence.getInsertTime());
        incidenceUI.setResolutionTime(incidence.getResolutionTime());
        incidenceUI.setAreaId(incidence.getAreaId());

        return incidenceUI;
    }

    public static List<IncidenceUI> toUI(List<Incidence> incidences) {
        List<IncidenceUI> incidenceUIList = new ArrayList<IncidenceUI>();
        for (Incidence incidence : incidences) {
            incidenceUIList.add(toUI(incidence));
        }
        return incidenceUIList;
    }

    public static Incidence toModel(IncidenceUI incidenceUI) {

        Incidence incidence = new Incidence();
        if (incidenceUI.getId() != null) {
            incidence.setId(incidenceUI.getId());
        }
        Event event = new Event();
        event.setEventId(incidenceUI.getEvent().getId());
        incidence.setEvent(event);
        User userRequest = new User();
        userRequest.setUserId(incidenceUI.getUserUIRequest().getId());
        incidence.setUserRequest(userRequest);
        if (incidenceUI.getUserUIResolution() != null) {
            User userResolution = new User();
            userResolution.setUserId(incidenceUI.getUserUIResolution().getId());
            incidence.setUserResolution(userResolution);
        }
        if (incidenceUI.getCheckIn() != null) {
            Check checkIn = new Check();
            checkIn.setId(incidenceUI.getCheckIn().getId());
            incidence.setCheckIn(checkIn);
        }
        if (incidenceUI.getCheckOut() != null) {
            Check checkOut = new Check();
            checkOut.setId(incidenceUI.getCheckOut().getId());
            incidence.setCheckOut(checkOut);
        }
        if (incidenceUI.getUserUIAssociated() != null) {
            User userAssociated = new User();
            userAssociated.setUserId(incidenceUI.getUserUIAssociated().getId());
            incidence.setUserIDAssociated(userAssociated);
        }

        incidence.setResolutionTime(incidenceUI.getResolutionTime());
        incidence.setResolution(incidenceUI.getResolution());
        incidence.setInsertTime(incidenceUI.getInsertTime());
        incidence.setDescription(incidenceUI.getDescription());
        incidence.setStatus(incidenceUI.getStatus());
        incidence.setType(incidenceUI.getType());
        incidence.setAreaId(incidenceUI.getAreaId());

        return incidence;
    }

    public static String toCSV(List<IncidenceUI> incidences, boolean user) {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = null;

        try {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<String[]>();

            List<String> record = null;

            if (user) {
                record = Arrays.asList("ESTAT", "TIPUS", "USUARI", "EVENT", "DATA D´INICI CLASE", "INFORME PROF.", "COMENTARI RESP.");

            } else {
                record = Arrays.asList("ESTAT", "TIPUS", "EVENT", "DATA D´INICI CLASE", "INFORME PROF.", "COMENTARI RESP.");
            }

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (IncidenceUI incidence : incidences) {

                String subTipo = incidence.getEvent().getId().substring(incidence.getEvent().getId().length() - 5);
                Integer tipoIndex = subTipo.indexOf(incidence.getEvent().getTipoId());
                String tipo =  subTipo.substring(tipoIndex);
                if (user) {
                    record = Arrays.asList(String.valueOf(CSVUtils.parseStatus(incidence.getStatus())),
                            String.valueOf(CSVUtils.parseType(incidence.getType())),
                            String.valueOf(incidence.getUserUIRequest() == null ? "" : incidence.getUserUIRequest().getName().toString().concat(" " + incidence.getUserUIRequest().getSurname())),
                            String.valueOf(incidence.getEvent().getName() == null ? "" : incidence.getEvent().getCode() + " - " + tipo +" - "+incidence.getEvent().getName()),
                            String.valueOf(incidence.getEvent().getStartEvent() == null ? "" : incidence.getEvent().getStartEvent()),
                            String.valueOf(incidence.getDescription() == null ? "" : incidence.getDescription()),
                            String.valueOf(incidence.getResolution() == null ? "" : incidence.getResolution()));
                } else {
                    record = Arrays.asList(String.valueOf(CSVUtils.parseStatus(incidence.getStatus())),
                            String.valueOf(CSVUtils.parseType(incidence.getType())),
                            String.valueOf(incidence.getEvent().getName() == null ? "" : incidence.getEvent().getCode()+ " - " + tipo +" - "+incidence.getEvent().getName()),
                            String.valueOf(incidence.getEvent().getStartEvent() == null ? "" : incidence.getEvent().getStartEvent()),
                            String.valueOf(incidence.getDescription() == null ? "" : incidence.getDescription()),
                            String.valueOf(incidence.getResolution() == null ? "" : incidence.getResolution()));
                }

                recordArray = new String[record.size()];
                record.toArray(recordArray);
                records.add(recordArray);
            }

            csvWriter.writeAll(records);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

}
