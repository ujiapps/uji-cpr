package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CPR_VW_DEPARTAMENTOS_AREAS", schema = "UJI_CONTROLPRESENCIA")
public class DepartmentsAreas implements Serializable {

    @Column(name = "DEP_ID")
    private int depId;

    @ManyToMany
    @JoinTable(name = "CPR_VW_DEPARTAMENTOS")
    private Set<Departments> departments;

    @Column(name = "DEPARTAMENTO")
    private String department;

    @Id
    @Column(name = "AREA_ID")
    private int areaId;

    @Column(name = "AREA")
    private String area;

    @OneToMany(mappedBy = "departmentsAreas", fetch = FetchType.EAGER)
    private Set<AreasResponsible> areasResponsible;

    public int getDepId() {
        return depId;
    }

    public void setDepId(int depId) {
        this.depId = depId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Set<AreasResponsible> getAreasResponsible() {
        return areasResponsible;
    }

    public void setAreasResponsible(Set<AreasResponsible> areasResponsible) {
        this.areasResponsible = areasResponsible;
    }

    public Set<Departments> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Departments> departments) {
        this.departments = departments;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
