package es.uji.apps.cpr.dao;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.DirDegree;
import es.uji.apps.cpr.model.QDirDegree;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DirDegreeDAO extends BaseDAODatabaseImpl {

    public DirDegree getDirDegreeByIdDegree(int degreeId) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);
        QDirDegree qDirDegree = QDirDegree.dirDegree;

        query.from(qDirDegree).where(qDirDegree.degreeId.eq(degreeId));

        return query.uniqueResult(qDirDegree);
    }

    public DirDegree getDirDegreeByUserId(long userId) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);
        QDirDegree qDirDegree = QDirDegree.dirDegree;

        query.from(qDirDegree).where(qDirDegree.userId.eq(userId));

        return query.uniqueResult(qDirDegree);
    }

    public List<DirDegree> getDirDegreeByUsersId(long userId) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);
        QDirDegree qDirDegree = QDirDegree.dirDegree;

        query.from(qDirDegree).where(qDirDegree.userId.eq(userId));

        return query.list(qDirDegree);
    }

}
