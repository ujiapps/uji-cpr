package es.uji.apps.cpr.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CPR_VW_KEYS", schema = "UJI_CONTROLPRESENCIA")
public class UserKey implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "KEY")
    private String key;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private User user;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "TYPE")
    private String type;

    public UserKey() {
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
