package es.uji.apps.cpr.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.TerminalDAO;
import es.uji.apps.cpr.model.Terminal;

@Service
public class TerminalService
{
    static final Logger logger = LoggerFactory.getLogger(CheckService.class);

    @Autowired
    private TerminalDAO terminalDAO;

    public Terminal getTerminalById(Long id)
    {
        try
        {
            return terminalDAO.getTerminalById(id);
        }
        catch (GeneralCPRException ex)
        {
            logger.info("No existen los Terminales para el identificador propuesto.", ex);
        }
        return null;
    }

}
