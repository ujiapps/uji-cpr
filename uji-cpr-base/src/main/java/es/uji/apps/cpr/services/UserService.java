package es.uji.apps.cpr.services;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.UserDAO;
import es.uji.apps.cpr.model.AvisoUsuario;
import es.uji.apps.cpr.model.User;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.commons.db.BaseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BaseDAO baseDAO;

    private static String PERIODO_DOCENTE;

    @Value("${uji.cpr.periodoDocente}")
    private void setPeridoDocente(String periodoDocente) { PERIODO_DOCENTE = periodoDocente; };

    public static String getPeriodoDocente()
    {
        return PERIODO_DOCENTE;
    }

    public static void setPeriodoDocente(String periodoDocente)
    {
        PERIODO_DOCENTE = periodoDocente;
    }

    public User getUserById(long userId) throws GeneralCPRException {
        return userDAO.getUserById(userId);
    }

    public List<User> getUsersByQuery(String query) throws GeneralCPRException {
        return userDAO.getUsersByQuery(query, null);
    }

    public List<User> searchUsersByArea(Integer areaId, String query)
    {
        return userDAO.searchUsersByArea(areaId,query);
    }

    public List<User> getUsersByAreaId(int areaId, int start, int limit) throws GeneralCPRException {
        return userDAO.getUsersByAreaId(areaId, start, limit);
    }
    public List<User> getUsersPDIByDates(long userId, String eventId, Date startEvent, Date endEvent, List<ItemFilter> filterList, int start, int limit) throws GeneralCPRException, ParseException {
        return userDAO.getUsersPDIByDates(userId, eventId, startEvent, endEvent, filterList, start, limit);
    }

    public List<User> getUsersByLocation(long userId, String location, Date startEvent, Date endEvent, List<ItemFilter> filterList, int start, int limit) throws GeneralCPRException, ParseException {
        return userDAO.getUsersByLocation(userId, location, startEvent, endEvent, filterList, start, limit);
    }

    public List<User> getUsersPDIByEventId(String eventId) {
        return userDAO.getUsersPDIByEventId(eventId);
    }
    public List<User> getUsersConAvisoDia(Date dia)
    {
        return userDAO.getUsersConAvisoDia(dia);
    }
    public List<AvisoUsuario> getUsersConAvisoSemanal(Date diaInicial, Date diaFinal) {
        return userDAO.getUsersConAvisoSemanal(diaInicial, diaFinal);
    }
    public List<User> getDirectoresAreaAviso(Date diaInicial, Date diaFinal) {
        return userDAO.getDirectoresAreaAviso(diaInicial, diaFinal);
    }

    public List<User> getDirectoresTitulacionAviso(Date diaInicial, Date diaFinal) {
        return userDAO.getDirectoresTitulacionAviso(diaInicial, diaFinal);
    }

    public List<User> getUsersByCodeAsignatura(String code, Integer areaId)
    {

        return userDAO.getUsersByCodeAsignatura(code,areaId);
    }
}
