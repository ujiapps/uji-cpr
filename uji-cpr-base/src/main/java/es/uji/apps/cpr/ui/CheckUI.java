package es.uji.apps.cpr.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import es.uji.apps.cpr.model.Check;
import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.model.User;

public class CheckUI implements Serializable {
    private long id;
    private long idTerminal;
    private String location;
    private UserUI user;
    private EventUI event;
    private String key;
    private Date time;
    private Date itime;
    private String service;
    private int type;
    private int status;
    private Integer direction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdTerminal() {
        return idTerminal;
    }

    public void setIdTerminal(long idTerminal) {
        this.idTerminal = idTerminal;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public UserUI getUser() {
        return user;
    }

    public void setUser(UserUI user) {
        this.user = user;
    }

    public EventUI getEvent() {
        return event;
    }

    public void setEvent(EventUI event) {
        this.event = event;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getItime() {
        return itime;
    }

    public void setItime(Date itime) {
        this.itime = itime;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public static CheckUI toUI(Check check, User user, Event event, int status) {
        CheckUI checkUI = new CheckUI();
        checkUI.setId(check.getId());
        if (user != null) {
            checkUI.setUser(UserUI.toUI(user, null));
        }
        if (event != null) {
            checkUI.setEvent(EventUI.toUI(event));
        }
        if (check.getTerminal() != null) {
            checkUI.setLocation(check.getTerminal().getLocation());
            checkUI.setIdTerminal(check.getTerminal().getId());
        }
        checkUI.setItime(check.getItime());
        checkUI.setKey(check.getKey());
        checkUI.setTime(check.getTime());
        checkUI.setType(check.getType());
        checkUI.setService(check.getService());
        checkUI.setStatus(status);
        if (check.getDirection() != null) {
            checkUI.setDirection(check.getDirection());
        }


        return checkUI;

    }

    public static CheckUI toUIByUI(Check check, UserUI userUI, EventUI eventUI, int status) {
        CheckUI checkUI = new CheckUI();
        checkUI.setId(check.getId());
        if (userUI != null) {
            checkUI.setUser(userUI);
        }
        if (eventUI != null) {
            checkUI.setEvent(eventUI);
        }
        checkUI.setLocation(check.getTerminal().getLocation());
        checkUI.setIdTerminal(check.getTerminal().getId());
        checkUI.setItime(check.getItime());
        checkUI.setKey(check.getKey());
        checkUI.setTime(check.getTime());
        checkUI.setType(check.getType());
        checkUI.setService(check.getService());
        checkUI.setStatus(status);
        if (check.getDirection() != null) {
            checkUI.setDirection(check.getDirection());
        }

        return checkUI;

    }

    public static List<CheckUI> toUI(List<Check> checks) {
        List<CheckUI> checkUIList = new ArrayList<CheckUI>();
        for (Check check : checks) {
            checkUIList.add(toUI(check, null, null, -1));
        }
        return checkUIList;
    }

    public static List<CheckUI> toUI(Set<Check> checks) {
        List<CheckUI> checkUIList = new ArrayList<CheckUI>();

        if (checks != null) {
            for (Check check : checks) {
                checkUIList.add(toUI(check, null, null, -1));
            }
        }
        return checkUIList;
    }
}
