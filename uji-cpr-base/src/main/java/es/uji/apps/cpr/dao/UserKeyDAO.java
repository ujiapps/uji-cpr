package es.uji.apps.cpr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.QUserKey;
import es.uji.apps.cpr.model.UserKey;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class UserKeyDAO extends BaseDAODatabaseImpl {

    public List<UserKey> getUserKeysByUserId(long userId) throws GeneralCPRException {
        JPAQuery query = new JPAQuery(entityManager);

        QUserKey qUserKey = QUserKey.userKey;

        query.from(qUserKey).where(qUserKey.user.userId.eq(userId));

        return query.list(qUserKey);
    }

}
