package es.uji.apps.cpr.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;

import es.uji.apps.cpr.model.QResumenEventos;
import es.uji.apps.cpr.model.QUser;
import es.uji.apps.cpr.model.ResumenEventos;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class ResumenEventosDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem>
{
    private QResumenEventos qResumenEventos = QResumenEventos.resumenEventos;
    private QUser qUser = QUser.user;

    public List<ResumenEventos> getResumenEventos(Long personaId, String codigo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qResumenEventos);
        if (!codigo.equals("-1"))
        {
            query.where(qResumenEventos.id.upper().like("%" + codigo.toUpperCase() + "%"));
        }
        if (!personaId.equals(-1L))
        {
            query.where(qResumenEventos.personaId.eq(personaId));
        }
        query.orderBy(qResumenEventos.startEvent.desc());

        return query.list(qResumenEventos);
    }

    @Override
    public List<LookupItem> search(String queryString)
    {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(queryString);
        List<LookupItem> result = new ArrayList<>();

        Long queryStringLong = 0L;

        try
        {
            queryStringLong = Long.parseLong(queryString);
        }
        catch (Exception e)
        {
        }

        query.from(qUser)
                .where(qUser.nombreBusqueda.lower()
                        .like("%" + cadenaLimpia.toLowerCase() + "%")
                        .or(qUser.login.lower()
                                .like("%" + cadenaLimpia.toLowerCase() + "%")
                                .or(qUser.userId.eq(queryStringLong))));


        List<Tuple> listaPersonas = query.distinct()
                .list(new QTuple(qUser.userId, qUser.login, qUser.name, qUser.surname1));

        for (Tuple persona : listaPersonas)
        {
            LookupItem item = new LookupItem();
            item.setId(String.valueOf(persona.get(qUser.userId)));
            item.setNombre(persona.get(qUser.name) + " " + persona.get(qUser.surname1));
            item.addExtraParam("cuenta", persona.get(qUser.login));

            result.add(item);
        }
        return result;

    }
}
