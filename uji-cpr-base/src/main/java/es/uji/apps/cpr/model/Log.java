package es.uji.apps.cpr.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "CPR_MENSAJES_LOG", schema = "UJI_CONTROLPRESENCIA")
@Entity
public class Log
{
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "PER_ID")
    private Long perId;

    @Column(name = "CUENTA")
    private String cuenta;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "ESTADO")
    private String estado;

    public Log(Long perId, String cuenta, Date fecha, String tipo, String estado)
    {
        this.perId = perId;
        this.cuenta = cuenta;
        this.fecha = fecha;
        this.tipo = tipo;
        this.estado = estado;
    }

    public Log()
    {
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getCuenta()
    {
        return cuenta;
    }

    public void setCuenta(String cuenta)
    {
        this.cuenta = cuenta;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }
}
