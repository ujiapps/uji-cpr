package es.uji.apps.cpr.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GeneralUtils
{
    @Value("${uji.cpr.initialDate}")
    private static String date;
    @Value("${uji.cpr.periodoDocente}")
    private static String periodoDocente;
    private static Date initialDate;
    @Value("${uji.cpr.cursoActual}")
    private static String cursoActual;

    @Autowired
    public GeneralUtils(@Value("${uji.cpr.initialDate}") String date,
                        @Value("${uji.cpr.periodoDocente}") String periodoDocente,
                        @Value("${uji.cpr.cursoActual}") String cursoActual)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        this.periodoDocente=periodoDocente;
        this.cursoActual=cursoActual;
        try
        {
            initialDate = format.parse(date);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public static Date getInitialDate()
    {
        return initialDate;
    }

    public static boolean isValidDate(Date date)
    {
        return initialDate.before(date);
    }

    public static String getPeriodoDocente()
    {
        return periodoDocente;
    }

    public static void setPeriodoDocente(String periodoDocente)
    {
        GeneralUtils.periodoDocente = periodoDocente;
    }

    public static String getCursoActual()
    {
        return cursoActual;
    }

    public static void setCursoActual(String cursoActual)
    {
        GeneralUtils.cursoActual = cursoActual;
    }
}
