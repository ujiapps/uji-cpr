package es.uji.apps.cpr.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import es.uji.apps.cpr.model.EventUser;
import es.uji.apps.cpr.model.User;

@XmlRootElement(name = "UserUI")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserUI implements Serializable {

    static final long USERID_UJI_DEFAULT = 40000;

    private String login;
    private String name;
    private long id;
    private String surname;
    private String type;
    private List<CheckUI> checks;
    private int areaId;

    public UserUI() {
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<CheckUI> getChecks() {
        return checks;
    }

    public void setChecks(List<CheckUI> checks) {
        this.checks = checks;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public static List<UserUI> toUI(List<User> userList) {
        List<UserUI> userUIList = new ArrayList<UserUI>();

        for (User user : userList) {

            UserUI userUI = toUI(user, null);
            userUIList.add(userUI);
        }

        return userUIList;
    }

    public static UserUI toUI(User user, List<CheckUI> checks) {
        UserUI userUI = new UserUI();
        userUI.setLogin(user.getLogin());
        userUI.setName(user.getName());
        userUI.setId(user.getUserId());
        String surname1 = new String("");
        String surname2 = new String("");

        if (user.getUserId() == USERID_UJI_DEFAULT) {
            userUI.setName("Targeta blanca");
            userUI.setSurname("");
        } else {
            if (user.getSurname1() != null) {
                surname1 = user.getSurname1();
            }
            if (user.getSurname2() != null) {
                surname2 = user.getSurname2();
            }
            userUI.setSurname(surname1 + " " + surname2);
        }

        userUI.setType(user.getType());
        if (checks != null) {
            userUI.setChecks(checks);
        }
        if (user.getAreaId() != null) {
            userUI.setAreaId(user.getAreaId());
        }
        return userUI;

    }

    public static UserUI toUI(EventUser eventUser, List<CheckUI> checks) {
        UserUI userUI = new UserUI();
        userUI.setName(eventUser.getName());
        userUI.setId(eventUser.getUser().getUserId());
        userUI.setSurname(eventUser.getSurname());
        userUI.setType(eventUser.getType());
        if (checks != null) {
            userUI.setChecks(checks);
        }
        return userUI;

    }

}
