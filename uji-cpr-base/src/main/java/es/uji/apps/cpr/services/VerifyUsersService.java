package es.uji.apps.cpr.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.AreasResponsibleDAO;
import es.uji.apps.cpr.dao.DepartmentsDAO;
import es.uji.apps.cpr.dao.DirDegreeDAO;
import es.uji.apps.cpr.dao.UserDAO;
import es.uji.apps.cpr.model.DirDegree;
import es.uji.apps.cpr.model.User;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.dao.ApaDAO;

@Service
public class VerifyUsersService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private DepartmentsDAO departmentsDAO;

    @Autowired
    private AreasResponsibleDAO areasResponsibleDAO;

    @Autowired
    private ApaDAO apaDAO;

    @Autowired
    private DirDegreeDAO dirDegreeDAO;

    public boolean isAdmin(long connectedUser) {
        return apaDAO.hasPerfil("CPR", "ADMIN", connectedUser);
    }

    public boolean isDepDir(long connectedUser) {
        if (departmentsDAO.getDepartmentByDepartments(connectedUser) != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isRespArea(long connectedUser) {
        if (areasResponsibleDAO.getAreasResponsiblesByUserId(connectedUser, -1, -1, null, null) != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isPDI(long connectedUser) {
        try {
            User user =userDAO.getUserById(connectedUser);
            if (user != null && user.getType().equals("PDI")) {
                return true;
            } else {
                return false;
            }
        } catch (GeneralCPRException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isTitDir(long connectedUser) {
        try {
            List<DirDegree> dir = dirDegreeDAO.getDirDegreeByUsersId(connectedUser);
            if (dir != null && dir.size()>0) {
                return true;
            } else {
                return false;
            }
        } catch (GeneralCPRException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Boolean isAlumno(Long userId)
    {
        return userDAO.isAlumno(userId);
    }
}
