package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CPR_VW_INCIDENCIAS_AUTO", schema = "UJI_CONTROLPRESENCIA")
public class IncidenciasAutoResueltas implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "IDUSER")
    private Long userId;

    @Column(name = "START_EVENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startEvent;

    @Column(name = "TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @Column(name = "GRUPO_COMP")
    private String grupoComp;

    @OneToOne
    @JoinColumn(name = "CHECK_ID")
    private Check checkIn;

    @Column(name = "AREA_ID")
    private Integer areaId;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "LOCATION_CHECK")
    private String locationCheck;


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Date getStartEvent()
    {
        return startEvent;
    }

    public void setStartEvent(Date startEvent)
    {
        this.startEvent = startEvent;
    }

    public Date getTime()
    {
        return time;
    }

    public void setTime(Date time)
    {
        this.time = time;
    }

    public String getGrupoComp()
    {
        return grupoComp;
    }

    public void setGrupoComp(String grupoComp)
    {
        this.grupoComp = grupoComp;
    }

    public Check getCheckIn()
    {
        return checkIn;
    }

    public void setCheckIn(Check checkIn)
    {
        this.checkIn = checkIn;
    }

    public Integer getAreaId()
    {
        return areaId;
    }

    public void setAreaId(Integer areaId)
    {
        this.areaId = areaId;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getLocationCheck()
    {
        return locationCheck;
    }

    public void setLocationCheck(String locationCheck)
    {
        this.locationCheck = locationCheck;
    }
}