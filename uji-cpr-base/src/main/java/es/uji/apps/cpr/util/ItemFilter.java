package es.uji.apps.cpr.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

public class ItemFilter
{
    private final String field;
    private final String value;

    public ItemFilter(String field, String value)
    {
        this.field = field;
        this.value = value;
    }

    public static List<ItemFilter> jsonDecode(String json) throws IOException
    {
        List<ItemFilter> itemFilters = new ArrayList<ItemFilter>();
        if (json != null && json.length() > 0)
        {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory factory = mapper.getJsonFactory();
            JsonParser jp = factory.createJsonParser(json);
            JsonNode actualObj = mapper.readTree(jp);
            for (int i = 0; i < actualObj.size(); i++)
            {
                String sp = actualObj.get(i).get("property").getTextValue();
                String sd = actualObj.get(i).get("value").getTextValue();
                if (sd == null)
                {
                    sd = actualObj.get(i).get("value").asText();
                }
                itemFilters.add(new ItemFilter(sp, sd));
            }
        }
        return itemFilters;
    }

    public String getField()
    {
        return field;
    }

    public String getValue()
    {
        return value;
    }


}
