package es.uji.apps.cpr.ui;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import es.uji.apps.cpr.model.Event;
import es.uji.apps.cpr.model.User;
import es.uji.apps.cpr.util.CSVUtils;

public class IncidencePendingUI implements Serializable
{

    private EventUI event;
    private List<UserUI> userList;
    private byte type;

    public IncidencePendingUI()
    {

    }

    public EventUI getEvent()
    {
        return event;
    }

    public void setEvent(EventUI event)
    {
        this.event = event;
    }

    public List<UserUI> getUserList()
    {
        return userList;
    }

    public void setUserList(List<UserUI> userList)
    {
        this.userList = userList;
    }

    public byte getType()
    {
        return type;
    }

    public void setType(byte type)
    {
        this.type = type;
    }

    public static IncidencePendingUI toUI(EventUI eventUI, UserUI userUI, byte type)
    {
        IncidencePendingUI incidencePendingUI = new IncidencePendingUI();
        incidencePendingUI.setEvent(eventUI);
        List<UserUI> userUIList = new ArrayList<UserUI>();
        userUIList.add(userUI);
        incidencePendingUI.setUserList(userUIList);
        incidencePendingUI.setType(type);

        return incidencePendingUI;
    }

    public static IncidencePendingUI toUI(Event event, List<User> userList, byte type)
    {
        IncidencePendingUI incidencePendingUI = new IncidencePendingUI();
        incidencePendingUI.setEvent(EventUI.toUI(event));
        incidencePendingUI.setUserList(UserUI.toUI(userList));
        incidencePendingUI.setType(type);
        return incidencePendingUI;
    }

    public static String toCSV(List<IncidencePendingUI> incidences)
    {

        final char DELIMITER = ';';

        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = null;

        try
        {
            csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");

            List<String[]> records = new ArrayList<String[]>();

            List<String> record = null;

            record = Arrays.asList("TIPUS", "DATA D´INICI CLASE", "USUARI", "EVENT");

            String[] recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);

            for (IncidencePendingUI incidence : incidences)
            {
                ArrayList<String> names = new ArrayList<String>();
                for (UserUI userUI : incidence.getUserList())
                {
                    names.add(String.valueOf(userUI.getName() == null ? "" : userUI.getName().concat(" " + userUI.getSurname())));
                }
                String subTipo = incidence.getEvent().getId().substring(incidence.getEvent().getId().length() - 5);
                Integer tipoIndex = subTipo.indexOf(incidence.getEvent().getTipoId());
                String tipo = subTipo.substring(tipoIndex);
                record = Arrays.asList(String.valueOf(CSVUtils.parseType(incidence.getType())),
                        String.valueOf(incidence.getEvent().getStartEvent() == null ? "" : incidence.getEvent().getStartEvent()),
                        StringUtils.join(names, " - "),
                        String.valueOf(incidence.getEvent().getCode()
                                + " - " + tipo
                                + " - " + incidence.getEvent().getName()));

                recordArray = new String[record.size()];
                record.toArray(recordArray);
                records.add(recordArray);
            }

            csvWriter.writeAll(records);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return writer.toString();
    }

    private static Date getDate(IncidencePendingUI incidence)
    {

        Date result = new Date();

        if (incidence.getType() == 0)
        {
            result = incidence.getEvent().getStartEvent();
        }
        if (incidence.getType() == 1)
        {
            result = incidence.getEvent().getEndEvent();
        }

        return result;
    }


    @Override
    public int hashCode()
    {
        int grupo = (event.getGrupoComp() != null) ? event.getGrupoComp().hashCode() : 0;
        int result = grupo
                + event.getStartEvent().hashCode()
                + event.getName().hashCode()
                + event.getLocation().hashCode()
                + event.getTipoId().hashCode();
        result = 31 * result + (int) type;
        return result;
    }

    @Override
    public boolean equals(final Object object)
    {

        if (this == object) return true;
        if (!(object instanceof IncidencePendingUI)) return false;
        final IncidencePendingUI point = (IncidencePendingUI) object;

        if (this.getEvent().getId().equals(point.getEvent().getId()))
        {
            return true;
        } else if (this.getEvent().getGrupoComp()!= null && point.getEvent().getGrupoComp()!= null)
        {
            if (this.getEvent().getGrupoComp().equals(point.getEvent().getGrupoComp()) &&
                    this.getEvent().getLocation().equals(point.getEvent().getLocation()) &&
                    this.getEvent().getStartEvent().equals(point.getEvent().getStartEvent())
                    )
            {
                return true;
            }
        }
        return false;
    }

    public static List<IncidencePendingUI> eliminaDuplicados(List<IncidencePendingUI> incidencePendingUIList)
    {

        Set<IncidencePendingUI> set = new HashSet<IncidencePendingUI>();
        set.addAll(incidencePendingUIList);
        incidencePendingUIList.clear();
        incidencePendingUIList.addAll(set);
        return incidencePendingUIList;

    }
}
