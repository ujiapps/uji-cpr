package es.uji.apps.cpr.ui;

public class NearEventsUI
{
    EventUI nextByUser;
    EventUI prevByUser;
    EventUI nextByLocation;
    EventUI prevByLocation;

    public EventUI getNextByUser()
    {
        return nextByUser;
    }

    public void setNextByUser(EventUI nextByUser)
    {
        this.nextByUser = nextByUser;
    }

    public EventUI getPrevByUser()
    {
        return prevByUser;
    }

    public void setPrevByUser(EventUI prevByUser)
    {
        this.prevByUser = prevByUser;
    }

    public EventUI getNextByLocation()
    {
        return nextByLocation;
    }

    public void setNextByLocation(EventUI nextByLocation)
    {
        this.nextByLocation = nextByLocation;
    }

    public EventUI getPrevByLocation()
    {
        return prevByLocation;
    }

    public void setPrevByLocation(EventUI prevByLocation)
    {
        this.prevByLocation = prevByLocation;
    }

}
