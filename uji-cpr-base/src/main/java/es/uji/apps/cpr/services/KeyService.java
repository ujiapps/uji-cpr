package es.uji.apps.cpr.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.KeyDAO;
import es.uji.apps.cpr.model.Key;

@Service
public class KeyService
{

    @Autowired
    private KeyDAO keyDAO;

    public Key getKeyByKeyId(String key) throws GeneralCPRException
    {
        return keyDAO.getKeyByKeyId(key);
    }

}
