package es.uji.apps.cpr.ui;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import es.uji.apps.cpr.model.Check;
import es.uji.apps.cpr.model.Event;

public class EventUI implements Serializable {

    private String code;
    private long cursoAca;
    private Date endEvent;
    private String id;
    private String location;
    private String name;
    private Date startEvent;
    private Integer grupoComp;
    private String grupoNom;
    private String tipoId;
    private List<CheckUI> checksIn;
    private List<CheckUI> checksOut;
    private String startDate;
    private String endDate;
    private int resourceId;

    public EventUI() {
    }

    public List<CheckUI> getChecksOut() {
        return checksOut;
    }

    public void setChecksOut(List<CheckUI> checksOut) {
        this.checksOut = checksOut;
    }

    public List<CheckUI> getChecksIn() {
        return checksIn;
    }

    public void setChecksIn(List<CheckUI> checksIn) {
        this.checksIn = checksIn;
    }

    public String getCode() {
        return this.code;

    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getCursoAca() {
        return this.cursoAca;
    }

    public void setCursoAca(long cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Date getEndEvent() {
        return this.endEvent;
    }

    public void setEndEvent(Date endEvent) {
        this.endEvent = endEvent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartEvent() {
        return this.startEvent;
    }

    public void setStartEvent(Date startEvent) {
        this.startEvent = startEvent;
    }

    public String getTipoId()
    {
        return tipoId;
    }

    public void setTipoId(String tipoId)
    {
        this.tipoId = tipoId;
    }

    public Integer getGrupoComp()
    {
        return grupoComp;
    }

    public void setGrupoComp(Integer grupoComp)
    {
        this.grupoComp = grupoComp;
    }

    public String getGrupoNom()
    {
        return grupoNom;
    }

    public void setGrupoNom(String grupoNom)
    {
        this.grupoNom = grupoNom;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.startDate = formatter.format(startDate);
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.endDate = formatter.format(endDate);
    }

    public static EventUI toUI(Event event) {
        if (event == null)
            return null;

        EventUI eventUI = new EventUI();
        eventUI.setId(event.getEventId());
        eventUI.setName(event.getName());
        eventUI.setCursoAca(event.getCursoAca());
        eventUI.setEndEvent(event.getEndEvent());
        eventUI.setStartEvent(event.getStartEvent());
        eventUI.setLocation(event.getLocation());
        eventUI.setCode(event.getCode());
        eventUI.setTipoId(event.getTipoId());
        eventUI.setGrupoComp(event.getGrupoComp());
        eventUI.setGrupoNom(event.getGrupoNom());


        return eventUI;
    }

    public static EventUI toUI(Event event, List<Check> checksIn, List<Check> checksOut) {
        EventUI eventUI = toUI(event);
        eventUI.setChecksIn(CheckUI.toUI(checksIn));
        eventUI.setChecksOut(CheckUI.toUI(checksOut));
        return eventUI;
    }

    public static EventUI toUIByUI(Event event, List<CheckUI> checksIn, List<CheckUI> checksOut) {
        EventUI eventUI = toUI(event);
        eventUI.setChecksIn(checksIn);
        eventUI.setChecksOut(checksOut);
        return eventUI;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }
}