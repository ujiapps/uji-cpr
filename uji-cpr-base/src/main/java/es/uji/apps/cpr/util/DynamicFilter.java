package es.uji.apps.cpr.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

public class DynamicFilter {

    public static BooleanBuilder applyFilter(List<ItemFilter> filterList,
                                             Map<String, Object> validFilters) {

        BooleanBuilder result = new BooleanBuilder();

        if (filterList.size() > 0 && filterList != null) {
            for (ItemFilter f : filterList) {
                if (validFilters.get(f.getField()) != null) {
                    if (validFilters.get(f.getField()).getClass().getCanonicalName()
                            .equals("com.mysema.query.types.path.NumberPath")) {
                        NumberPath<Integer> field = (NumberPath<Integer>) validFilters.get(f
                                .getField());
                        if (field != null) {
                            result.and(field.eq(Integer.valueOf(f.getValue())));
                        }
                    } else if (validFilters.get(f.getField()).getClass().getCanonicalName()
                            .equals("com.mysema.query.types.path.StringPath")) {
                        StringPath field = (StringPath) validFilters.get(f.getField());
                        if (field != null) {
                            result.and(field.lower().like("%" + f.getValue() + "%"));
                        }
                    }
                }
            }
        }
        return result;

    }

    public static List<OrderSpecifier<?>> applySort(List<ItemSort> sortList,
                                                    Map<String, Object> validSorts) {
        List<OrderSpecifier<?>> order = new ArrayList<OrderSpecifier<?>>();
        if (sortList.size() > 0 && sortList != null) {
            for (ItemSort s : sortList) {
                if (validSorts.get(s.getField()) != null) {
                    if (validSorts.get(s.getField()).getClass().getCanonicalName()
                            .equals("com.mysema.query.types.path.NumberPath")) {
                        NumberPath<Long> field = (NumberPath<Long>) validSorts.get(s.getField());
                        if (s.getDirection().equalsIgnoreCase("asc")) {
                            order.add(field.asc());
                        } else {
                            order.add(field.desc());
                        }
                    } else if (validSorts.get(s.getField()).getClass().getCanonicalName()
                            .equals("com.mysema.query.types.path.StringPath")) {
                        StringPath field = (StringPath) validSorts.get(s.getField());
                        if (s.getDirection().equalsIgnoreCase("asc")) {
                            order.add(field.asc());
                        } else {
                            order.add(field.desc());
                        }

                    } else if (validSorts.get(s.getField()).getClass().getCanonicalName()
                            .equals("com.mysema.query.types.path.DateTimePath")) {
                        DateTimePath field = (DateTimePath) validSorts.get(s.getField());
                        if (s.getDirection().equalsIgnoreCase("asc")) {
                            order.add(field.asc());
                        } else {
                            order.add(field.desc());
                        }

                    }
                }
            }
        }
        return order;
    }
}
