package es.uji.apps.cpr.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.model.Departments;
import es.uji.apps.cpr.model.User;
import es.uji.apps.cpr.services.DepartmentsService;
import es.uji.apps.cpr.services.UserService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("departamento")
public class DepartamentResource  extends CoreBaseService
{
    @InjectParam
    private DepartmentsService departmentsService;

    @InjectParam
    private UserService userService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getUserDepartaments() throws GeneralCPRException
    {
        Long userId = AccessManager.getConnectedUserId(request);

        List<UIEntity> entities = new ArrayList<UIEntity>();
        User user = userService.getUserById(userId);
        for (Departments department : departmentsService.getDepartamentsByArea(user.getAreaId()))
        {
            entities.add(UIEntity.toUI(department));
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(departmentsService.getDepartment(id));
    }
}