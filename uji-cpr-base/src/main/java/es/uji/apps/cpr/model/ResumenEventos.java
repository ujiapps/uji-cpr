package es.uji.apps.cpr.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CPR_VMC_RESUMEN_EVENTOS", schema = "UJI_CONTROLPRESENCIA")
public class ResumenEventos implements Serializable
{
    @Id
    private String id;

    private String code;

    private String subgrupo;
    private String tipo;
    @Column(name = "ESTAT")
    private String estado;

    @Column(name = "USUARI")
    private String usuario;
    @Id
    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "START_EVENT")
    private Date startEvent;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_EVENT")
    private Date endEvent;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CHECK_DATE")
    private Date checkDate;

    @Column(name = "LOCATION")
    private String location;


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getSubgrupo()
    {
        return subgrupo;
    }

    public void setSubgrupo(String subgrupo)
    {
        this.subgrupo = subgrupo;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Date getStartEvent()
    {
        return startEvent;
    }

    public void setStartEvent(Date startEvent)
    {
        this.startEvent = startEvent;
    }

    public Date getEndEvent()
    {
        return endEvent;
    }

    public void setEndEvent(Date endEvent)
    {
        this.endEvent = endEvent;
    }

    public Date getCheckDate()
    {
        return checkDate;
    }

    public void setCheckDate(Date checkDate)
    {
        this.checkDate = checkDate;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }
}
