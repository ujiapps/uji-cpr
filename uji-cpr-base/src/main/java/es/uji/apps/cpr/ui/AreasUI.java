package es.uji.apps.cpr.ui;

import java.io.Serializable;

import es.uji.apps.cpr.model.DepartmentsAreas;

public class AreasUI implements Serializable
{

    private int areaId;
    private String area;
    private int users;

    public AreasUI()
    {

    }

    public int getAreaId()
    {
        return areaId;
    }

    public void setAreaId(int areaId)
    {
        this.areaId = areaId;
    }

    public String getArea()
    {
        return area;
    }

    public void setArea(String area)
    {
        this.area = area;
    }

    public int getUsers()
    {
        return users;
    }

    public void setUsers(int users)
    {
        this.users = users;
    }

    public static AreasUI toUI(DepartmentsAreas departmentsAreas, int users)
    {
        AreasUI areasUI = new AreasUI();
        areasUI.setAreaId(departmentsAreas.getAreaId());
        areasUI.setArea(departmentsAreas.getArea());
        areasUI.setUsers(users);

        return areasUI;
    }
}
