package es.uji.apps.cpr;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class GeneralCPRException extends CoreBaseException {

    public GeneralCPRException() {
        super("S´ha produit un error en l´operació");
    }

    public GeneralCPRException(String message) {
        super(message);
    }

}
