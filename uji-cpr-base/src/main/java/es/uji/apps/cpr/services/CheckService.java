package es.uji.apps.cpr.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.dao.CheckDAO;
import es.uji.apps.cpr.model.Check;
import es.uji.apps.cpr.util.ItemFilter;
import es.uji.apps.cpr.util.ItemSort;

@Service
public class CheckService
{
    static final Logger logger = LoggerFactory.getLogger(CheckService.class);

    @Autowired
    private CheckDAO checkDAO;

    public Check getCheckById(long id) throws GeneralCPRException
    {
        return checkDAO.getCheckById(id);
    }

    @Transactional
    public Check update(Check check)
    {
        return checkDAO.update(check);
    }

    public List<Check> getChecksByEventId(String eventId, int start, int limit,
                                          List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        return checkDAO.getChecksByEventId(eventId, start, limit, filterList, sortList);
    }

    public List<Check> getChecksByEvent(String eventId) throws GeneralCPRException
    {
        return checkDAO.getChecksByEvent(eventId);
    }

    public List<Check> getChecksByEventIdAndUserIds(String eventId, List<Long> userIds, int start,
                                                    int limit, List<ItemFilter> filterList, List<ItemSort> sortList)
            throws GeneralCPRException
    {
        return checkDAO.getChecksByEventIdAndUserIds(eventId, userIds, start, limit, filterList,
                sortList);
    }

    public List<Check> getChecksByEventIdAndUserId(String eventId, long userId)
            throws GeneralCPRException
    {
        return checkDAO.getChecksByEventIdAndUserId(eventId, userId);
    }

    public List<Check> getChecksByUserId(long userId, int start, int limit,
                                         List<ItemFilter> filterList, List<ItemSort> sortList) throws GeneralCPRException
    {
        return checkDAO.getChecksByUserId(userId, start, limit, filterList, sortList);
    }

    public List<Check> getChecksByDate(Date startDate, Date endDate) throws GeneralCPRException
    {
        return checkDAO.getChecksByDate(startDate, endDate);
    }

    public List<Check> getAccessChecksByUserIdAndDate(long userId, Date startEvent, int start,
                                                      int limit) throws GeneralCPRException
    {

        List<Check> checksResult = checkDAO.getChecksByUserIdAndDate(userId, startEvent, start, limit);

        return checksResult;
    }

    public List<Check> getExitsChecksByUserIdAndDate(long userId, Date endEvent, int start,
                                                     int limit) throws GeneralCPRException
    {
        return checkDAO.getChecksByUserIdAndDate(userId, endEvent, start, limit);
    }

    public List<Check> getChecksByLocation(String location, Date startEvent, Long userId)
    {
        return checkDAO.getChecksByLocation(location, startEvent, userId);
    }

    public List<Check> searchChecks(String location, String fecha, String userLogin) throws ParseException
    {

        return checkDAO.searchChecks(location, fecha, userLogin);

    }
}
