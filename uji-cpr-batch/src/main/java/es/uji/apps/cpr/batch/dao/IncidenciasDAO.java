package es.uji.apps.cpr.batch.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.cpr.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class IncidenciasDAO extends BaseDAODatabaseImpl {

    public static Logger log = LoggerFactory.getLogger(IncidenciasDAO.class);


    private QIncidenciasAutoResueltas qIncidenciasAutoResueltas = QIncidenciasAutoResueltas.incidenciasAutoResueltas;
    private QEventPendent qEventPendent = QEventPendent.eventPendent;
    private QIncidence qIncidence = QIncidence.incidence;
    private QIncidenciaPermiso qIncidenciaPermiso = QIncidenciaPermiso.incidenciaPermiso;

    public List<IncidenciasAutoResueltas> getIncidenciasAuto() {
        log.info("Extrayendo incidencias auto.");
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qIncidenciasAutoResueltas).list(qIncidenciasAutoResueltas);
    }

    public List<EventPendent> getIncidenciasByDate(Date diaInicial, Date diaFinal) {
        log.info("Entrando en el getIncidenciasByDate.");
        JPAQuery query = new JPAQuery(entityManager);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {

            Date beginday = df.parse(df.format(diaInicial));
            Date endday = df2.parse(df.format(diaFinal) + " 23:59:59");
            query.from(qEventPendent)
                    .where(qEventPendent.startEvent.goe(beginday).and(qEventPendent.startEvent.loe(endday)
                            .and(new JPASubQuery().from(qIncidence)
                                    .where(qIncidence.event.eventId.eq(qEventPendent.eventId)).notExists())));

        } catch (ParseException e) {
            log.error("Error al parseando fechas", e);
        }
        return query.list(qEventPendent);

    }

    public List<IncidenciaPermiso> getIncidenciasPermisos() {
        log.info("Entrayendo incidencias permisos.");
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qIncidenciaPermiso).list(qIncidenciaPermiso);
    }
}
