package es.uji.apps.cpr.batch.services;

import es.uji.apps.cpr.model.AvisoUsuario;
import es.uji.apps.cpr.model.Log;
import es.uji.apps.cpr.model.User;
import es.uji.apps.cpr.services.LogService;
import es.uji.apps.cpr.services.UserService;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AvisoService {
    private static final int TIEMPO_ESPERA_ENTRE_CONSULTAS = 3600000;
    public static Logger log = LoggerFactory.getLogger(AvisoService.class);
    private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat hourOfDay = new SimpleDateFormat("HH");
    private SimpleDateFormat dayOfWeek = new SimpleDateFormat("u");

    private UserService userService;
    private LogService logService;


    @Autowired
    public AvisoService(UserService userService, LogService logService)
    {
        this.userService = userService;
        this.logService = logService;
    }

//    public static void main(String[] args) throws Exception
//    {
//        ApplicationContext context = new ClassPathXmlApplicationContext(
//                "classpath:applicationContext.xml");
//        UserService userService = context.getBean(UserService.class);
//        LogService logService = context.getBean(LogService.class);
//
//        AvisoService avisoService = new AvisoService(userService, logService);
//
//        log.info("START CPR Avisos " + new Date());
//        new Thread(avisoService).start();
//    }

    public Date getFirstDayOfWeek(Date dia)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dia);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return cal.getTime();
    }

    public void procesaAvisos()
    {
        try
        {
            log.info("Procensando envio de avisos");
            Date date = new Date();
            Integer hour = Integer.parseInt(hourOfDay.format(date));
            Integer dayOfweek = Integer.parseInt(dayOfWeek.format(date));
            if (userService.getPeriodoDocente().equals("1"))
            {
                if (hour == 9 && dayOfweek == 1)
                {
                    Integer numAvisosSemanal = sendMailUsersConAvisoSemanal(GeneralUtils.getInitialDate(), getOneDayBefore());
                    Integer numAvisosArea = sendMailDirectoresArea(GeneralUtils.getInitialDate(), getThreeWeeksBefore());
                    Integer numAvisosTitulacion = sendMailDirectoresTitulacion(GeneralUtils.getInitialDate(), getThreeWeeksBefore());
                    log.info("Envio semanal PDI: " + numAvisosSemanal);
                    log.info("Envio semanal Directores area: " + numAvisosArea);
                    log.info("Envio semanal Directores titulacion: " + numAvisosTitulacion);
                    if (numAvisosSemanal > 0 || numAvisosArea > 0 || numAvisosTitulacion > 0)
                    {
                        sendMail("Aviso Semanal:<br/ > Numero de mails enviados PDI: " + numAvisosSemanal.toString() +
                                "<br/ > Numero de mails enviados Directores area: " + numAvisosArea.toString() +
                                "<br/ > Numero de mails enviados Directores titulacion: " + numAvisosTitulacion.toString());
                    }

                }
            }

        }
        catch (Exception e)
        {
            log.error("Error creando la incidencias", e);

        }

    }

    public void sendMail(String cuerpo)
    {
        MailMessage mensaje = new MailMessage("CPR");
        mensaje.setTitle("[CPR] Informe Avisos");
        mensaje.setContentType(MediaType.TEXT_HTML_VALUE);
        mensaje.setSender("CPR-Avisos <noreply@uji.es>");
        mensaje.setContent(cuerpo);
        mensaje.addToRecipient("jogil@uji.es");
        MessagingClient client = new MessagingClient();
        try
        {
            client.send(mensaje);
        }
        catch (MessageNotSentException e)
        {
            log.error("Error en el hilo: ", e);
        }
    }

    public Date getOneDayBefore()
    {
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        return cal.getTime();
    }


    public Date getThreeWeeksBefore()
    {
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -21);
        return cal.getTime();
    }

    public Integer sendMailUsersConAvisoSemanal(Date fechaIni, Date fechaFin) {
        try {
            Integer avisos = 0;
            List<AvisoUsuario> userList = userService.getUsersConAvisoSemanal(fechaIni, fechaFin);
            Map<Long, List<AvisoUsuario>> avisosAgrupadosByUser = userList.stream().collect(Collectors.groupingBy(AvisoUsuario::getUserId));
            for (Map.Entry<Long, List<AvisoUsuario>> avisoAgrupado : avisosAgrupadosByUser.entrySet()) {
                String cuerpo = null;
                try {
                    String eventos = formateaEventos(avisoAgrupado.getValue());
                    AvisoUsuario user = avisoAgrupado.getValue().get(0);
                    if (ParamUtils.isNotNull(user.getLogin())) {
                        cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                                "/etc/uji/cpr/templates/profesorSemanal.template")));
                        cuerpo = MessageFormat.format(cuerpo, user.getNameUsuario(), "//ujiapps.uji.es/cpr", eventos);
                        MailMessage mensaje = new MailMessage("CPR");
                        mensaje.setTitle("Recordatori d'incidències pendents en el seguiment docent");
                        mensaje.setContentType(MediaType.TEXT_HTML_VALUE);
                        mensaje.setSender("Seguiment docent <seguiment_docent@uji.es>");
                        mensaje.setContent(cuerpo);
                        mensaje.addToRecipient(user.getLogin());
                        MessagingClient client = new MessagingClient();
                        client.send(mensaje);
                        Log log = new Log(user.getUserId(), user.getLogin(), df.parse(df.format(new Date())), "2", "ENVIADO");
                        logService.insert(log);
                        avisos++;
                    }
                } catch (Exception e) {
                    log.error("Error al leer plantilla: ", e);
                }
            }
            return avisos;
        } catch (Exception e) {
            log.error("Error al enviar mail: ", e);
        }
        return 0;
    }

    private String formateaEventos(List<AvisoUsuario> value) {
        String pattern = "dd/MM/yyyy HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return value.stream().map(a ->
                        "".concat(a.getNameAsignatura())
                                .concat(" - ")
                                .concat(a.getCode())
                                .concat(" ")
                                .concat(a.getSubGrupo())
                                .concat(" - ")
                                .concat(simpleDateFormat.format(a.getStartEvent()))
//                                .concat(" - ")
//                                .concat(simpleDateFormat.format(a.getEndEvent())))
                .concat("\n<br />")
        ).collect(Collectors.joining());
    }

    public Integer sendMailDirectoresArea(Date fechaIni, Date fechaFin) {
        String cuerpo = null;
        try
        {
            List<User> userList = userService.getDirectoresAreaAviso(fechaIni, fechaFin);
            Integer avisos = 0;
            for (User user : userList)
            {
                try
                {
                    if (ParamUtils.isNotNull(user.getLogin()))
                    {
                        cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                                "/etc/uji/cpr/templates/directorDepartamento.template")));
                        cuerpo = MessageFormat.format(cuerpo, user.getName(), "//ujiapps.uji.es/cpr");
                        MailMessage mensaje = new MailMessage("CPR");
                        mensaje.setTitle("Informe setmanal d'incidències en el seguiment docent");
                        mensaje.setContentType(MediaType.TEXT_HTML_VALUE);
                        mensaje.setSender("Seguiment docent <seguiment_docent@uji.es>");
                        mensaje.setContent(cuerpo);
                        mensaje.addToRecipient(user.getLogin());
                        MessagingClient client = new MessagingClient();
                        client.send(mensaje);
                        Log log = new Log(user.getUserId(), user.getLogin(), df.parse(df.format(new Date())), "3", "ENVIADO");
                        logService.insert(log);
                        avisos++;
                    }
                }
                catch (Exception e)
                {
                    log.error("Error al enviar mail: ", e);
                }
            }
            return avisos;

        }
        catch (Exception e)
        {
            log.error("Error al leer plantilla: ", e);
        }
        return 0;
    }

    public Integer sendMailDirectoresTitulacion(Date fechaIni, Date fechaFin)
    {
        String cuerpo = null;
        try
        {
            cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                    "/etc/uji/cpr/templates/directorTitulacion.template")));
            List<User> userList = userService.getDirectoresTitulacionAviso(fechaIni, fechaFin);
            Integer avisos = 0;
            for (User user : userList)
            {
                try
                {
                    if (ParamUtils.isNotNull(user.getLogin()))
                    {
                        cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                                "/etc/uji/cpr/templates/directorTitulacion.template")));
                        cuerpo = MessageFormat.format(cuerpo, user.getName(), "//ujiapps.uji.es/cpr");
                        MailMessage mensaje = new MailMessage("CPR");
                        mensaje.setTitle("Informe setmanal d'incidències en el seguiment docent");
                        mensaje.setContentType(MediaType.TEXT_HTML_VALUE);
                        mensaje.setSender("Seguiment docent <seguiment_docent@uji.es>");
                        mensaje.setContent(cuerpo);
                        mensaje.addToRecipient(user.getLogin());
                        MessagingClient client = new MessagingClient();
                        client.send(mensaje);
                        Log log = new Log(user.getUserId(), user.getLogin(), df.parse(df.format(new Date())), "4", "ENVIADO");
                        logService.insert(log);
                        avisos++;
                    }
                }
                catch (Exception e)
                {
                    log.error("Error al enviar mail: ", e);
                }
            }
            return avisos;

        }
        catch (Exception e)
        {
            log.error("Error al leer plantilla: ", e);
        }
        return 0;
    }

    public Integer sendMailUsersConAvisoDia()
    {
        try
        {
            List<User> userList = userService.getUsersConAvisoDia(getOneDayBefore());
            Integer avisos = 0;
            for (User user : userList)
            {
                try
                {
                    if (ParamUtils.isNotNull(user.getLogin()))
                    {
                        String cuerpo = new String(StreamUtils.inputStreamToByteArray(new FileInputStream(
                                "/etc/uji/cpr/templates/profesorDiaria.template")));
                        cuerpo = MessageFormat.format(cuerpo, user.getName(), df.format(getOneDayBefore()), "//ujiapps.uji.es/cpr");
                        MailMessage mensaje = new MailMessage("CPR");
                        mensaje.setTitle("Nova incidència en el seguiment docent");
                        mensaje.setContentType(MediaType.TEXT_HTML_VALUE);
                        mensaje.setSender("Seguiment docent <seguiment_docent@uji.es>");
                        mensaje.setContent(cuerpo);
                        mensaje.addToRecipient(user.getLogin());
                        MessagingClient client = new MessagingClient();
                        client.send(mensaje);
                        Log log = new Log(user.getUserId(), user.getLogin(), df.parse(df.format(new Date())), "1", "ENVIADO");
                        logService.insert(log);
                        avisos++;
                    }
                }
                catch (Exception e)
                {
                    log.error("Error al enviar mail: ", e);
                }
            }
            return avisos;

        }
        catch (Exception e)
        {
            log.error("Error al leer plantilla: ", e);
        }
        return 0;
    }
}
