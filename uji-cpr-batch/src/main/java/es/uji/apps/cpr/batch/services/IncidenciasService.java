package es.uji.apps.cpr.batch.services;

import es.uji.apps.cpr.GeneralCPRException;
import es.uji.apps.cpr.batch.dao.IncidenciasDAO;
import es.uji.apps.cpr.dao.CheckDAO;
import es.uji.apps.cpr.model.*;
import es.uji.apps.cpr.util.GeneralUtils;
import es.uji.apps.cpr.util.IncidenceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
public class IncidenciasService {
    public static Logger log = LoggerFactory.getLogger(IncidenciasService.class);

    private IncidenciasDAO incidenciasDAO;
    private CheckDAO checkDAO;

    @Autowired
    public IncidenciasService(IncidenciasDAO incidenciasDAO, CheckDAO checkDAO) {

        this.incidenciasDAO = incidenciasDAO;
        this.checkDAO = checkDAO;
    }

    public void cerrarIncidenciasAuto() {
        try {
            log.info("Creando incidencias autoresueltas.");
            incidenciasDAO.getIncidenciasAuto().stream().collect(groupingBy(IncidenciasAutoResueltas::getGrupoComp))
                    .entrySet().forEach(e -> {
                        log.info("Recorriendo los resultados de las incidencias auto.");
                        if (e.getKey().equals("0")) {
                            e.getValue().stream().collect(groupingBy(IncidenciasAutoResueltas::getId))
                                    .entrySet().forEach(i -> addIncidencia(i.getValue().get(0)));

                        } else {
                            addIncidencia(e.getValue().get(0));
                        }
                    });
            log.info("Acabando incidencias auto.");
        } catch (Exception e) {
            log.error("Error cerrando la incidencia", e);

        }
    }

    @Transactional
    void addIncidencia(IncidenciasAutoResueltas incidenciasAutoResueltas) {
        log.info("Entrando en el addIncidencia.");
        IncidenceManager incidenceManager = new IncidenceManager();
        Incidence incidence = incidenceManager.createIncidenceAuto(incidenciasAutoResueltas);
        incidenciasDAO.insert(incidence);
        if (incidence.getStatus() == 1) {
            Check check = incidence.getCheckIn();
            check.setEvent(incidence.getEvent());
            check.setDirection(0);
            checkDAO.update(check);
        }
        log.info("Acabando el addIncidencia.");
    }

    @Transactional
    void addIncidencia(IncidenciaPermiso incidenciaPermiso) {
        log.info("Anyadiendo incidenciaPermiso.");
        IncidenceManager incidenceManager = new IncidenceManager();
        Incidence incidence = incidenceManager.createIncidencePermiso(incidenciaPermiso);
        incidenciasDAO.insert(incidence);
        log.info("Acabando anyadir incidenciaPermiso.");
    }

    @Transactional
    void addIncidenciaAntigua(EventPendent eventPendent) {
        log.info("Entrando en el anyadir incidencia antigua.");
        IncidenceManager incidenceManager = new IncidenceManager();
        Incidence incidence = incidenceManager.createIncidenceEventPendent(eventPendent);
        incidenciasDAO.insert(incidence);
        log.info("Acabando el anyadir incidencia antigua.");
    }

    public void crearIncidenciasAntiguas() {
        try {
            log.info("Creando incidencias antiguas");
            List<EventPendent> a = incidenciasDAO.getIncidenciasByDate(GeneralUtils.getInitialDate(), getThreeWeeksBefore());
            Map<String, List<EventPendent>> map = a.stream().collect(Collectors.groupingBy(event -> event.getEventId().substring(0, 20)));
            log.info("Mapeando incidencias antiguas.");
            map.entrySet().forEach(e -> addIncidenciaAntigua(e.getValue().get(0)));
            log.info("Acabando incidencias antiguas.");
        } catch (Exception e) {
            log.error("Error creando la incidencias", e);

        }

    }

    public void crearIncidenciasPermisos() {
        try {
            log.info("Creando incidencias con permisos.");
            incidenciasDAO.getIncidenciasPermisos().stream().collect(groupingBy(IncidenciaPermiso::getGrupoComp))
                    .entrySet().forEach(e -> {
                        log.info("Entrando en el bucle de incidencias permisos.");
                        if (e.getKey().equals("0")) {
                            e.getValue().stream().collect(groupingBy(IncidenciaPermiso::getEventId))
                                    .entrySet().forEach(i -> tramitaIncidencia(i.getValue().get(0))
                                    );
                        } else {
                            tramitaIncidencia(e.getValue().get(0));
                        }
                    });
            log.info("Acabando incidenciaPermiso.");
        } catch (Exception e) {
            log.error("Error cerrando la incidencia", e);

        }
    }

    public void tramitaIncidencia(IncidenciaPermiso incidenciaPermiso) {
        log.info("Tramitando incidencia.");
        try {
            addIncidencia(incidenciaPermiso);
            IncidenceManager incidenceManager = new IncidenceManager();
            incidenceManager.enviarMailPermiso(incidenciaPermiso);
            log.info("Acabando tramitar incidencia.");
        } catch (InvalidDataAccessApiUsageException | GeneralCPRException ex) {
            log.error("Error al tramitar incidencia", ex);
        }
    }

    public Date getThreeWeeksBefore() {
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -21);
        return cal.getTime();
    }
}
