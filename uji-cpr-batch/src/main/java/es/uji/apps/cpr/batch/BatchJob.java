package es.uji.apps.cpr.batch;

import es.uji.apps.cpr.batch.services.AvisoService;
import es.uji.apps.cpr.batch.services.IncidenciasService;
import es.uji.apps.cpr.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BatchJob implements Runnable {
    private static final int UNA_HORA = 1000 * 60 * 60;
    public static Logger log = LoggerFactory.getLogger(BatchJob.class);
    private AvisoService avisoService;
    private UserService userService;
    private IncidenciasService incidenciasService;

    private SimpleDateFormat hourOfDay = new SimpleDateFormat("HH");
    private SimpleDateFormat dayOfWeek = new SimpleDateFormat("u");

    public BatchJob(AvisoService avisoService, IncidenciasService incidenciasService, UserService userService) {
        this.avisoService = avisoService;
        this.incidenciasService = incidenciasService;
        this.userService = userService;
    }

    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        AvisoService avisoService = context.getBean(AvisoService.class);
        IncidenciasService incidenciasService = context.getBean(IncidenciasService.class);
        UserService userService = context.getBean(UserService.class);

        BatchJob job = new BatchJob(avisoService, incidenciasService, userService);

        log.info("START BatchJob " + new Date());

        new Thread(job).start();
    }

    public void run() {
        while (true) {
            try {
                Date date = new Date();
                Integer hour = Integer.parseInt(hourOfDay.format(date));
                log.info("Hour: {}", hour);
                Integer dayOfweek = Integer.parseInt(dayOfWeek.format(date));
                log.info("DayOfWeek: {}", dayOfweek);
                log.info("Periodo docente: {}", userService.getPeriodoDocente());
                if (userService.getPeriodoDocente().equals("1")) {
                    log.info("Periodo docente correcto (1)");
                    if (hour == 9 && dayOfweek == 1) {
                        log.info("Entrando en el if de las 9 de la mañana los lunes");
                        avisoService.procesaAvisos();
                    }
                    if (hour == 21) {
                        log.info("Entrando en el if de las 21");
                        incidenciasService.cerrarIncidenciasAuto();
                    }
                    if (hour == 22) {
                        log.info("Entrando en el if de las 22");
                        incidenciasService.crearIncidenciasPermisos();
                    }
                    if (hour == 23) {
                        log.info("Entrando en el if de las 23");
                        incidenciasService.crearIncidenciasAntiguas();
                    }
                }
                sleep();
            } catch (Exception e) {
                log.error("Error general", e);
                sleep();
            }
        }
    }

    private void sleep() {
        try {
            log.info("No hay más peticiones a procesar");
            Thread.sleep(UNA_HORA);
        } catch (InterruptedException e) {
            log.error("Error en la interrupción del thread principal", e);
        }
    }
}
